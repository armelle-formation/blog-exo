<?php

class StatusManager extends AbstractManager
{

    /**
     * Get StatusEntity object by id
     * @param int $id
     * @return StatusEntity
     */
    public function getStatusById(int $id) : StatusEntity
    {
        $aStatus = [];
        $bdd = $this->_db;
        $sql = "
            SELECT
              cs.*
            FROM comment_status cs
            WHERE cs.status_id = :id
         ";
        $req = $bdd->prepare($sql);
        $req->bindParam('id', $id, PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aStatus = $req->fetch(PDO::FETCH_ASSOC);
        }
        return new StatusEntity($aStatus);
    }

    /**
     * Return all status for comments
     * @return array
     */
    public function getAllStatus()
    {
        $bdd = $this->_db;
        $aStatus = [];
        $sql = 'SELECT * FROM comment_status cs ORDER BY cs.status_id';

        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oStatus = new StatusEntity($row);
            array_push($aStatus, $oStatus);
        }
        return $aStatus;
    }
}
