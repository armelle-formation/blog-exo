<?php

/**
 * Class StatusEntity : comments status
 */
class StatusEntity extends AbstractEntity
{

    /**
     * @var int
     */
    private $_status_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * @return int
     */
    public function getStatusId(): int
    {
        return $this->_status_id;
    }

    /**
     * @param int $status_id
     */
    public function setStatusId(int $status_id): void
    {
        $this->_status_id = $status_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->_name = $name;
    }
}
