<?php


class ResumeEntity extends AbstractEntity
{

    /**
     * ResumeEntity constructor.
     * @param $resumeObject
     */
    public function __construct($resumeObject)
    {
        $this->extend($resumeObject);
    }
}
