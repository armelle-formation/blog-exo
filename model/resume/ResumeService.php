<?php

/**
 * Class ResumeService :  specific class to buil Resume object
 */
class ResumeService
{

    /**
     * @var array
     */
    public $_section;

    /**
     * Select parts of resume to include
     * @return $this
     */
    public function buildResume()
    {
        $this->loadProfile();

        $this->loadContactDetails();
        $this->loadCareer();
        $this->loadFormation();
        $this->loadAbilities();
        $this->loadProjects();
        $this->loadInterests();
        return $this;
    }

    /**
     * Construct the profile entry according to json file properties
     */
    private function loadProfile()
    {
        $this->_section['profile'] = Data::getInstance()->factory('profile');
        $this->_section['profile']['age'] = Helper::calculateAge($this->_section['profile']['date_of_birth']);
        $this->_section['profile']['full_name'] = $this->_section['profile']['first_name'] . ' '.$this->_section['profile']['last_name'];
    }

    /**
     * Construct the contact entry according to json file properties
     */
    private function loadContactDetails()
    {
        $this->_section['contact'] = Data::getInstance()->factory('contact_details');
    }

    /**
     * Construct the formation entry according to json file properties
     */
    private function loadFormation()
    {
        $this->_section['experiences']['formation'] = Data::getInstance()->factory('educations');
    }

    /**
     * Construct the career entry according to json file properties
     */
    private function loadCareer()
    {
        $this->_section['experiences']['carrière'] = Data::getInstance()->factory('careers');
    }

    /**
     * Construct the Abilities entry according to json file properties
     */
    private function loadAbilities()
    {
        $this->_section['abilities']['skills'] = Data::getInstance()->factory('skills');
        $this->_section['abilities']['environnement technique'] = Data::getInstance()->factory('tools');
        $this->_section['abilities']['langues'] = Data::getInstance()->factory('languages');
    }

    /**
     * Construct the projects entry according to json file properties
     */
    private function loadProjects()
    {
        $this->_section['projects'] = Data::getInstance()->factory('projects');
    }

    /**
     * Construct the interests entry according to json file properties
     */
    private function loadInterests()
    {
        $this->_section['interests'] = Data::getInstance()->factory('interests');
    }
}
