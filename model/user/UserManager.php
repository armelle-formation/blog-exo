<?php

class UserManager extends AbstractManager
{

    /**
     * Get UserEntity object by id
     * @param int $id
     * @return UserEntity
     * @throws Exception
     */
    public function getUserById(int $id): UserEntity
    {
        $aUser = [];
        $bdd = $this->_db;
        $qUser = $bdd->prepare('SELECT * FROM users WHERE user_id = :id');
        $qUser->bindParam('id', $id, PDO::PARAM_INT);
        $qUser->execute();

        if ($qUser->rowCount() > 0) {
            $aUser = $qUser->fetch(PDO::FETCH_ASSOC);
        }
        return new UserEntity($aUser);
    }


    /**
     * return the best commentator
     * @return array
     * @throws Exception
     */
    public function getTopUser()
    {
        $aReturn = [];
        $aUser = [];
        $bdd = $this->_db;
        $sql = "
            SELECT
                count(c.comment_id) as nb,
                c.user_id
            FROM comments c
            INNER JOIN users u ON c.user_id = u.user_id
            GROUP BY c.user_id
            ORDER BY nb DESC
            LIMIT 1";

        $req = $bdd->prepare($sql);
        $req->execute();
        $oResult = $req->fetch(PDO::FETCH_ASSOC);

        $user = $this->getUserById($oResult['user_id']);
        $nbComments = $oResult['nb'];

        $aReturn['user'] = $user;
        $aReturn['nb'] = $nbComments;

        return $aReturn;
    }

    /**
     * Check if email exists for other user
     * @param string $email
     * @param null|int $id
     * @return int
     */
    public function checkUserExists(string $email, $id = null): int
    {
        $bdd = $this->_db;
        $conditions = "";
        if (!is_null($id)) {
            $conditions = " AND users.user_id != :id";
        }
        $sql = "SELECT * FROM users WHERE lower(email) = lower(:email)" . $conditions . " LIMIT 1";
        $req = $bdd->prepare($sql);
        $req->bindValue(':email', $email, \PDO::PARAM_STR);

        if (!is_null($id)) {
            $req->bindValue(':id', $id, \PDO::PARAM_INT);
        }

        $req->execute();
        return $req->rowCount();
    }

    //TOASK : renv
    /**
     * Get an user Entity email
     * @param string $email
     * @return UserEntity
     */
    public function getUserByEmail(string $email): UserEntity
    {
        $aUser = [];
        $bdd = $this->_db;
        $req = $bdd->prepare("SELECT * FROM users WHERE email = :email");
        $req->bindValue(':email', $email, PDO::PARAM_STR);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aUser = $req->fetch(PDO::FETCH_ASSOC);
        }
        return new UserEntity($aUser);
    }

    /**
     * Get an array of users by role id
     * @param array $array
     * @return array
     */
    public function getUserByRole(array $array): array
    {
        $aUsers = [];
        $bdd = $this->_db;
        $clause = implode(',', array_fill(0, count($array), '?'));
        $req = $bdd->prepare("SELECT * FROM users WHERE role_id IN ($clause)");
        $req->execute($array);

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oUser = new UserEntity($row);
            array_push($aUsers, $oUser);
        }
        return $aUsers;
    }

    /**
     * Returns the number of comments posted per month for the last 7 months
     * @return array
     */
    public function getStatsActiveCommentators(): array
    {
        $stats = [];
        $bdd = $this->_db;
        $sql = "
            SELECT DATE_FORMAT(date, '%M %Y') date, COUNT(u.date_create) as value
            FROM (
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH AS date UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 2 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 3 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 4 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 5 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 6 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 7 MONTH
            ) AS dates
            LEFT JOIN (
              SELECT * FROM users WHERE active = 1 AND role_id = 3
            ) u ON u.date_create >= date AND u.date_create < date + INTERVAL 1 MONTH
            GROUP BY date
        ";

        $req = $bdd->prepare($sql);
        $req->execute();
        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $row['date'] = ucfirst($row['date']);
            array_push($stats, $row);
        }
        return $stats;
    }

    /**
     * Get all users list
     * @return array of user entity
     */
    public function getAllUsers()
    {
        $bdd = $this->_db;
        $aUsers = [];

        $sql = "
            SELECT
                   u.* ,
                   r.role_name
            FROM users u
            INNER JOIN roles r ON u.role_id = r.role_id
        ";
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oUser = new UserEntity($row);
            array_push($aUsers, $oUser);
        }
        return $aUsers;
    }

    /**
     * Customizable function for Users query
     * @param array $array : must be sent only by controller
     * @param string $order
     * @param string $sort
     * @return array
     */
    public function getUsersByProperties(array $array, string $order = 'u.user_id', string $sort = 'ASC'): array
    {
        $bdd = $this->_db;
        $aUsers = [];
        $sql = 'SELECT * FROM users u';

        if (count($array)) {
            $condition = " WHERE ";
            $selectors = [ 'IS NOT NULL', 'IS NULL'];
            $keys = array_keys($array);

            for ($i = 0; $i < count($keys); $i++) {
                $value = $array[$keys[$i]];

                if (in_array(strtoupper($value), $selectors, true)) {
                    $condition .= $keys[$i] . ' ' . $value;
                } else {
                    $condition .= $keys[$i] . " = " . $value;
                }
                $condition .= ($i != (count($keys) -1)) ? " AND " : "";
            }
            $sql .= $condition;
        }

        $sql .= " ORDER BY ".$order . " " . $sort;
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oUser = new UserEntity($row);
            array_push($aUsers, $oUser);
        }

        return $aUsers;
    }

    /**
     * Save an user (redirect to create or update)
     * @param UserEntity $oUser
     * @return int|string|null
     */
    public function saveUser(UserEntity $oUser)
    {
        if (!is_null($oUser->getUserId())) {
            $userId = $oUser->getUserId();
            $this->updateUser($oUser);
        } else {
            $userId = $this->createUser($oUser);
            $oUser->setUserId($userId);
        }
        return $userId;

    }

    /**
     * Create a user, return true if no errors
     * @param UserEntity $oUser
     * @return string
     */
    public function createUser(UserEntity $oUser)
    {
        $sql = "
        INSERT INTO users
          (firstname, lastname, email, password, active, role_id, date_create, image, geo_consent)
        VALUES
            (:firstname, :lastname, :email, :password, :active, :role_id, NOW(), :image, :geo_consent)
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':firstname', $oUser->getFirstname(), PDO::PARAM_STR);
        $req->bindValue(':lastname', $oUser->getLastName(), PDO::PARAM_STR);
        $req->bindValue(':email', $oUser->getEmail(), PDO::PARAM_STR);
        $req->bindValue(':password', $oUser->getPassword(), PDO::PARAM_STR);
        $req->bindValue(':active', $oUser->getActive(), PDO::PARAM_BOOL);
        $req->bindValue(':role_id', $oUser->getRoleId(), PDO::PARAM_INT);
        $req->bindValue(':image', $oUser->getImage(), PDO::PARAM_STR);
        $req->bindValue(':geo_consent', $oUser->getGeoConsent(), PDO::PARAM_INT);

        $req->execute();

        return $bdd->lastInsertId();

    }

    /**
     * Update user based on its ID
     * @param UserEntity $oUser
     * @return bool
     */
    public function updateUser(UserEntity $oUser)
    {
        $sql = "
            UPDATE users SET
                firstname = :firstname,
                lastname = :lastname,
                email = :email,
                password = :password,
                active = :active,
                role_id = :role_id,
                image = :image,
                geo_consent = :geo_consent
            WHERE user_id = :id
            LIMIT 1
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':id', $oUser->getUserId(), PDO::PARAM_INT);
        $req->bindValue(':firstname', $oUser->getFirstname(), PDO::PARAM_STR);
        $req->bindValue(':lastname', $oUser->getLastName(), PDO::PARAM_STR);
        $req->bindValue(':email', $oUser->getEmail(), PDO::PARAM_STR);
        $req->bindValue(':password', $oUser->getPassword(), PDO::PARAM_STR);
        $req->bindValue(':active', $oUser->getActive(), PDO::PARAM_BOOL);
        $req->bindValue(':role_id', $oUser->getRoleId(), PDO::PARAM_INT);
        $req->bindValue(':image', $oUser->getImage(), PDO::PARAM_STR);
        $req->bindValue(':geo_consent', $oUser->getGeoConsent(), PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Save cookie User
     * @param int $user_id
     * @param string $cookie
     */
    public function saveLogCookie(int $user_id, string $cookie)
    {
        $bdd = $this->_db;
        $qCookie = $bdd->prepare('UPDATE users SET cookie = :cookie WHERE user_id = :user_id');

        $qCookie->bindParam('user_id', $user_id, PDO::PARAM_INT);
        $qCookie->bindParam('cookie', $cookie, PDO::PARAM_STR);
        $qCookie->execute();
    }

    /**
     * get cookie User
     * @param string $cookieCrypted
     * @return array
     */
    public function getLogCookie(string $cookieCrypted)
    {
        $bdd = $this->_db;
        $qCookie = $bdd->prepare('SELECT user_id FROM users WHERE cookie = :cookie');
        $qCookie->bindParam('cookie', $cookieCrypted, PDO::PARAM_STR);
        $qCookie->execute();

        return $qCookie->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Change status of user
     * @param UserEntity $oUser
     * @return bool
     * @throws BlogException
     */
    public function changeStatusUser(UserEntity $oUser)
    {
        $bdd = $this->_db;

        //Check if user exists
        if (is_a($oUser, 'UserEntity')) {
            $status = !$oUser->getActive();
            $id = $oUser->getUserId();

            $req = $bdd->prepare('UPDATE users SET active = :status WHERE user_id = :id LIMIT 1');
            $req->bindParam(':status', $status, \PDO::PARAM_BOOL);
            $req->bindParam(':id', $id, \PDO::PARAM_INT);

            return $req->execute();

        } else {
            throw new BlogException(
                "Forbidden",
                403,
                "Données utilisateur incorrectes",
                'ArgumentsException',
                __FUNCTION__
            );
        }
    }
}
