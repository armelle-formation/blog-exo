<?php


class UserService
{

    /**
     * Check if current session is an admin session
     * @return bool
     */
    public function isAdminSession()
    {
        $return =  false;
        $session = Session::getInstance();
        if ($session->getValue('role') == ID_ADMIN) {
            $return = true;
        }
        return $return;
    }

    /**
     * Check if current session is an editor session
     * @return bool
     */
    public function isEditorSession()
    {
        $return =  false;
        $session = Session::getInstance();
        if ($session->getValue('role') == ID_EDITOR) {
            $return = true;
        }
        return $return;
    }

    /**
     * Check if current session is an front reader session
     * @return bool
     */
    public function isReaderSession()
    {
        $return =  false;
        $session = Session::getInstance();
        if ($session->getValue('role') == ID_READER) {
            $return = true;
        }
        return $return;
    }

    /**
     * Check if current session is expired
     * @return bool
     * @throws BlogException
     */
    public function checkSession()
    {
        $session = Session::getInstance();

        $timeoutIdle = $session->getValue('timeout_idle');
        $sessionInit= $session->getValue('initialize');

        $org = in_array($session->getValue('role'), [ID_ADMIN,ID_EDITOR]) ? 'admin' : 'front';
        $param = [
            "org" => $org
        ];

        if (!is_integer($timeoutIdle) || $timeoutIdle < time() || is_null($sessionInit)) {
            $this->logout($param);
        }
        if (!$this->checkTicketCookie()) {
            $this->logout($param);
        }
        return true;
    }

    /**
     * Check if current session is an authorized session for BackOffice
     * and Cookie ticket is egal to Session ticket
     * @return bool
     */
    public function isBackSession()
    {
        $ret = false;
        if ($this->isAdminSession() || $this->isEditorSession()) {
            $ret = true;
        }
        return $ret;
    }

    /**
     * Check if current session is an authorized session FrontOffice
     * @return bool
     */
    public function isPublicSession()
    {
        $ret = false;
        if ($this->isReaderSession()) {
            $ret = true;
        }
        return $ret;
    }

    /**
     * Check if cookie ticket and session ticket have equal values
     * @return bool
     * @throws BlogException
     */
    public function checkTicketCookie()
    {

        $return = false;

        // Session ticket
        $session = Session::getInstance();
        $sessionTicket = $session->getValue('sm_t');

        // Cookie ticket
        if (!isset($_COOKIE['sm_t'])) {
            return false;
        } else {
            $cookieTicket = $_COOKIE['sm_t'];
        }

        // Tickets equal ? Regenerate ticket for next pages
        if ($cookieTicket == $sessionTicket) {

            $ticket = Key::generateTicket();

            // Cookie
            setcookie('sm_t', $ticket, time()+ (60*20), '/', SERVER_BASE);

            //Session
            $_SESSION['ticket'] = $ticket;

            $return = true;
        }
        return $return;
    }

    /**
     * Logout user and then redirect to the admin login page or front blog page
     * @param array $get
     */
    public function logout(array $get)
    {

        //Decode origin
        $origin = isset($get['org']) ? $get['org'] : 'front';
        $redirect = array_key_exists('redirect', $get) ? boolval($get['redirect']) : boolval(1);

        //Update session Class
        $session = Session::getInstance();
        unset($session);

        //Destroy session
        session_unset();
        session_destroy();
        $_SESSION = array();

        //Destroy cookies
        setcookie("keep_log", "", time()-3600);
        setcookie("sm_t", "", time()-3600);

        if ($redirect) {
            if ($origin == 'admin') {
                header('Location:' . SERVER_BASE . 'admin');
                exit();
            }
            header('Location: '. SERVER_BASE . 'blog');
            exit();
        }
    }
}
