<?php

class UserEntity extends AbstractEntity
{
    /**
     * @var int
     */
    private $_user_id;

    /**
     * @var string
     */
     private $_firstname;

    /**
     * @var string
     */
    private $_lastname;

    /**
     * @var string
     */
    private $_fullname;

    /**
     * @var string
     */
    private $_email;

    /**
     * @var string
     */
    private $_password;

    /**
     * @var boolean
     */
    private $_active;

    /**
     * @var datetime
     */
    private $_date_create;

    /**
     * @var datetime
     */
    private $_date_update;

    /**
     * @var integer
     */
    private $_role_id;

    /**
     * @var string
     */
    private $_role_name;

    /**
     * @var string
     */
    private $_image;

    /**
     * @var boolean
     */
    private $_geo_consent;


    /**
     * @return integer
     */
    public function getUserId(): ?int
    {
        return $this->_user_id;
    }

    /**
     * @param int $id
     */
    public function setUserId($id)
    {
        $this->_user_id = (int) $id;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->_firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->_lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->_lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->_fullname = $this->getFirstname() . " " . $this->getLastname();
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * @param $email
     */
    public function setEmail($email)
    {
        $this->_email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * @return boolean
     */
    public function getActive()
    {
        return $this->_active ;
    }

    /**
     * @param $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * @return datetime
     */
    public function getDateCreate()
    {
        return $this->_date_create;
    }

    /**
     * @param $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->_date_create = $dateCreate;
    }

    /**
     * @return datetime
     */
    public function getDateUpdate()
    {
        return $this->_date_update;
    }

    /**
     * @param $dateUpdate
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->_date_update = $dateUpdate;
    }

    /**
     * @return integer
     */
    public function getRoleId()
    {
        return $this->_role_id;
    }

    /**
     * @param integer $role
     */
    public function setRoleId(int $role)
    {
        $this->_role_id = $role;
    }

    /**
     * @return string
     */
    public function getRoleName()
    {
        return $this->_role_name;
    }

    /**
     * @param $role
     */
    public function setRoleName($role)
    {
        $this->_role_name = $role;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * @param string|null $imageFile
     */
    public function setImage(?string $imageFile)
    {
        $this->_image = $imageFile;
    }

    /**
     * @return int|null
     */
    public function getGeoConsent(): ?int
    {
        return $this->_geo_consent;
    }

    /**
     * @param int|null $geo_consent
     */
    public function setGeoConsent(?int $geo_consent): void
    {
        $this->_geo_consent = $geo_consent;
    }
}
