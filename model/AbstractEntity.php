<?php

class AbstractEntity implements JsonSerializable
{

    /**
     * AbstractEntity constructor.
     * @param array $aData
     */
    public function __construct(array $aData = [])
    {
        if (!empty($aData)) {
            $this->hydrate($aData);
        }

    }

    /**
     * Hydrate a model object
     * @param array $data
     */
    public function hydrate(array $data)
    {
        foreach ($data as $key => $value) {
            $key = str_replace("_", "", $key);
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * Specific hydrate function for resume object
     * @param array $aData
     * @return $this
     */
    protected function extend(array $aData)
    {
        foreach ($aData as $field => $value) {
            $this->$field = $value;
        }
        return $this;
    }

    /**
     * Overload isset function
     * Must be defined to allow implements jSonSerialize overload function
     * @param $property
     * @return bool
     */
    public function __isset($property)
    {
        return isset($this->$property);
    }

    /**
     * Overload JSON ENCODE php function
     * Allow to access to private properties of an object during JSON serialization
     * Use for example to pre-fill a form in the case of for ajax call
     * @return array|mixed
     * @throws ReflectionException
     */
    public function jsonSerialize()
    {
        $reflectionClass = new ReflectionClass(get_class($this));
        $array = array();

        foreach ($reflectionClass->getProperties() as $property) {
            $property->setAccessible(true);
            $array[$property->getName()] = $property->getValue($this);
            $property->setAccessible(false);
        }
        return $array;
    }
}
