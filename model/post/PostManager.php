<?php

/**
 * Class PostManager
 */
class PostManager extends AbstractManager
{

    /**
     * Get PostEntity object by id with lazyloads
     * @param int $id
     * @param bool $getUser
     * @param bool $getCats
     * @param bool $getTags
     * @param bool $getComments
     * @param int|null $statusIdComment
     * @return PostEntity
     * @throws Exception
     */
    public function getPostById(
        int $id,
        bool $getUser = true,
        bool $getCats = true,
        bool $getTags = false,
        bool $getComments = false,
        ?int $statusIdComment = null
    ) : PostEntity
    {

        $aPost = [];
        $bdd = $this->_db;
        $sql = "
            SELECT
              p.*,
              GROUP_CONCAT(DISTINCT  c.category_id) as categories_ids,
              GROUP_CONCAT(DISTINCT  t.tag_id) as tags_ids
            FROM posts p
              INNER JOIN users u on p.user_id = u.user_id
              INNER JOIN post_category pc on p.post_id = pc.post_id
              INNER JOIN categories c on pc.category_id = c.category_id
              LEFT OUTER JOIN post_tag pt on p.post_id = pt.post_id
              LEFT OUTER JOIN tags t on pt.tag_id = t.tag_id
              WHERE p.post_id = :id
         ";

        $qPost = $bdd->prepare($sql);
        $qPost->bindParam('id', $id, PDO::PARAM_INT);
        $qPost->execute();
        if ($qPost->rowCount() > 0) {
            $aPost = $qPost->fetch(PDO::FETCH_ASSOC);
        }
        $oPost = new PostEntity($aPost);

        if (!is_null($oPost->getPostId()) && $oPost->getPostId()) {
            if ($getCats) {
                $this->setCategoriesToPost($oPost, $aPost['categories_ids']);
            }
            if ($getTags) {
                $this->setTagsToPost($oPost, $aPost['tags_ids']);
            }
            if ($getUser) {
                $this->setUserToPost($oPost, $aPost['user_id']);
            }
            if ($getComments) {
                $this->setCommentsToPost($oPost, $statusIdComment);
            }
        }

        return $oPost;
    }

    /**
     * Return post with previous Id
     * @param int $postId
     * @return PostEntity
     */
    public function getPreviousPost(int $postId): PostEntity
    {
        $aPost = [];
        $bdd = $this->_db;
        $sql = "SELECT p.title, p.post_id FROM posts p WHERE p.post_id < :id LIMIT 1";

        $qPost = $bdd->prepare($sql);
        $qPost->bindParam('id', $postId, PDO::PARAM_INT);
        $qPost->execute();

        if ($qPost->rowCount() > 0) {
            $aPost = $qPost->fetch(PDO::FETCH_ASSOC);
        }

        return new PostEntity($aPost);
    }

    /**
     * Return post with next Id
     * @param int $postId
     * @return PostEntity
     */
    public function getNextPost(int $postId): PostEntity
    {
        $aPost = [];
        $bdd = $this->_db;
        $sql = "SELECT p.title, p.post_id FROM posts p WHERE p.post_id > :id LIMIT 1";

        $qPost = $bdd->prepare($sql);
        $qPost->bindParam('id', $postId, PDO::PARAM_INT);
        $qPost->execute();
        if ($qPost->rowCount() > 0) {
            $aPost = $qPost->fetch(PDO::FETCH_ASSOC);
        }
        return new PostEntity($aPost);
    }

    /**
     * Return most commented post (ranking by the number of views)
     * @param int $statusIdComment
     * @return PostEntity
     * @throws Exception
     */
    public function getTopPost(int $statusIdComment): PostEntity
    {
        $aPost = [];
        $bdd = $this->_db;
        $sql = "
          SELECT
            p.title,
            p.post_id,
            p.user_id,
            p.date_create,
            p.count
        FROM posts p
          LEFT JOIN comments c ON c.post_id = p.post_id
          INNER JOIN post_category pc ON p.post_id = pc.post_id
          INNER JOIN (SELECT * FROM categories WHERE is_private = 0) cat ON pc.category_id = cat.category_id
        WHERE
              p.active = 1 AND
              c.status_id = 2
        GROUP BY p.title, p.post_id, p.user_id, pc.category_id
        ORDER BY p.count DESC LIMIT 1";

        $qPost = $bdd->prepare($sql);
        $qPost->execute();
        if ($qPost->rowCount() > 0) {
            $aPost = $qPost->fetch(PDO::FETCH_ASSOC);
        }
        $oPost = new PostEntity($aPost);

        if (!is_null($oPost->getPostId()) && $oPost->getPostId()) {
            $this->setUserToPost($oPost, $aPost['user_id']);
            $this->setCommentsToPost($oPost, $statusIdComment);
        }
        return $oPost;
    }

    /**
     * Admin interface : get number of posts published in the last of 6 months
     * @return array
     */
    public function getStatsPostsPublished(): array
    {

        $aStats = [];
        $bdd = $this->_db;
        $sql = "
            SELECT date, COUNT(p.date_publish) as value
            FROM (
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH AS date UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 2 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 3 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 4 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 5 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 6 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 7 MONTH
            ) AS dates
            LEFT JOIN posts p ON p.date_publish >= date AND p.date_publish < date + INTERVAL 1 MONTH
            GROUP BY date
        ";

        $req = $bdd->prepare($sql);
        $req->execute();
        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            array_push($aStats, $row);
        }
        return $aStats;
    }

    /**
     * Return the number of posts by category
     * @return array
     */
    public function getPostsByCategory()
    {

        $aStats = [];
        $bdd = $this->_db;
        $sql = "
            SELECT
                   c.category_id as id,
                   c.name as category,
                   count(DISTINCT p.post_id) as value
            FROM
                posts p
                INNER JOIN post_category pc ON p.post_id = pc.post_id
                INNER JOIN categories c ON pc.category_id = c.category_id
            GROUP BY c.category_id,c.name
            ORDER BY id ASC
        ";

        $req = $bdd->prepare($sql);
        $req->execute();
        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            array_push($aStats, $row);
        }
        return $aStats;
    }

    /**
     * Admin part : list of all posts with user, cat and tags objects
     * @param bool $isAdmin
     * @param bool $online
     * @return array
     * @throws Exception
     */
    public function getAllPostBlog(bool $isAdmin = false, bool $online = false)
    {
        $bdd = $this->_db;
        $aPosts = [];
        $where = (!$isAdmin) ?  'c.category_id_parent = 1' : '1 = 1';
        $where .= ($online) ?  ' AND p.active = 1' : '';
        $sql = "
            SELECT
                p.*,
                GROUP_CONCAT(DISTINCT  c.category_id) as categories_ids,
                GROUP_CONCAT(DISTINCT  t.tag_id) as tags_ids
            FROM posts p
                INNER JOIN users u on p.user_id = u.user_id
                INNER JOIN post_category pc on p.post_id = pc.post_id
                INNER JOIN categories c on pc.category_id = c.category_id
                LEFT OUTER JOIN post_tag pt on p.post_id = pt.post_id
                LEFT OUTER JOIN tags t on pt.tag_id = t.tag_id
            WHERE " .  $where . "
            GROUP BY p.post_id
        ";
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oPost = new PostEntity($row);

            $this->setCategoriesToPost($oPost, $row['categories_ids']);
            $this->setTagsToPost($oPost, $row['tags_ids']);
            $this->setUserToPost($oPost, $row['user_id']);

            array_push($aPosts, $oPost);
        }
        return $aPosts;
    }

    /**
     * Customizable function for Post query (with tag not implemented)
     * @param array $array : must be sent only by controller
     * @param string $order
     * @param string $sort
     * @param bool $getUser
     * @param bool $getCats
     * @param bool $getTags
     * @param bool $getComments
     * @param int|NULL $limit
     * @param int|NULL $offset
     * @param int|NULL $statusIdComment
     * @return array
     * @throws Exception
     */
    public function getPostsByProperties(
        array $array,
        string $order = 'p.post_id',
        string $sort = 'ASC',
        bool $getUser = true,
        bool $getCats = false,
        bool $getTags = false,
        bool $getComments = false,
        int $limit = null,
        int $offset = null,
        int $statusIdComment = null
    ): array
    {
        $bdd = $this->_db;
        $aPosts = [];

        $aggregateFields = "SQL_CALC_FOUND_ROWS p.count";
        $fields = "
            p.post_id,
            p.title,
            p.chapo,
            p.content,
            p.active,
            p.date_publish,
            p.date_create,
            p.date_update,
            p.date_last_access,
            p.image_file,
            p.is_highlighted,
            p.user_id
         ";
        $groupBy = ($getTags || $getCats) ? " GROUP BY ". $fields : "";
        $fields .= $getCats ? ",GROUP_CONCAT(DISTINCT c.category_id) as categories_ids" : "";
        $fields .= $getTags ? ",GROUP_CONCAT(DISTINCT  t.tag_id) as tags_ids" : "";
        $sql = 'SELECT ' . $aggregateFields. "," .$fields .' FROM posts p';

        if ($getCats) {
            $sql .= ' INNER JOIN post_category pc on p.post_id = pc.post_id';
            $sql .= ' INNER JOIN categories c on pc.category_id = c.category_id';
        }

        if ($getTags) {
            $sql .= ' LEFT OUTER JOIN post_tag pt on p.post_id = pt.post_id';
            $sql .= ' LEFT OUTER JOIN tags t on pt.tag_id = t.tag_id';
        }

        if (count($array)) {
            $condition = " WHERE ";
            $selectors = [ 'IS', '<='];
            $keys = array_keys($array);

            for ($i = 0; $i < count($keys); $i++) {
                $value = $array[$keys[$i]];

                if (in_array(strtoupper(substr($value, 0, 2)), $selectors, true)) {
                    $condition .= $keys[$i] . ' ' . $value;
                } else {
                    $condition .= $keys[$i] . " = " . $value;
                }
                $condition .= ($i != (count($keys) -1)) ? " AND " : "";
            }
            $sql .= $condition;
        }

        $sql .= $groupBy;
        $sql .= " ORDER BY ".$order . " " . $sort;
        $sql .= (!is_null($limit)) ? " LIMIT " . $limit : "";
        $sql .= (!is_null($offset)) ? " OFFSET " . $offset : "";
        $req = $bdd->prepare($sql);
        $req->execute();

        $resultFoundRows = $bdd->query('SELECT found_rows()');
        $nbTotalelements = $resultFoundRows->fetchColumn();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oPost = new PostEntity($row);
            if ($getCats) {
                $this->setCategoriesToPost($oPost, $row['categories_ids']);
            }
            if ($getTags) {
                $this->setTagsToPost($oPost, $row['tags_ids']);
            }
            if ($getUser) {
                $this->setUserToPost($oPost, $row['user_id']);
            }
            if ($getComments) {
                $this->setCommentsToPost($oPost, $statusIdComment);
            }

            array_push($aPosts, $oPost);
        }

        return [
            'aPosts' => $aPosts,
            'nbTotal' => $nbTotalelements
        ];
    }

    /**
     * Return a list of posts for one tag
     * @param int $idTag
     * @return array
     * @throws Exception
     */
    public function getPostsByTagId(int $idTag): array
    {
        $bdd = $this->_db;
        $aPosts = [];
        $fields = "
            p.post_id,
            p.title,
            p.chapo,
            p.content,
            p.active,
            p.date_publish,
            p.date_create,
            p.date_update,
            p.date_last_access,
            p.image_file,
            p.is_highlighted,
            p.user_id
        ";
        $sql =
            "SELECT " . $fields . "
               ,GROUP_CONCAT(DISTINCT  c.category_id) as categories_ids
            FROM posts p
                INNER JOIN post_category pc on p.post_id = pc.post_id
                INNER JOIN categories c on pc.category_id = c.category_id
            WHERE p.post_id IN
                (select pt.post_id from post_tag pt where pt.tag_id = :tagId)
            GROUP BY "  . $fields . " ORDER BY p.date_publish DESC";

        $req = $bdd->prepare($sql);
        $req->bindParam('tagId', $idTag, PDO::PARAM_INT);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oPost = new PostEntity($row);
            $this->setCategoriesToPost($oPost, $row['categories_ids']);

            array_push($aPosts, $oPost);
        }

        return $aPosts;
    }


    /**
     * Categories du blog uniquement
     * @param PostEntity $oPost
     * @return mixed
     */
    public function savePost(PostEntity $oPost)
    {
        if (!is_null($oPost->getPostId())) {
            $postId = $oPost->getPostId();
            $this->updatePost($oPost);
        } else {
            $postId = $this->createPost($oPost);
            $oPost->setPostId($postId);
        }

        //Cats
        $categoryManager = new CategoryManager($this->_db);
        $categoryManager->savePostCategory($oPost);

        //Tags
        $tagManager = new TagManager($this->_db);
        $tagManager->savePostTag($oPost);

        return $postId;
    }

    /**
     * Create a blog post, return true if no errors
     * @param PostEntity $oPost
     * @return bool
     */
    public function createPost(PostEntity $oPost)
    {
        $sql = "
        INSERT INTO posts (
            title,
            chapo,
            content,
            image_file,
            active,
            date_create,
            date_publish,
            date_update,
            date_last_access,
            is_highlighted,
            count,
            user_id
        ) VALUES (
            :title,
            :chapo,
            :content,
            :image,
            :active,
            NOW(),
            :date_publish,
            NOW(),
            NULL,
            :highlighted,
            :count,
            :id_user
        )
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':title', $oPost->getTitle(), \PDO::PARAM_STR);
        $req->bindValue(':chapo', $oPost->getChapo(), \PDO::PARAM_STR);
        $req->bindValue(':content', $oPost->getContent(), \PDO::PARAM_LOB);
        $req->bindValue(':image', $oPost->getImageFile(), \PDO::PARAM_STR);
        $req->bindValue(':active', $oPost->getActive(), \PDO::PARAM_BOOL);
        $req->bindValue(':date_publish', $oPost->getDatePublish(), \PDO::PARAM_STR);
        $req->bindValue(':highlighted', $oPost->getIsHighlighted(), \PDO::PARAM_BOOL);
        $req->bindValue(':count', $oPost->getCount(), \PDO::PARAM_INT);
        $req->bindValue(':id_user', $oPost->getUser()->getUserId(), \PDO::PARAM_INT);

        $req->execute();

        return $bdd->lastInsertId();

    }

    /**
     * Update blog post's data based on its ID
     * @param PostEntity $oPost
     * @return bool
     */
    public function updatePost(PostEntity $oPost)
    {
        $sql = "
            UPDATE posts SET
                title = :title,
                chapo = :chapo,
                content = :content,
                image_file = :image,
                active = :active,
                date_publish = :date_publish,
                date_update = :date_update,
                date_last_access = :date_last_access,
                is_highlighted = :highlighted,
                count = :count,
                user_id = :id_user
            WHERE post_id = :id
            LIMIT 1
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':id', $oPost->getPostId(), \PDO::PARAM_INT);
        $req->bindValue(':title', $oPost->getTitle(), \PDO::PARAM_STR);
        $req->bindValue(':chapo', $oPost->getChapo(), \PDO::PARAM_STR);
        $req->bindValue(':content', $oPost->getContent(), \PDO::PARAM_LOB);
        $req->bindValue(':image', $oPost->getImageFile(), \PDO::PARAM_STR);
        $req->bindValue(':active', $oPost->getActive(), \PDO::PARAM_BOOL);
        $req->bindValue(':date_publish', $oPost->getDatePublish(), \PDO::PARAM_STR);
        $req->bindValue(':date_update', $oPost->getDateUpdate(), \PDO::PARAM_STR);
        $req->bindValue(':date_last_access', $oPost->getDateLastAccess(), \PDO::PARAM_STR);
        $req->bindValue(':highlighted', $oPost->getIsHighlighted(), \PDO::PARAM_BOOL);
        $req->bindValue(':count', $oPost->getCount(), \PDO::PARAM_STR);
        $req->bindValue(':id_user', $oPost->getUser()->getUserId(), \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Return number of all posts
     * @return mixed
     * Get total number of blog posts
     */
    public function getNumberOfPosts()
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('SELECT COUNT(*) FROM posts WHERE active = 1');
        $req->execute();

        return $req->fetchColumn();
    }

    /**
     * Delete a post and his categories
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function deletePost(int $id)
    {

        $bdd = $this->_db;
        $oPost = $this->getPostById($id);

        $oCategoryManager = new CategoryManager($bdd);
        $oCategoryManager->deletePostCategory($oPost);

        $req = $bdd->prepare('DELETE FROM posts WHERE post_id = :id LIMIT 1');
        $req->bindParam(':id', $id, \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Fill Category object in Post object (lazyload)
     * @param PostEntity $oPost
     * @param string $sCatsIds
     * @throws Exception
     */
    public function setCategoriesToPost(PostEntity $oPost, string $sCatsIds)
    {
        $aCats = [];
        $aCategoriesIds = explode(",", $sCatsIds);

        $catManager = new CategoryManager($this->_db);

        if (count($aCategoriesIds)) {
            foreach ($aCategoriesIds as $cat) {
                $oCat = $catManager->getCatById($cat);
                array_push($aCats, $oCat);
            }
            $oPost->setCategories($aCats);
        }
    }

    /**
     * Fill Tag object in Post object (lazyload)
     * @param PostEntity $oPost
     * @param string|null $sTagsIds
     * @throws Exception
     */
    public function setTagsToPost(PostEntity $oPost, ?string $sTagsIds)
    {
        $aTags = [];
        $aTagsIds = (isset($sTagsIds)) ? explode(",", $sTagsIds) : [];
        $tagManager = new TagManager($this->_db);

        if (count($aTagsIds)) {
            foreach ($aTagsIds as $tag) {
                $oTag = $tagManager->getTagById($tag);
                array_push($aTags, $oTag);
            }
            $oPost->setTags($aTags);
        }
    }

    /**
     * Fill User object in Post object (lazyload)
     * @param PostEntity $oPost
     * @param int $userId
     * @throws Exception
     */
    public function setUserToPost(PostEntity $oPost, int $userId)
    {
        $userManager = new UserManager($this->_db);
        $oUser = $userManager->getUserById($userId);
        $oPost->setUser($oUser);
    }

    /**
     * Fill Category object in Post object (lazyload)
     * @param PostEntity $oPost
     * @param int|NULL $statusComment
     * @throws Exception
     */
    public function setCommentsToPost(PostEntity $oPost, int $statusComment = null)
    {
        $params = ['post_id' => $oPost->getPostId(), 'user_id' => 'IS NOT NULL'];
        if (!is_null($statusComment)) {
            $params['c.status_id'] = COMMENT_ACTIVE;
        }

        //Get all comments for post
        $commentManager = new CommentManager($this->_db);
        $aComments = $commentManager->getCommentsByProperties(
            $params,
            true,
            true,
            false,
            'c.date_update',
            'DESC'
        );
        $oPost->setComments($aComments);
    }
}
