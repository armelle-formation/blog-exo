<?php

class PostEntity extends AbstractEntity
{

    /**
     * @var int
     */
    private $_post_id;

    /**
     * @var string
     */
    private $_title;

    /**
     * @var string
     */
    private $_chapo;

    /**
     * @var string
     */
    private $_content;

    /**
     * @var boolean
     */
    private $_active;

    /**
     * @var string
     */
    private $_date_create;

    /**
     * @var string
     */
    private $_date_publish;

    /**
     * @var string
     */
    private $_date_update;

    /**
     * @var string
     */
    private $_date_last_access;

    /**
     * @var string
     */
    private $_image_file;

    /**
     * @var boolean
     */
    private $_is_highlighted;

    /**
     * @var integer
     */
    private $_count = 0;

    /**
     * @var UserEntity
     */
    private $_user;

    /**
     * @var array
     */
    private $_categories;

    /**
     * @var array
     */
    private $_tags = [];

    /**
     * @var array
     */
    private $_comments = [];


    // SETTERS - GETTERS

    /**
     * @return int|null
     */
    public function getPostId(): ?int
    {
        return $this->_post_id;
    }

    /**
     * @param $id
     */
    public function setPostId($id)
    {
        $this->_post_id = (int)$id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * @param $title
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }

    /**
     * @return string
     */
    public function getChapo()
    {
        return $this->_chapo;
    }

    /**
     * @param $chapo
     */
    public function setChapo($chapo)
    {
        $this->_chapo = $chapo;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->_content;
    }

    /**
     * @param $content
     */
    public function setContent($content)
    {
        $this->_content = $content;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * @param $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * @return string|null
     */
    public function getDatePublish(): ?string
    {
        return $this->_date_publish;
    }

    /**
     * @param string|null $datePublish
     */
    public function setDatePublish(?string $datePublish): void
    {
        $this->_date_publish = $datePublish;
    }

    /**
     * @return string|null
     */
    public function getDateCreate(): ?string
    {
        return $this->_date_create;
    }

    /**
     * @param string|null $dateCreate
     */
    public function setDateCreate(?string $dateCreate): void
    {
        $this->_date_create = $dateCreate;
    }

    /**
     * @return string|null
     */
    public function getDateUpdate(): ?string
    {
        return $this->_date_update;
    }

    /**
     * @param string|null $dateUpdate
     */
    public function setDateUpdate(?string $dateUpdate): void
    {
        $this->_date_update = $dateUpdate;
    }

    /**
     * @return string
     */
    public function getDateLastAccess(): ?string
    {
        return $this->_date_last_access;
    }

    /**
     * @param string $date_last_access
     */
    public function setDateLastAccess(?string $date_last_access): void
    {
        $this->_date_last_access = $date_last_access;
    }

    /**
     * @return string
     */
    public function getImageFile()
    {
        return $this->_image_file;
    }

    /**
     * @param $imageFile
     */
    public function setImageFile($imageFile)
    {
        $this->_image_file = $imageFile;
    }

    /**
     * @return bool
     */
    public function getIsHighlighted()
    {
        return $this->_is_highlighted;
    }

    /**
     * @param $isHighlighted
     */
    public function setIsHighlighted($isHighlighted)
    {
        $this->_is_highlighted = $isHighlighted;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @param $count
     */
    public function setCount($count)
    {
        $this->_count = $count;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): ?UserEntity
    {
        return $this->_user;
    }

    /**
     * @param UserEntity $oUser
     */
    public function setUser(?UserEntity $oUser)
    {
        $this->_user = $oUser;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->_categories;
    }

    /**
     * @param $aCategories
     */
    public function setCategories($aCategories)
    {
        $this->_categories = $aCategories;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->_tags;
    }

    /**
     * @param array $aTags
     */
    public function setTags(array $aTags)
    {
        if (count($aTags)) {
            $this->_tags = $aTags;
        }
    }

    /**
     * @return array
     */
    public function getComments(): array
    {
        return $this->_comments;
    }

    /**
     * @param array $aComments
     */
    public function setComments(array $aComments): void
    {
        if (count($aComments)) {
            $this->_comments = $aComments;
        }

    }
}
