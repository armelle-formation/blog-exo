<?php


class TagManager extends AbstractManager
{

    /**
     * Get TagEntity object by id
     * @param int $tagId
     * @param bool $getUser
     * @param bool $getPosts
     * @return TagEntity
     * @throws Exception
     */
    public function getTagById(int $tagId, bool $getUser = true, bool $getPosts = false) : TagEntity
    {
        $aTag = [];
        $bdd = $this->_db;
        $sql = "
            SELECT
              t.*,
              GROUP_CONCAT(DISTINCT  pt.post_id) as posts_ids
            FROM tags t
            RIGHT OUTER JOIN post_tag pt ON t.tag_id = pt.tag_id
            WHERE t.tag_id = :id
         ";
        $req = $bdd->prepare($sql);
        $req->bindParam('id', intval($tagId), PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aTag = $req->fetch(PDO::FETCH_ASSOC);
        }
        $oTag = new TagEntity($aTag);

        if (!is_null($oTag->getTagId()) && $oTag->getTagId()) {
            if ($getUser) {
                $this->setUserToTag($oTag, (int)$aTag['user_id']);
            }
            if ($getPosts) {
                $this->setPostsToTag($oTag, $aTag['posts_ids']);
            }
        }

        return $oTag;
    }

    /**
     * Get Tags with posts by properties
     * @param array $array
     * @param string $order
     * @param string $sort
     * @param bool $getUser
     * @param bool $getPosts
     * @param string $limit
     * @return array
     * @throws Exception
     */
    public function getTagsByProperties(
        array $array = [],
        string $order = 't.tag_id',
        string $sort = 'ASC',
        bool $getUser = true,
        bool $getPosts = false,
        string $limit = ""
    ) {
        $bdd = $this->_db;
        $aTags = [];
        $fields = "t.tag_id,t.name,t.user_id,t.date_create";
        $groupBy = " GROUP BY " . $fields;

        $fields .= ($getPosts) ? ",GROUP_CONCAT(DISTINCT pt.post_id) as posts_ids,COUNT(DISTINCT pt.post_id) as nbPosts" : "";
        $sql = "SELECT " . $fields . " FROM tags t";

        if ($getPosts) {
            $sql .= ' RIGHT OUTER JOIN post_tag pt ON t.tag_id = pt.tag_id  ';
        }

        if (count($array)) {
            $sql .= " WHERE ";
            $counter = 0;
            $selectors = [
                'IS',
                '<='
            ];

            foreach ($array as $field => $value) {
                if (in_array(substr($value, 0, 2), $selectors, true)) {
                    $sql .= $field . ' ' . $value;
                } else {
                    $sql .= $field . " = " . $value;
                }
                if ($counter != count($array) -1) {
                    $sql .= " AND ";
                }
                $counter++;
            }
        }

        $sql .= $groupBy;
        $sql .= " ORDER BY ".$order . " " . $sort;
        $sql .= (!empty($limit)) ? " LIMIT " . $limit : "";

        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oTag = new TagEntity($row);
            if ($getUser) {
                $this->setUserToTag($oTag, $row['user_id']);
            }
            if ($getPosts) {
                $this->setPostsToTag($oTag, $row['posts_ids']);
            }
            array_push($aTags, $oTag);
        }
        return $aTags;
    }

    /**
     * Return all tags
     * @param bool $getUser
     * @param bool $getPosts
     * @return array
     * @throws Exception
     */
    public function getAllTags(bool $getUser = true, bool $getPosts = false)
    {
        $bdd = $this->_db;
        $aTags = [];
        $sql = "
            SELECT
                t.*,
                GROUP_CONCAT(pt.post_id) as posts_ids
            FROM tags t
              LEFT JOIN post_tag pt ON t.tag_id = pt.tag_id
            GROUP BY t.tag_id
            ORDER BY name
        ";

        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oTag = new TagEntity($row);
            if ($getUser) {
                $this->setUserToTag($oTag, $row['user_id']);
            }
            if ($getPosts) {
                $this->setPostsToTag($oTag, $row['posts_ids']);
            }

            array_push($aTags, $oTag);
        }

        return $aTags;
    }

    /**
     * Save Tag in database (redirect to create or update)
     * @param TagEntity $oTag
     * @return int
     */
    public function saveTag(TagEntity $oTag)
    {
        if (!is_null($oTag->getTagId())) {
            $TagId = $oTag->getTagId();
            $this->updateTag($oTag);
        } else {
            $TagId = $this->createTag($oTag);
        }
        return $TagId;
    }

    /**
     * Create a blog category, return id if no errors
     * @param $oTag TagEntity
     * @return integer
     */
    public function createTag(TagEntity $oTag)
    {

        $sql = "
        INSERT INTO tags
          (name, user_id)
        VALUES
            (:name, :userId)
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':name', $oTag->getName(), \PDO::PARAM_STR);
        $req->bindValue(':userId', $oTag->getUser()->getUserId(), \PDO::PARAM_INT);

        $req->execute();

        return $bdd->lastInsertId();

    }

    /**
     * Update blog category's data based on its ID
     * @param TagEntity $oTag
     * @return bool
     */
    public function updateTag(TagEntity $oTag)
    {
        $sql = "
            UPDATE tags SET
                name = :name,
                user_id = :userId
            WHERE tag_id = :id
            LIMIT 1
        ";


        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':id', $oTag->getTagId(), \PDO::PARAM_INT);
        $req->bindValue(':name', $oTag->getName(), \PDO::PARAM_STR);
        $req->bindValue(':userId', $oTag->getUser()->getUserId(), \PDO::PARAM_INT);
        return $req->execute();
    }

    /**
     * Update blog post's data based on its ID
     * @param PostEntity $oPost
     */
    public function savePostTag(PostEntity $oPost)
    {
        $this->deletePostTag($oPost);

        if (count($oPost->getTags())) {
            $bdd = $this->_db;
            $postId = $oPost->getPostId();

            $sql = "
              INSERT INTO post_tag
                (post_id, tag_id)
              VALUES
                (:postId, :tagId)
            ";

            foreach ($oPost->getTags() as $oTag) {
                $tagId = $oTag->getTagId();
                $req = $bdd->prepare($sql);
                $req->bindValue(':postId', $postId, \PDO::PARAM_INT);
                $req->bindValue(':tagId', $tagId, \PDO::PARAM_INT);

                $req->execute();
            }
        }
    }


    /**
     * Delete link blog between a category and a post
     * @param PostEntity $oPost
     */
    public function deletePostTag(PostEntity $oPost)
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('DELETE FROM post_tag WHERE post_id = :id');
        $req->bindValue(':id', $oPost->getPostId(), \PDO::PARAM_INT);

        $req->execute();
    }

    /***
     * Delete a Tag
     * @param int $id
     * @return bool
     */
    public function deleteTag(int $id)
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('DELETE FROM tags WHERE tag_id = :id LIMIT 1');
        $req->bindParam(':id', $id, \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Count number of post for a tag
     * @param int $id
     * @return bool|int
     */
    public function countPostsByTag(int $id)
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('SELECT COUNT(post_id) FROM post_tag WHERE tag_id = :id');
        $req->bindValue(':id', $id, \PDO::PARAM_INT);

        $res = $req->execute();

        if ($res) {
            return (int) $req->fetchColumn();
        } else {
            return false;
        }
    }

    /**
     * Fill User object in tag object (lazyload)
     * @param TagEntity $oTag
     * @param int $userId
     * @throws Exception
     */
    public function setUserToTag(TagEntity $oTag, int $userId)
    {
        $userManager = new UserManager($this->_db);
        $oUser = $userManager->getUserById($userId);
        $oTag->setUser($oUser);
    }

    /**
     * Fill Post object in Tag object (lazyload)
     * @param TagEntity $oTag
     * @param string|null $sPostsIds
     * @throws Exception
     */
    public function setPostsToTag(TagEntity $oTag, ?string $sPostsIds)
    {
        $aPosts = [];
        $aPostsIds =  explode(",", $sPostsIds);
        $postManager = new PostManager($this->_db);

        if (count($aPostsIds)) {
            foreach ($aPostsIds as $postId) {
                $oPost = $postManager->getPostById($postId);
                array_push($aPosts, $oPost);
            }
            $oTag->setPosts($aPosts);
        }
    }
}
