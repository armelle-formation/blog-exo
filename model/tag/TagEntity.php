<?php


class TagEntity extends AbstractEntity
{
    /**
     * @var int
     */
    public $_tag_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var datetime
     */
    private $_date_create;

    /**
     * @var UserEntity
     */
    private $_user;

    /**
     * @var array
     */
    private $_posts;

    /**
     * @return int
     */
    public function getTagId()
    {
        return $this->_tag_id;
    }

    /**
     * @param int $tag_id
     */
    public function setTagId($tag_id)
    {
        $this->_tag_id = $tag_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return datetime
     */
    public function getDateCreate()
    {
        return $this->_date_create;
    }

    /**
     * @param string $date_create
     */
    public function setDateCreate($date_create)
    {
        $this->_date_create = $date_create;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): ?UserEntity
    {
        return $this->_user;
    }

    /**
     * @param UserEntity $oUser
     */
    public function setUser(UserEntity $oUser)
    {
        $this->_user = $oUser;
    }

    /**
     * @return array|null
     */
    public function getPosts(): ?array
    {
        return $this->_posts;
    }

    /**
     * @param $aPosts
     */
    public function setPosts($aPosts)
    {
        $this->_posts = $aPosts;
    }
}
