<?php

class AbstractManager
{
    /**
     * @var DbConnect
     */
    protected $_db;

    /**
     * Constructor.
     * @param DbConnect() $db
     */
    public function __construct($db)
    {
        $this->setDb($db);
    }

    /**
     * @param PDO $db
     */
    public function setDb(PDO $db)
    {
        $this->_db = $db;
    }

}
