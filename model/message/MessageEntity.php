<?php

class MessageEntity extends AbstractEntity
{

    /**
     * @var int
     */
    private $_message_id;

    /**
     * @var string
     */
    private $_subject;

    /**
     * @var datetime
     */
    private $_date_send;

    /**
     * @var string
     */
    private $_sender;

    /**
     * @var string
     */
    private $_email_sender;

    /**
     * @var string
     */
    private $_recipient;

    /**
     * @var string
     */
    private $_email_recipient;

    /**
     * @var string
     */
    private $_content;

    /**
     * @return int
     */
    public function getMessageId(): int
    {
        return $this->_message_id;
    }

    /**
     * @param int $message_id
     */
    public function setMessageId(int $message_id): void
    {
        $this->_message_id = $message_id;
    }

    /**
     * @return string
     */
    public function getSubject(): ?string
    {
        return $this->_subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject(string $subject): void
    {
        $this->_subject = $subject;
    }

    /**
     * @return datetime
     */
    public function getDateSend(): ?datetime
    {
        return $this->_date_send;
    }

    /**
     * @param datetime $date_send
     */
    public function setDateSend($date_send): void
    {
        if (!$date_send instanceof DateTime) {
            $date_send = DateTime::createFromFormat("Y-m-d H:i:s", $date_send);
        }
        $this->_date_send = $date_send;
    }

    /**
     * @return string
     */
    public function getSender(): ?string
    {
        return $this->_sender;
    }

    /**
     * @param string $sender
     */
    public function setSender(string $sender): void
    {
        $this->_sender = $sender;
    }

    /**
     * @return string
     */
    public function getEmailSender(): ?string
    {
        return $this->_email_sender;
    }

    /**
     * @param string $email_sender
     */
    public function setEmailSender(string $email_sender): void
    {
        $this->_email_sender = $email_sender;
    }

    /**
     * @return string
     */
    public function getRecipient(): ?string
    {
        return $this->_recipient;
    }

    /**
     * @param string $recipient
     */
    public function setRecipient(string $recipient): void
    {
        $this->_recipient = $recipient;
    }

    /**
     * @return string
     */
    public function getEmailRecipient(): ?string
    {
        return $this->_email_recipient;
    }

    /**
     * @param string $email_recipient
     */
    public function setEmailRecipient(string $email_recipient): void
    {
        $this->_email_recipient = $email_recipient;
    }

    /**
     * @return string
     */
    public function getContent(): ?string
    {
        return $this->_content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content): void
    {
        $this->_content = $content;
    }
}
