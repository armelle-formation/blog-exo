<?php

class MessageManager extends AbstractManager
{

    /**
     * Return a contact message send from contact form (FrontOffice)
     * @param int $msgId
     * @return MessageEntity
     */
    public function getMsgById(int $msgId) : MessageEntity
    {
        $aMsg = [];
        $bdd = $this->_db;
        $sql = "SELECT m.* FROM messages m WHERE m.message_id = :id";

        $req = $bdd->prepare($sql);
        $req->bindParam('id', $msgId, PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aMsg = $req->fetch(PDO::FETCH_ASSOC);
        }
        return new MessageEntity($aMsg);

    }

    /**
     * Return the last contact message send from contact form (FrontOffice)
     * @return MessageEntity
     */
    public function getLastMsg() : MessageEntity
    {
        $aMsg = [];
        $bdd = $this->_db;
        $sql = "SELECT m.* FROM messages m ORDER BY m.date_send DESC LIMIT 1";

        $req = $bdd->prepare($sql);
        $req->bindParam('id', $msgId, PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aMsg = $req->fetch(PDO::FETCH_ASSOC);
        }
        return new MessageEntity($aMsg);

    }


    /**
     * Save a message in database
     * @param MessageEntity $oMsg
     * @return boolean
     */
    public function saveMessage(MessageEntity $oMsg)
    {
        $sql = "
        INSERT INTO messages
          (subject, date_send, sender, email_sender, recipient, email_recipient,content)
        VALUES
            (:subject, :date_send, :sender, :email_sender, :recipient, :email_recipient, :content)
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':subject', $oMsg->getSubject(), PDO::PARAM_STR);
        $req->bindValue(':date_send', $oMsg->getDateSend()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
        $req->bindValue(':sender', $oMsg->getSender(), PDO::PARAM_STR);
        $req->bindValue(':email_sender', $oMsg->getEmailSender(), PDO::PARAM_STR);
        $req->bindValue(':recipient', $oMsg->getRecipient(), PDO::PARAM_STR);
        $req->bindValue(':email_recipient', $oMsg->getEmailRecipient(), PDO::PARAM_STR);
        $req->bindValue(':content', $oMsg->getContent(), PDO::PARAM_STR);

        return $req->execute();
    }
}
