<?php

/**
 * Class BlogException : personalized Exception
 */
class BlogException extends ErrorException
{

    /**
 * @var string
 */
    private $_customText;

    /**
     * @var string
     */
    private $_type;

    /**
     * @var string
     */
    private $_additionalMessage;

    /**
     * BlogException constructor.
     * @param string $message
     * @param string $customText
     * @param int $code
     * @param null $type
     * @param null $addMessage
     */
    public function __construct(
        string $message,
        int $code = 0,
        string $customText = null,
        $type = null,
        $addMessage = null
    ) {
        parent::__construct($message, $code);
        $this->setCustomText($customText);
        $this->setAdditionalMessage($addMessage);
        $this->setType($type);
    }

    /**
     * @return string
     */
    public function getCustomText(): ?string
    {
        return $this->_customText;
    }

    /**
     * @param string $customText
     */
    public function setCustomText(?string $customText)
    {
        $this->_customText = $customText;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->_type;
    }

    /**
     * @param string $type
     */
    public function setType(?string $type): void
    {
        $this->_type = $type;
    }

    /**
     * @return string
     */
    public function getAdditionalMessage(): ?string
    {
        return $this->_additionalMessage;
    }

    /**
     * @param string $additionnalMessage
     */
    public function setAdditionalMessage(?string $additionnalMessage): void
    {
        $this->_additionalMessage = $additionnalMessage;
    }
}
