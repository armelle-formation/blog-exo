<?php

class CategoryEntity extends AbstractEntity
{
    /**
     * @var int
     */
    private $_category_id;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var boolean
     */
    private $_private = false;

    /**
     * @var int or null
     */
    private $_category_id_parent;

    /**
     * @var datetime
     */
    private $_date_create;

    /**
     * @var boolean
     */
    private $_active;

    /**
     * @var UserEntity
     */
    private $_user;



    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->_category_id;
    }

    /**
     * @param int $id
     */
    public function setCategoryId(int $id)
    {
        $this->_category_id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->_name = $name;
    }

    /**
     * @return bool
     */
    public function getIsPrivate(): bool
    {
        return $this->_private;
    }

    /**
     * @param bool $private
     */
    public function setIsPrivate(bool $private)
    {
        $this->_private = $private;
    }

    /**
     * @return int
     */
    public function getCategoryIdParent(): ?int
    {
        return $this->_category_id_parent;
    }

    /**
    * @param int $id_parent
    */
    public function setCategoryIdParent(?int $id_parent)
    {
        $this->_category_id_parent = $id_parent;
    }

    /**
     * @return datetime
     */
    public function getDateCreate()
    {
        return $this->_date_create;
    }

    /**
     * @param $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->_date_create = $dateCreate;
    }

    /**
     * @return bool
     */
    public function getIsActive(): ?bool
    {
        return $this->_active;
    }

    /**
     * @param bool
     */
    public function setIsActive(bool $active)
    {
        $this->_active = $active;
    }

    /**
     * @return null|UserEntity
     */
    public function getUser(): ?UserEntity
    {
        return $this->_user;
    }

    /**
     * @param UserEntity $oUser
     */
    public function setUser(UserEntity $oUser)
    {
        $this->_user = $oUser;
    }

}
