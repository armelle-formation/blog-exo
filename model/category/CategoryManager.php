<?php


class CategoryManager extends \AbstractManager
{

    /**
     * Get CategoryEntity object by id
     * @param int $catId
     * @param bool $getUser
     * @return CategoryEntity
     * @throws Exception
     */
    public function getCatById(int $catId, bool $getUser = false) : CategoryEntity
    {
        $aCat = [];
        $bdd = $this->_db;
        $sql = "
          SELECT 
              c.*
            FROM categories c
            WHERE c.category_id = :id
         ";

        $req = $bdd->prepare($sql);
        $req->bindParam('id', $catId, PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aCat = $req->fetch(PDO::FETCH_ASSOC);
        }
        $oCategory = new CategoryEntity($aCat);

        if (!is_null($oCategory->getCategoryId()) && $oCategory->getCategoryId()) {
            if ($getUser) {
                $this->setUserToCat($oCategory, (int)$aCat['user_id']);
            }
        }
        return $oCategory;
    }

    /**
     * Customizable function for Blog's categories query
     * @param array $array
     * @param string $order
     * @param string $sort
     * @param bool $getUser
     * @return array
     * @throws Exception
     */
    public function getCategoriesByProperties(
        array $array = [],
        string $order = 'c.name',
        string $sort = 'ASC',
        bool $getUser = false
    ): array {
        $bdd = $this->_db;
        $aCats = [];
        $sql = 'SELECT * FROM categories c';

        if (count($array)) {
            $sql .= " WHERE ";
            $counter = 0;

            foreach ($array as $field => $value) {
                $sql .= $field . " = " . $value;

                if ($counter != count($array) -1) {
                    $sql .= " AND ";
                }
                $counter++;
            }
        }

        $sql .= " ORDER BY ".$order . " " . $sort;
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oCategory = new CategoryEntity($row);
            if ($getUser) {
                $this->setUserToCat($oCategory, $row['user_id']);
            }
            array_push($aCats, $oCategory);
        }
        return $aCats;
    }

    /**
     * Save a category (redirect to create or update)
     * @param CategoryEntity $oCat
     * @return int
     */
    public function saveCategory(CategoryEntity $oCat)
    {
        if (!is_null($oCat->getCategoryId())) {
            $catId = $oCat->getCategoryId();
            $this->updateCategory($oCat);
        } else {
            $catId = $this->createCategory($oCat);
        }
        return $catId;
    }

    /**
     * Create a blog category, return id if no errors
     * @param CategoryEntity $oCat
     * @return string
     */
    public function createCategory(CategoryEntity $oCat)
    {
        $sql = "
        INSERT INTO categories
          (name, is_private, category_id_parent, user_id, is_active) 
        VALUES
            (:name, :private, :categoryParent, :userId, :isActive)           
        ";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':name', $oCat->getName(), \PDO::PARAM_STR);
        $req->bindValue(':categoryParent', $oCat->getCategoryIdParent(), \PDO::PARAM_INT);
        $req->bindValue(':private', $oCat->getIsPrivate(), \PDO::PARAM_BOOL);
        $req->bindValue(':userId', $oCat->getUser()->getUserId(), \PDO::PARAM_INT);
        $req->bindValue(':isActive', $oCat->getIsActive(), \PDO::PARAM_BOOL);

        $req->execute();

        return $bdd->lastInsertId();

    }

    /**
     * Update blog category's data based on its ID
     * @param CategoryEntity $oCat
     * @return boolean
     */
    public function updateCategory(CategoryEntity $oCat)
    {
        $sql = "
            UPDATE categories SET
                name = :name,
                is_private = :private,
                category_id_parent = :categoryParent,
                user_id = :userId,
                is_active = :isActive
            WHERE category_id = :id
            LIMIT 1
        ";


        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':id', $oCat->getCategoryId(), \PDO::PARAM_INT);
        $req->bindValue(':private', $oCat->getIsPrivate(), \PDO::PARAM_BOOL);
        $req->bindValue(':categoryParent', $oCat->getCategoryIdParent(), \PDO::PARAM_INT);
        $req->bindValue(':name', $oCat->getName(), \PDO::PARAM_STR);
        $req->bindValue(':userId', $oCat->getUser()->getUserId(), \PDO::PARAM_INT);
        $req->bindValue(':isActive', $oCat->getIsActive(), \PDO::PARAM_BOOL);

        return $req->execute();
    }

    /**
     * Save cats of a post
     * @param PostEntity $oPost
     */
    public function savePostCategory(PostEntity $oPost): void
    {
        $this->deletePostCategory($oPost);
        $bdd = $this->_db;
        $postId = $oPost->getPostId();

        $sql = "
          INSERT INTO post_category
            (post_id, category_id)
          VALUES
            (:postId, :categoryId)
        ";

        foreach ($oPost->getCategories() as $oCategory) {
            $catId = $oCategory->getCategoryId();
            $req = $bdd->prepare($sql);
            $req->bindValue(':postId', $postId, \PDO::PARAM_INT);
            $req->bindValue(':categoryId', $catId, \PDO::PARAM_INT);

            $req->execute();
        }

    }

    /**
     * Delete link blog between a category and a post
     * @param PostEntity $oPost
     */
    public function deletePostCategory(PostEntity $oPost)
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('DELETE FROM post_category WHERE post_id = :id');
        $req->bindValue(':id', $oPost->getPostId(), \PDO::PARAM_INT);

        $req->execute();
    }

    /**
     * Delete a category
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function deleteCategory(int $id): bool
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('DELETE FROM categories WHERE category_id = :id LIMIT 1');
        $req->bindParam(':id', $id, \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Count number of post for a category (front & back versions)
     * @param int $idCat
     * @param bool $online
     * @return int
     */
    public function countPostsByCat(int $idCat, bool $online = false) : int
    {
        $bdd = $this->_db;
        $date = date_create('tomorrow midnight');

        //For count post for front
        if ($online) {
            $sql = "
              SELECT
                p.post_id,
                p.active
              FROM post_category pc
              RIGHT OUTER JOIN posts p ON p.post_id = pc.post_id
              WHERE
                    pc.category_id = :id AND
                    p.active = 1 AND
                    p.date_publish <= :datePublish
              ";
        //For count posts on back
        } else {
            $sql = "
            SELECT
              pc.post_id
            FROM post_category pc
            WHERE pc.category_id = :id
         ";
        }
        $req = $bdd->prepare($sql);
        $req->bindParam('id', $idCat, PDO::PARAM_INT);
        if ($online) {
            $datePublish = $date->format('Y-m-d H:i:s');
            $req->bindParam('datePublish', $datePublish, PDO::PARAM_STR);
        }

        $req->execute();
        return $req->rowCount();
    }

    /**
     * Fill object USer in Category object (Lazyload)
     * @param CategoryEntity $oCat
     * @param int $userId
     * @throws Exception
     */
    public function setUserToCat(CategoryEntity $oCat, int $userId)
    {
        $userManager = new UserManager($this->_db);
        $oUser = $userManager->getUserById($userId);
        $oCat->setUser($oUser);
    }

}
