<?php


class RoleManager extends AbstractManager
{
    /**
     * Return all users roles
     * @return array
     */
    public function getAllRoles() : array
    {
        $bdd = $this->_db;
        $aRoles = [];

        $sql = "
            SELECT
                r.*
            FROM roles r
            ORDER BY r.role_name
        ";
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oRole = new RoleEntity($row);
            array_push($aRoles, $oRole);
        }
        return $aRoles;
    }
}
