<?php

/**
 * Class RoleEntity : User roles
 */
class RoleEntity extends AbstractEntity
{
    /**
     * @var int
     */
    private $_role_id;

    /**
     * @var string
     */
    private $_role_name;

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->_role_id;
    }

    /**
     * @param int $role_id
     */
    public function setRoleId(int $role_id)
    {
        $this->_role_id = $role_id;
    }

    /**
     * @return string
     */
    public function getRoleName(): string
    {
        return $this->_role_name;
    }

    /**
     * @param string $role_name
     */
    public function setRoleName(string $role_name)
    {
        $this->_role_name = $role_name;
    }
}
