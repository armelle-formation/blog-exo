<?php

class CommentManager extends AbstractManager
{

    /**
     * Get CommentEntity object by id
     * @param int $id
     * @return CommentEntity
     * @throws Exception
     */
    public function getCommentById(int $id) : CommentEntity
    {
        $aCom = [];
        $bdd = $this->_db;
        $sql = '
            SELECT
              c.*
            FROM comments c
            WHERE c.comment_id = :id
         ';
        $req = $bdd->prepare($sql);
        $req->bindParam('id', $id, PDO::PARAM_INT);
        $req->execute();

        if ($req->rowCount() > 0) {
            $aCom = $req->fetch(PDO::FETCH_ASSOC);
        }
        $oComment = new CommentEntity($aCom);

        if (!is_null($oComment->getCommentId()) && $oComment->getCommentId()) {
            $this->setStatusToComment($oComment, $aCom['status_id']);
            $this->setUserToComment($oComment, $aCom['user_id']);
            $this->setPostToComment($oComment, $aCom['post_id']);
        }

        return $oComment;
    }


    /**
     * Customizable function for Users query
     * @param array $array
     * @param bool $getStatus
     * @param bool $getUser
     * @param bool $getPost
     * @param string $order
     * @param string $sort
     * @param string $limit
     * @return array
     * @throws Exception
     */
    public function getCommentsByProperties(
        array $array,
        bool $getStatus = false,
        bool $getUser = false,
        bool $getPost = false,
        string $order = 'c.comment_id',
        string $sort = 'ASC',
        string $limit = ""
    ): array
    {
        $bdd = $this->_db;
        $aComments = [];
        $sql = 'SELECT * FROM comments c';

        if ($getStatus) {
            $sql .= ' INNER JOIN comment_status cs on c.status_id = cs.status_id';
        }

        if (count($array)) {
            $condition = " WHERE ";
            $selectors = [ 'IS NOT NULL', 'IS NULL'];
            $keys = array_keys($array);
            for ($i = 0; $i < count($keys); $i++) {
                $value = $array[$keys[$i]];
                $condition .= (in_array($value, $selectors, true)) ? $keys[$i] . ' ' . $value : $keys[$i] . " = " . $value;
                $condition .= ($i != (count($keys) -1)) ? " AND " : "";
            }
            $sql .= $condition;
        }

        $sql .= " ORDER BY ".$order . " " . $sort;
        $sql .= (!empty($limit)) ? " LIMIT " . $limit : "";
        $req = $bdd->prepare($sql);
        $req->execute();

        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            $oComment = new CommentEntity($row);

            if ($getUser) {
                $this->setUserToComment($oComment, $row['user_id']);
            }
            if ($getStatus) {
                $this->setStatusToComment($oComment, $row['status_id']);
            }
            if ($getPost) {
                $this->setPostToComment($oComment, $row['post_id']);
            }
            array_push($aComments, $oComment);
        }
        return $aComments;
    }

    /**
     * Save a comment (redirect to create or update)
     * @param CommentEntity $oComment
     * @return int
     */
    public function saveComment(CommentEntity $oComment) : int
    {
        if (!is_null($oComment->getCommentId())) {
            $commentId = $oComment->getCommentId();
            $this->updateComment($oComment);
        } else {
            $commentId = $this->createComment($oComment);
        }
        return $commentId;
    }

    /**
     * Create a comment post, return id if no errors
     * @param $oCom CommentEntity
     * @return integer
     */
    public function createComment(CommentEntity $oCom)
    {
        $sql = "
        INSERT INTO comments (
              user_id,
              post_id,
              status_id,
              pseudo,
              comment_title,
              comment_text,
              date_create,
              date_update
        ) VALUES (
            :userId,
            :postId,
            :statusId,
            :pseudo,
            :title,
            :text,
            NOW(),
            NOW()
        )";

        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':userId', $oCom->getUser()->getUserId(), PDO::PARAM_INT);
        $req->bindValue(':postId', $oCom->getPost()->getPostId(), PDO::PARAM_INT);
        $req->bindValue(':statusId', $oCom->getStatus()->getStatusId(), PDO::PARAM_INT);
        $req->bindValue(':pseudo', $oCom->getPseudo(), PDO::PARAM_STR);
        $req->bindValue(':title', $oCom->getCommentTitle(), PDO::PARAM_STR);
        $req->bindValue(':text', $oCom->getCommentText(), PDO::PARAM_STR);

        $req->execute();

        return $bdd->lastInsertId();

    }

    /**
     * Update comment post data based on its ID
     * @param $oCom CommentEntity
     * @return boolean
     */
    public function updateComment(CommentEntity $oCom)
    {
        $sql = "
            UPDATE comments SET
                user_id = :userId,
                post_id = :postId,
                status_id = :statusId,
                pseudo = :pseudo,
                comment_title = :title,
                comment_text = :text,
                date_update = NOW()
            WHERE comment_id = :id
            LIMIT 1
        ";


        $bdd = $this->_db;
        $req = $bdd->prepare($sql);
        $req->bindValue(':id', $oCom->getCommentId(), PDO::PARAM_INT);
        $req->bindValue(':userId', $oCom->getUser()->getUserId(), PDO::PARAM_INT);
        $req->bindValue(':postId', $oCom->getPost()->getPostId(), PDO::PARAM_INT);
        $req->bindValue(':statusId', $oCom->getStatus()->getStatusId(), PDO::PARAM_INT);
        $req->bindValue(':pseudo', $oCom->getPseudo(), PDO::PARAM_STR);
        $req->bindValue(':title', $oCom->getCommentTitle(), PDO::PARAM_STR);
        $req->bindValue(':text', $oCom->getCommentText(), PDO::PARAM_STR);

        return $req->execute();
    }

    /**
     * Admin interface : get number of comments published in the last of 6 months
     * @return array
     */
    public function getCommentsPublishedByMonths(): array
    {

        $aStats = [];
        $bdd = $this->_db;
        $sql = "
            SELECT date, COUNT(c.date_create) as value
            FROM (
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH AS date UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 2 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 3 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 4 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 5 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 6 MONTH UNION ALL
                SELECT LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 7 MONTH
            ) AS dates
            LEFT JOIN comments c ON c.date_create >= date AND c.date_create < date + INTERVAL 1 MONTH
            GROUP BY date
        ";

        $req = $bdd->prepare($sql);
        $req->execute();
        while ($row = $req->fetch(PDO::FETCH_ASSOC)) {
            array_push($aStats, $row);
        }
        return $aStats;
    }

    /**
     * Delete a comment
     * @param int $id
     * @return bool
     */
    public function deleteComment(int $id)
    {
        $bdd = $this->_db;
        $req = $bdd->prepare('DELETE FROM comments WHERE comment_id = :id LIMIT 1');
        $req->bindParam(':id', $id, \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Change status of a comment
     * @param int $idComment
     * @param int $idStatus
     * @return bool
     */
    public function changeStatusComment(int $idComment, int $idStatus)
    {
        $bdd = $this->_db;
        $sql = 'UPDATE comments SET status_id = :idStatus, date_update = NOW() WHERE comment_id = :id LIMIT 1';
        $req = $bdd->prepare($sql);
        $req->bindParam(':id', $idComment, \PDO::PARAM_INT);
        $req->bindParam(':idStatus', $idStatus, \PDO::PARAM_INT);

        return $req->execute();
    }

    /**
     * Fill Status object in comment object (lazyload)
     * @param CommentEntity $oComment
     * @param int $statusId
     */
    private function setStatusToComment(CommentEntity $oComment, int $statusId): void
    {
        $statusManager = new StatusManager($this->_db);
        $oComment->setStatus($statusManager->getStatusById($statusId));

    }

    /**
     * Fill User object in comment object (lazyload)
     * @param CommentEntity $oComment
     * @param int $userId
     * @throws Exception
     */
    private function setUserToComment(CommentEntity $oComment, int $userId): void
    {
        $userManager = new UserManager($this->_db);
        $oComment->setUser($userManager->getUserById($userId));

    }

    /**
     * Fill Post object in comment object (lazyload)
     * @param CommentEntity $oComment
     * @param int $postId
     * @throws Exception
     */
    private function setPostToComment(CommentEntity $oComment, int $postId): void
    {
        $postManager = new PostManager($this->_db);
        $oComment->setPost($postManager->getPostById($postId));

    }
}
