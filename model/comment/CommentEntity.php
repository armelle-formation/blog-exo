<?php

/**
 * Class CommentEntity
 */
class CommentEntity extends AbstractEntity
{

    /**
     * @var int
     */
    private $_comment_id;

    /**
     * @var UserEntity
     */
    private $_user;

    /**
     * @var PostEntity
     */
    private $_post;

    /**
     * @var StatusEntity
     */
    private $_status;

    /**
     * @var string
     */
    private $_pseudo;

    /**
     * @var string
     */
    private $_comment_title;

    /**
     * @var string
     */
    private $_comment_text;

    /**
     * @var int
     */
    private $_date_create;

    /**
     * @var int
     */
    private $_date_update;


    /**
     * @return int
     */
    public function getCommentId(): ?int
    {
        return $this->_comment_id;
    }

    /**
     * @param int $comment_id
     */
    public function setCommentId(int $comment_id): void
    {
        $this->_comment_id = (int)$comment_id;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->_user;
    }

    /**
     * @param UserEntity $oUser
     */
    public function setUser(UserEntity $oUser): void
    {
        $this->_user = $oUser;
    }

    /**
     * @return PostEntity
     */
    public function getPost(): PostEntity
    {
        return $this->_post;
    }

    /**
     * @param PostEntity $oPost
     */
    public function setPost(PostEntity $oPost): void
    {
        $this->_post = $oPost;
    }


    /**
     * return StatusEntity
     */
    public function getStatus(): StatusEntity
    {
        return $this->_status;
    }

    /**
     * @param StatusEntity $oStatus
     */
    public function setStatus(StatusEntity $oStatus)
    {
        $this->_status = $oStatus;
    }

    /**
     * @return string
     */
    public function getPseudo(): string
    {
        return $this->_pseudo;
    }

    /**
     * @param string $pseudo
     */
    public function setPseudo(string $pseudo): void
    {
        $this->_pseudo = $pseudo;
    }

    /**
     * @return string
     */
    public function getCommentTitle(): string
    {
        return $this->_comment_title;
    }

    /**
     * @param string $comment_title
     */
    public function setCommentTitle(string $comment_title): void
    {
        $this->_comment_title = $comment_title;
    }

    /**
     * @return string
     */
    public function getCommentText(): string
    {
        return $this->_comment_text;
    }

    /**
     * @param string $comment_text
     */
    public function setCommentText(string $comment_text): void
    {
        $this->_comment_text = $comment_text;
    }

    /**
     * @return string
     */
    public function getDateCreate(): string
    {
        return $this->_date_create;
    }

    /**
     * @param string $date_create
     */
    public function setDateCreate(string $date_create): void
    {
        $this->_date_create = $date_create;
    }

    /**
     * @return null|string
     */
    public function getDateUpdate(): ?string
    {
        return $this->_date_update;
    }

    /**
     * @param null|string $date_update
     */
    public function setDateUpdate(?string $date_update): void
    {
        $this->_date_update = $date_update;
    }

}
