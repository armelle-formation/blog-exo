<?php

/**
 * Customizable section
 */
//BDD Credentials
define('BDD_HOST', 'localhost');
define('BDD_NAME', 'bdd');
define('BDD_USER', 'root');
define('BDD_PASSWORD', '');

// Mode
define('MODE_DEV', true);

//Email for test email function
define('EMAIL_PERSO', 'test@test.com');

// External
define('NAME_CONTACT', 'Contact mon Blog');
define('EMAIL_CONTACT', 'contact@monblog.com');
define('URL_WEBMAIL', 'https://monblog.webmail.com');

// API Open Weather : key to generate here : https://openweathermap.org/appid
define('OPEN_WEATHER_KEY', '2788bgr95c6ab3862e0357');

// Generical messages
define("ERROR_MESSAGE", "Une erreur s'est produite, merci de réessayer ultérieurement");
define("SUCCESS_MESSAGE", "L'enregistrement s'est déroulé avec succés");

// Misceallenous
define("CV_FILENAME", 'CV_Armelle_Braud.pdf');

// Random Key for cookie encryption
define('SECRET_KEY', 'TOTOETTITI');
define('IV_KEY', 'SONTDANSUNBATEAU');

// Preferences
define('MIN_WIDTH_POST_IMG', 800);
define('MIN_HEIGHT_POST_IMG', 600);
define('MD_WIDTH_POST_IMG', 350);
define('MD_HEIGHT_POST_IMG', 261);
define('MIN_WIDTH_USER_IMG', 80);
define('MIN_HEIGHT_USER_IMG', 80);
define('MAX_WIDTH_IMG', 6000);
define('MAX_HEIGHT_IMG', 6000);
define('NB_POST_PER_PAGE', 8);
define('NUMBER_DISPLAY_TAGS', 30);
define('NUMBER_DISPLAY_CATS', 8);

/**
 * End customizable section
 */

// Paths
define('SERVER_BASE', str_replace('//', '/', dirname($_SERVER['PHP_SELF']) . '/'));
define('ROOT', dirname($_SERVER["SCRIPT_FILENAME"]) . '/');
define('COMPLETE_PATH', 'http://' . $_SERVER['SERVER_NAME'] . SERVER_BASE);

define('MODEL_PATH', ROOT . 'model/');
define('CONTROLLERS_PATH', ROOT . 'controllers/');
define('ASSETS_PATH', SERVER_BASE . 'assets/');
define('VIEWS_PATH', ROOT . 'views/');
define('CORE_PATH', ROOT . 'core/');

// Enums (must match with data from the database)
define("ID_ADMIN", 1);
define("ID_EDITOR", 2);
define("ID_READER", 3);
define('COMMENT_TO_MODERATE', 1);
define('COMMENT_ACTIVE', 2);
define('COMMENT_MASK', 3);

// Applicatives rules
define('COMMENT_DEFAULT_STATUS', COMMENT_TO_MODERATE);                                                                  //Status "to moderate" by default
define('POST_PRIVACY', 1);                                                                                              //Id of privacy post
define('POST_LEGALS', 2);                                                                                               //Id of legals post

// Cookie lifetime
define('MAX_IDLE_TIME', 18000);                                                                                         // 300 minuts
define('COOKIE_DURATION', time()+60*60*24*1);                                                                           // 24 hours

// Application
setlocale(LC_ALL, 'fr_FR.utf8', 'fra');                                                               // I'm french !
date_default_timezone_set('Europe/Paris');
mb_internal_encoding('UTF-8');

// Uploads Folder
define('UPLOADS_PATH', SERVER_BASE . 'uploads/');
define('CV_PATH', SERVER_BASE .'uploads/documents/cv/');
define('BLOG_IMG_PATH', 'uploads/images/blog/');
define('PROFILE_IMG_PATH', 'uploads/images/admin/profile/');

// Genericals views
define('VIEW_ERROR', 'error/displayError.html.twig');

// Filenames
define('JSON_DATA_FILE', ROOT.'core/json/resume.json');
define('NAMEFILE_RESUME', CV_FILENAME);
define('GENERIC_USER_IMAGE', 'generical-user.png');
define('GENERIC_BLOG_IMAGE', 'blog-default.jpg');

//Helpers
define('KB', 1024);
define('MB', 1048576);
define('GB', 1073741824);
define('TB', 1099511627776);

// Inclusions
include_once (CORE_PATH . 'util/autoload.php');
include_once (ROOT . 'vendor/autoload.php');

// Autoloader
spl_autoload_register('chargeClass');
