<?php

/**
 * Spécific class for database connexion
 */
final class ConfigDbConnect
{
    const DB_HOST = BDD_HOST;
    const DB_NAME = BDD_NAME;
    const DB_USR = BDD_USER;
    const DB_PWD = BDD_PASSWORD;
}
