<?php

/**
 * Class DbConnect : specific class for PDO object
 */

class DbConnect extends PDO
{

    /**
     * DbConnect constructor
     */
    public function __construct()
    {
        try {

            $options = [
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET lc_time_names = 'fr_FR', NAMES utf8"
            ];
            parent::__construct(
                'mysql:host='. ConfigDbConnect::DB_HOST.';dbname='.ConfigDbConnect::DB_NAME.';charset=utf8',
                ConfigDbConnect::DB_USR,
                ConfigDbConnect::DB_PWD,
                $options
            );

            if (MODE_DEV) {
                $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }

        } catch (PDOException $e) {
            throw new BlogException(
                $e->getMessage(),
                401,
                'Erreur lors de la connexion à la base de données.',
                'DataBaseException',
                'ErrorCode PDO : ' . $e->getCode()
            );
        }
    }

}
