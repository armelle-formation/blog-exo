<?php

use \Twig\Loader\FilesystemLoader;
use \Twig\Environment;

class Mail
{
    protected $_twig;

    public function __construct(Environment $twig)
    {
        $this->_twig = $twig;
    }

    /**
     * Send a mail with a twig template
     * @param string $destinataire
     * @param string $content
     * @param array $aParams
     * @throws BlogException
     */
    public function sendMailTwig(string $destinataire, string $content, array $aParams): void
    {
        //Headers
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'From:'.$aParams["sender"].' <'.$aParams["emailSender"].'>' . "\r\n" .
            'Reply-To:'.$aParams["emailSender"]. "\r\n" .
            'Content-Type: text/html; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n" .
            'Content-Disposition: inline'. "\r\n" .
            'Content-Transfer-Encoding: 7bit'." \r\n" .
            'Return-Path: '.$aParams["emailSender"]."\r\n";
            'X-Mailer:PHP/'.phpversion();
        $subject = '=?UTF-8?B?'.base64_encode($aParams['subject']).'?=';

        // Envoi du mail
        try {
            mail($destinataire, $subject, $content, $headers);
        } catch (Exception $e) {
            if (MODE_DEV) {
                Debug::printr($e);
                exit();
            } else {
                throw new BlogException($e->getMessage(), $e->getCode(), "Problème d'envoi", 'EmailException', __FUNCTION__);
            }
        }
    }

    /**
     * check if the configuration of the mail server is valid
     * @return bool
     */
    public function sendTestMail()
    {
        //Headers
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'From:'.NAME_CONTACT.' <'.EMAIL_CONTACT.'>' . "\r\n" .
            'Reply-To:'.EMAIL_CONTACT. "\r\n" .
            'Content-Type: text/html; charset="utf-8"; DelSp="Yes"; format=flowed '."\r\n" .
            'Content-Disposition: inline'. "\r\n" .
            'Content-Transfer-Encoding: 7bit'." \r\n" .
            'X-Mailer:PHP/'.phpversion();

        $to = EMAIL_PERSO;
        $subject = "Test mail";
        $message = "Ceci est un email de test pour confirmer que le serveur est configuré correctement";
        return mail($to, $subject, $message, $headers);
    }
}
