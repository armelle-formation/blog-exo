<?php

/**
 * Class Data : generical class to store data
 */
class Data
{
    private static $_instance = null;
    private $_oData = null;

    /* CONSTRUCTOR */

    /**
     * Constructor skeleton
     * @return Data|null
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Data();
        }
        return self::$_instance;
    }

    /* PUBLIC METHODS */
    /**
     * @param $file
     */
    public function setData($file)
    {
        $json_data = file_get_contents($file);
        $this->_oData = @json_decode($json_data, true);
    }

    /**
     * @return null
     */
    public function getData()
    {
        return $this->_oData;
    }

    /**
     * Get and Add an array to Data object corresponding to a given section
     * @param string $sSectionTitle
     * @return mixed|null
     */
    public function factory(string $sSectionTitle)
    {
        return $this->getSection($sSectionTitle);
    }

    /* PRIVATE METHODS */

    /** Return data array of given section title contained in Data Object
     * @param string $sSectionTitle
     * @return mixed|null
     */
    public function getSection(string $sSectionTitle)
    {
        return (@isset($this->_oData[$sSectionTitle])) ? $this->_oData[$sSectionTitle] : null;
    }
}
