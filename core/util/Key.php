<?php


class Key
{
    /**
     * @return string
     */
    public static function createNewRandomKey()
    {
        return SECRET_KEY;
    }

    /**
     * Simple method to encrypt or decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     * @param $action
     * @param $string
     * @param $secretKey
     * @return bool|string
     * @throws BlogException
     */
    public static function encryptDecrypt($action, $string, $secretKey)
    {

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_iv = 'IV_KEY';

        // hash
        $key = hash('sha256', $secretKey);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
       $iv = substr(hash('sha256', $secret_iv), 0, 16);
        try {
            if ($action == 'encrypt') {
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
            } elseif ($action == 'decrypt') {
                $string = base64_decode($string);
                $output = openssl_decrypt($string, $encrypt_method, $key, 0, $iv);
            }
        } catch (Exception $e) {
            if (MODE_DEV) {
                Debug::printr($e);
                exit();
            } else {
                throw new BlogException(
                    "Forbidden",
                    403,
                    "Erreur d'encryptage / décryptage",
                    'SecurityException',
                    __FUNCTION__
                );
            }
        }
        return $output;
    }

    public static function generateTicket()
    {
        $ticket = session_id().microtime().rand(0, 9999999999);
        $ticket = hash('sha512', $ticket);
        return $ticket;
    }

}
