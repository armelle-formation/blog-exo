<?php

/**
 * Autoloader for required classes of application
 * @param object $classe
 */
function chargeClass($classe)
{
    try {
        //Basic Classes
        $aTypeClasse = array("Manager", "Controller", "Entity", "Service");
        $subfolder = str_replace($aTypeClasse, "", $classe) . "/";

        //Root files located
        $bIsRoot = false;
        $aRootList = ["abstract"];

        foreach ($aRootList as $index) {
            if (strpos(strtolower($classe), $index) !== false) {
                $bIsRoot = true;
                $subfolder = "";
                break;
            }
        }

        //Utility classes
        $bIsUtil = false;
        $aUtilList = ["view","debug","router","session","key","helper","data","mail","file","api"];

        foreach ($aUtilList as $index) {
            if (strpos(strtolower($classe), $index) !== false) {
                $bIsUtil = true;
                break;
            }
        }

        //Subfolder definition
        if ($bIsUtil) {
            $subfolder = "util/";
        }

        //Special cases for folder not based of class name
        $aSpecials = [
            "db" => "db",
            "exception" => "exception"
        ];

        if (!$bIsRoot && !$bIsUtil) {
            foreach ($aSpecials as $key => $value) {
                if (strpos(strtolower($classe), $key) !== false) {
                    $subfolder = strtolower($value) . "/";
                    break;
                }
            }
        }

        $subfolder = strtolower($subfolder);

        // Checking file existence
        if (file_exists($file = MODEL_PATH . $subfolder . $classe . '.php')) {
            require_once $file;
        } elseif (file_exists($file = CONTROLLERS_PATH . $subfolder . $classe . '.php')) {
            require_once $file;
        } elseif (file_exists($file = CORE_PATH . $subfolder . $classe . '.php')) {
            require_once $file;
        }  else {
            throw new BlogException(
                'Class not found',
                500,
                "La classe demandée ".$classe." n'a pas pu être chargée !",
                "AutoloadException"
            );
        }

    } catch (Exception $e) {
        trigger_error($e->getMessage(), E_USER_ERROR);
    }
}
