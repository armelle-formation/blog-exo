<?php

class Router
{

    private $_routes = array();
    private $_pathNotFound = null;
    private $_methodNotAllowed = null;

    /**
     * Return all routes
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->_routes;
    }

    /**
     * @param $function
     */
    public function pathNotFound($function)
    {
        $this->_pathNotFound = $function;
    }

    /**
     * @param $function
     */
    public function methodNotAllowed($function)
    {
        $this->_methodNotAllowed = $function;
    }

    /**
     * Add a route
     * @param $exp
     * @param $function
     */
    public function add($exp, $function)
    {
        array_push($this->_routes, array(
            'expression' => $exp,
            'function' => $function
        ));
    }

    /**
     * Launches the router
     * @param $url
     * @param string $basepath
     */
    public function run($url, $basepath = '/')
    {
        //Initialization
        $pathMatchFound = false;
        $routeMatchFound = false;

        // Parse current url
        $parsedUrl = parse_url($url);

        if (isset($parsedUrl['path'])) {
            $path = $parsedUrl['path'];
        } else {
            $path = '/';
        }

        // Get current request method
        $method = $_SERVER['REQUEST_METHOD'];

        foreach ($this->_routes as $route) {

            // Add basepath to matching string
            if ($basepath != ''&& $basepath != '/') {
                $route['expression'] = '(' . $basepath . ')' . $route['expression'];
            }

            // Add 'find string start' automatically
            $route['expression'] = '^' . $route['expression'];

            // Add 'find string end' automatically
            $route['expression'] = $route['expression'].'$';

            // Check path match
            if (preg_match('#' . $route['expression'] . '#', $path, $matches)) {

                $pathMatchFound = true;


                array_shift($matches);// Removal of first element. This contains the whole string

                if ($basepath != '' && $basepath != '/') {
                    array_shift($matches);// Remove basepath
                }

                call_user_func_array($route['function'], $matches);
                $routeMatchFound = true;

                // Do not check other routes
                break;
            }
        }

        // No matching route was found
        if (!$routeMatchFound) {

            // But a matching path exists
            if ($pathMatchFound) {
                header("HTTP/1.0 500 Method Not Allowed");

                if ($this->_methodNotAllowed) {
                    call_user_func_array($this->_methodNotAllowed, array($path, $method));
                }
            } else {
                header("HTTP/1.0 404 Not Found");
                if ($this->_pathNotFound) {
                    call_user_func_array($this->_pathNotFound, array($path, $method));
                }
            }

        }
    }
}
