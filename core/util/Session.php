<?php


class Session
{
    private static $_instance = null;

    /**
     * Singleton
     * @return Session|null
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Session();
        }
        return self::$_instance;
    }


    /**
     * Class constructor
     * @return mixed
     * @access private
     */
    private function __construct()
    {
        session_start();
        session_regenerate_id();
    }

    /**
     * Set personalized value in session PHP array
     * @param string $variable
     * @param ?string $value
     */
    public function setValue($variable, $value)
    {
        $_SESSION[$variable] = $value;
    }

    /**
     * Return personalized value stored in session PHP
     * @param string $variable
     * @param $defaultValue= null
     * @return ?string
     */
    public function getValue($variable, $defaultValue= null)
    {
        $ret = $defaultValue;
        if (isset($variable) && array_key_exists($variable, $_SESSION)) {
           $ret = $_SESSION[$variable];
        }
        return $ret;
    }
}
