<?php


class Api
{

    /**
     * Geolocalization by IP by API
     * Return array with Country, City, latitute & longitude
     * API doc : http://ip-api.com/docs/
     * @return bool|mixed
     */
    public static function getLocalization()
    {

        //Localization API

        $url = "http://ip-api.com/json";
        $content = @file_get_contents($url);

        if ($content !== false) {
            $json = json_decode($content, true);
            if ($json['status'] == 'success') {
                return $json;
            }
        }
        return false;
    }


    /**
     * Return weather info by querying a geolocation API.
     * Choice between city, GPS coordinates or zip code (+ country code)
     * API doc :https://openweathermap.org/current
     * @param string $origin
     * @return bool|mixed
     */
    public static function getWeatherInfo($origin)
    {

        $infoGeo = self::getLocalization();

        if ($infoGeo !== false) {

            $apiParams['units'] = 'metric';
            $apiParams['lang'] = 'fr';
            $apiParams['APPID'] = OPEN_WEATHER_KEY;

            switch ($origin) {
                case 'city':
                    $apiParams['q'] = $infoGeo['city'];
                    break;
                case 'gps':
                    $apiParams['lat'] = $infoGeo['lat'];
                    $apiParams['lon'] = $infoGeo['lon'];
                    break;
                case 'zip':
                    $apiParams['zip'] = $infoGeo['zip'] . "," . strtolower($infoGeo['countryCode']);
                    break;
                default:
                    $apiParams['q'] = 'paris';
                    break;
            }

            $keys = array_keys($apiParams);
            $url = "https://api.openweathermap.org/data/2.5/weather?";
            for ($i = 0; $i < count($keys); $i++) {
                $key =$keys[$i];
                $value = $apiParams[$keys[$i]];

                $url .= ($i > 0) ? "&" . $key . "=" . $value : $key . "=" . $value;
            }

            $content = file_get_contents($url);

            if ($content !== false) {
                $json = json_decode($content, true);
                if ($json['cod'] == 200) {
                    return $json;
                }
            }
        }
        return false;
    }
}
