<?php

class File
{
    /**
     * Generic function for upload an image with resize function
     * @param $file
     * @param $path
     * @param bool $madeLg
     * @param bool $madeMd
     * @return array
     * @throws BlogException
     */

    public static function uploadImage($file, $path, $madeLg = false, $madeMd = false)
    {
        $result = [];

        // Check valid file
        if ($file['image']['size'][0] == 0) {
            throw new BlogException("Forbidden", 403, "Fichier non valide.");
        }

        // Check image size
        if ($file['image']['size'][0] > 6*MB) {
            throw new BlogException("Forbidden", 403, "Taille maximale autorisée pour le fichier dépassée.");
        }
        $infoImage = pathinfo($file['image']['name'][0]);
        $imageExtension = $infoImage['extension'];

        //Testing image
        self::imageChecking(@getimagesize($file["image"]["tmp_name"][0]), $imageExtension);

        //Rename file
        $newName = self::randomFilename(20, $path, $imageExtension);

        try {
            // move original image into uploads folder
            move_uploaded_file($file["image"]["tmp_name"][0], ROOT . $path . $newName);

            //made a medium file for user edit profil
            self::imageResize(
                ROOT . $path . $newName,
                ROOT . $path . 'b_' . $newName,
                250,
                250,
                1
            );

            // made a large file for post header
            if ($madeLg) {
                self::imageResize(
                    ROOT . $path . $newName,
                    ROOT . $path . 'l_' . $newName,
                    MIN_WIDTH_POST_IMG,
                    MIN_HEIGHT_POST_IMG,
                    1
                );
            }

            // made a medium file for posts
            if ($madeMd) {
                self::imageResize(
                    ROOT . $path . $newName,
                    ROOT . $path . 'm_' . $newName,
                    MD_WIDTH_POST_IMG,
                    MD_HEIGHT_POST_IMG,
                    1
                );
            }

            // made a thumbnail file
            self::imageResize(
                ROOT . $path . $newName,
                ROOT . $path . 'v_' . $newName,
                MIN_WIDTH_USER_IMG,
                MIN_HEIGHT_USER_IMG,
                1
            );

            //Remove original file
            unlink($path . $newName);

            $result['success'] = true;
            $result['filename'] = $newName;

        } catch (BlogException $e) {
            $result['success'] = false;
            $result['filename'] = "";
            $result['message'] = $e->getCustomText();
        }

        return $result;
    }

    /**
     * Execute many tests on image file
     * @param $file
     * @param $imageExtension
     * @throws BlogException
     */
    public static function imageChecking($imageInfo, $imageExtension)
    {
        // Check file dimensions
        if (!list($w, $h) = $imageInfo) {
            throw new BlogException("Unsupported Media Type", 415, "Fichier non supporté.");
        }

        // allowed extensions
        $allowedExtensions = array('jpg', 'jpeg', 'gif', 'png');

        // check if extension is valid
        if (!in_array($imageExtension, $allowedExtensions)) {
            throw new BlogException("Unsupported Media Type", 415, "Format non supporté", 'FileException', __FUNCTION__);
        }

        //check if the dimensions are not too big
        if ($w > MAX_WIDTH_IMG || $h > MAX_HEIGHT_IMG) {
            throw new BlogException(
                "Forbidden",
                403,
                "Largeur supérieure à " .MAX_WIDTH_IMG . "px ou hauteur supérieure à ". MAX_HEIGHT_IMG ."px",
                'FileException',
                __FUNCTION__
            );
        }

    }

    /**
     * Generic function for upload an image with resize function
     * @param $file
     * @param $path
     * @param $nameField
     * @return mixed
     */
    public static function ckUploadImage($file, $path, $nameField)
    {
        $result['success'] = false;
        $result['filename'] = "";

        try {
            // Check image size
            if ($file[$nameField]['size'] > 6*MB) {
                throw new BlogException("Forbidden", 403, "Taille maximale autorisée pour le fichier dépassée.");
            }

            $infoImage = pathinfo($file[$nameField]['name']);
            $imageExtension = $infoImage['extension'];

            //Testing image
            self::imageChecking(@getimagesize($file[$nameField]["tmp_name"]), $imageExtension);

            //Rename file
            $newName = self::randomFilename(20, $path, $imageExtension);

            // move original image into uploads folder
            move_uploaded_file($file[$nameField]["tmp_name"], ROOT . $path . $newName);

            // made a middle file
            self::imageResize(ROOT . $path . $newName, ROOT . $path . 'l_' . $newName, 800, 1000, 0, 1);

            //Remove original file
            unlink($path . $newName);

            $result['success'] = true;
            $result['filename'] = 'l_' . $newName;

        } catch (BlogException $e) {

            $result['message'] = $e->getCustomText();
        }

        return $result;
    }

    /**
     * Check if the image exist, then remove it
     * @param $image
     * @param $path
     */
    public static function removeImage($image, $path)
    {
        $path = ROOT . $path;

        if ($image != null) {
            if (file_exists($path . $image)) {
                unlink($path . $image);
            }
            if (file_exists($path . 'v_' . $image)) {
                unlink($path . 'v_' . $image);
            }
            if (file_exists($path . 'm_' . $image)) {
                unlink($path . 'm_' . $image);
            }
            if (file_exists($path . 'l_' . $image)) {
                unlink($path . 'l_' . $image);
            }
            if (file_exists($path . 'b_' . $image)) {
                unlink($path . 'b_' . $image);
            }
        }
    }

    /**
     * Generic function for upload a file with controls
     * No rename : file public (CV,  porjects)
     * @param $file
     * @param $path
     * @param $filename
     * @param $allowedExtensions
     * @param $mimetype
     * @return array
     */
    public static function uploadFile($file, $path, $filename, $allowedExtensions, $mimetype)
    {

        $result = [];

        try {

            if (!empty($file['file']['error']) || !is_array($file['file']['error'])) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['file']['error'] value.
            if ($file['file']['error'][0] != '0') {
                switch ($file['file']['error']) {
                    case UPLOAD_ERR_OK:
                        break;
                    case UPLOAD_ERR_NO_FILE:
                        throw new RuntimeException('Aucun fichier envoyé');
                        break;
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new RuntimeException('Taille maximale autorisée pour le fichier dépassée.');
                        break;
                    default:
                        throw new RuntimeException('Erreurs inconnues');
                        break;
                }
            }

            // Check file size
            if (intval($file['file']['size']) > 1000000) {
                throw new RuntimeException('Taille maximale autorisée pour le fichier dépassée.');
            }

            // Check MIME Type
            $mime = mime_content_type($file['file']['tmp_name'][0]);
            if ($mime != $mimetype) {
                throw new RuntimeException('Type mime non autorisé.');
            }

            //Check extension
            $ext = pathinfo($file['file']['name'][0], PATHINFO_EXTENSION);
            if (!in_array($ext, $allowedExtensions)) {
                throw new RuntimeException('Format de fichier invalide');
            }

            //Move and Rename
            $file = Helper::cleanNameFile($filename);
            if (!move_uploaded_file($file['file']['tmp_name'][0], $path . $filename)) {
                throw new RuntimeException("Une erreur s'est produite dans l'enregistrement du fichier PDF");
            }

            $result['success'] = true;

        } catch (RuntimeException $e) {
            $result['success'] = true;
            $result['message'] =  $e->getMessage();

        }

        return $result;
    }

    /**
     * Resize image according to passed options
     * @param $src string image link
     * @param $dst string link
     * @param $width int desired width
     * @param $height int desired heigth
     * @param int $crop
     * @param int $keepRatio
     * @return bool|string
     * @throws BlogException
     */
    public static function imageResize($src, $dst, $width, $height, $crop = 0, $keepRatio = 0)
    {
        if (!list($w, $h) = getimagesize($src)) {
            throw new BlogException("Forbidden", 403, "Type d’image non supporté", "FileException", __FUNCTION__);
        }

        $type = strtolower(substr(strrchr($src, '.'), 1));
        $type = ($type == 'jpeg') ? 'jpg' : $type;

        switch ($type) {
            case 'bmp':
                $img = imagecreatefromwbmp($src);
                break;
            case 'gif':
                $img = imagecreatefromgif($src);
                break;
            case 'jpg':
                $img = imagecreatefromjpeg($src);
                break;
            case 'png':
                $img = imagecreatefrompng($src);
                break;
            default :
                $img = "";
        }

        $w_point = 0;
        $h_point = 0;

        /** resize **/
        if ($crop) {
            if ($w < $width || $h < $height) {
                throw new BlogException(
                    "Forbidden",
                    403,
                    "Image trop petite, dimensions minimum : ". $width . " px de large et " . $height. " px de haut",
                    "FileException"
                );
            }
            $ratio = max($width / $w, $height / $h);

            $w_point = ($w - $width / $ratio) / 2;
            $h = $height / $ratio;
            $w = $width / $ratio;

        } elseif ($keepRatio) {
            $ratio = $w / $h;
            if ($ratio > 1) {
                $height = $width / $ratio;
            } else {
                $width = $height * $ratio;
            }
        } else {
            if ($w < $width && $h < $height) {
                throw new BlogException("Forbidden", 403, "Image trop petite", "FileException");
            }
            $ratio = min($width / $w, $height / $h);
            $width = $w * $ratio;
            $height = $h * $ratio;
        }

        $new = imagecreatetruecolor($width, $height);

        /** preserve transparency **/
        if ($type == 'gif' || $type == 'png') {
            imagecolortransparent($new, imagecolorallocatealpha($new, 0, 0, 0, 127));
            imagealphablending($new, false);
            imagesavealpha($new, true);
        }

        if (empty($img)) {
            throw new BlogException("Forbidden", 403, "Type d’image non supporté", "FileException");
        }

        imagecopyresampled($new, $img, 0, 0, $w_point, $h_point, $width, $height, $w, $h);

        switch ($type) {
            case 'bmp':
                imagewbmp($new, $dst);
                break;
            case 'gif':
                imagegif($new, $dst);
                break;
            case 'jpg':
                imagejpeg($new, $dst, 100);
                break;
            case 'png':
                imagepng($new, $dst, 9);
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * Make a random name file anc verify that is unique in the directory
     * @param $length
     * @param string $directory
     * @param string $extension
     * @return string
     */
    public static function randomFilename($length, $directory = '', $extension = '')
    {
        // default to this files directory if empty...
        $dir = !empty($directory) && is_dir($directory) ? $directory : dirname(__FILE__);

        do {
            $key = '';
            $keys = array_merge(range(0, 9), range('a', 'z'));

            for ($i = 0; $i < $length; $i++) {
                $key .= $keys[array_rand($keys)];
            }
        } while (file_exists($dir . '/' . $key . (!empty($extension) ? '.' . $extension : '')));

        return $key . (!empty($extension) ? '.' . $extension : '');
    }
}
