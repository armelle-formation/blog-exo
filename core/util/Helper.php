<?php

/**
 * Class Helper : utilities functions classified by theme
 */
class Helper
{

    /* MATHEMATIC FUNCTIONS */
    /**
     * Calculate age from a date (birth date is a good choice)
     * @param $pstrDate
     * @return float
     */
    public static function calculateAge($pstrDate)
    {
        return floor((time() - strtotime($pstrDate)) / 31556926);
    }

    /*DATE FUNCTIONS */
    /**
     * Converts a date to a string like " 2 years ago " (in french)
     * @param $datetime
     * @param bool $full
     * @return string
     * @throws Exception
     */
    public static function timeElapsedString($datetime, $full = false)
    {

        $now = new DateTime;
        $ago = (!$datetime instanceof DateTime) ? new DateTime($datetime) : $datetime;
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'année',
            'm' => 'mois',
            'w' => 'semaine',
            'd' => 'jour',
            'h' => 'heure',
            'i' => 'minute',
            's' => 'seconde',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . (($diff->$k > 1 && $k != 'm') ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }
        return $string ? 'il y a ' . implode(', ', $string) : "à l'instant";
    }

    /* STRING FUNCTIONS */
    /**
     * Check if a password is complex enough
     * @param $password
     * @return array
     */
    public static function isPasswordComplex($password)
    {
        $errors = array();
        $errors["success"] = true;
        $errors["message"] = null;

        if (strlen($password) < 6) {
            $errors["success"] = false;
            $errors["message"] .= "Le mot de passe est trop court";
            return $errors;
        }

        if (!preg_match("#[0-9]+#", $password)) {
            $errors["success"] = false;
            $errors["message"] = "Le mot de passe doit contenir au moins un chiffre";
            return $errors;
        }
        if (!preg_match("#[a-z]+#", $password)) {
            $errors["success"] = false;
            $errors["message"] = "Le mot de passe doit contenir au moins une lettre miuscule";
            return $errors;
        }

        if (!preg_match("#[A-Z]+#", $password)) {
            $errors["success"] = false;
            $errors["message"] = "Le mot de passe doit contenir au moins une lettre majuscule";
            return $errors;
        }

        if (strlen($password) > 20) {
            $errors["success"] = false;
            $errors["message"] = "Le mot de passe est trop long";
            return $errors;
        }
        return $errors;
    }

    /**
     * Check that the string has only letters
     * @param $string
     * @return bool
     */
    public static function stringLettersOnly($string)
    {
        return !preg_match('~[0-9]+~', $string);
    }

    /**
     * Check that the string is a valid email
     * @param $email
     * @return mixed
     */
    public static function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * String cleaner : remove HTML tags ans convert special characters to HTML entities
     * @param $string
     * @param bool $htmlSpecialChars
     * @return string
     */
    public static function cleanPostString($string, $htmlSpecialChars = true, $linebreak = true)
    {
        $string = strip_tags($string);
        $string = ($htmlSpecialChars) ? htmlspecialchars($string, ENT_HTML5) : $string;
        $string = ($linebreak) ? str_replace(array("\n","\r",PHP_EOL), '', $string) : $string;
        return trim($string);
    }

    /**
     * Remove anything which isn't a word, whitespace, number or any of the following caracters -_~,;[]().
     * @param $filename
     * @return false|string
     */
    public static function cleanNameFile($filename)
    {
        $filename = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $filename);
        $filename = mb_ereg_replace("([\.]{2,})", '', $filename);

        return $filename;
    }
}
