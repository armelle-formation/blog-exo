<?php

/**
 * Debug class : contains useful functions for debugging
 */
class Debug
{

    /**
    * Displays the entire contents of a variable on the screen.
    * @param mixed $var : Variable to display
    * @param string $titre : Optional, to use if you want to put a "title" to the display.
    */
    public static function printr($var, $titre = null)
    {
        if (!isset($var)) {
            echo "Debug::printr : aucune variable définie.<br />";
        }
        if (empty($titre)) {
            echo "<pre>";
        } else {
            echo "<pre> $titre : \n";
        }
        print_r($var);
        echo "</pre>";
    }

    /**
    * This function is a shortcut for $_REQUEST.
    */
    public static function pr()
    {
        self::printr($_REQUEST);
    }

    /**
    * Displays the contents of a variable on the screen, and stops the application
    * @param mixed $var : Variable to display
    * @param string $titre : Optional, to use if you want to put a "title" to the display.
    */
    public static function dier($var, $titre = null)
    {
        Debug::printr($var, $titre);
        die ();
    }

    /**
    * Displays the function stack from where it was called.
    * Allows to know precisely where you are in the code.
    * @param string $titre : Optional, to use if you want to put a "title" to the display.
    */
    public static function printPile($titre = null)
    {
        $liste_fonction = debug_backtrace();
        $i = 0;
        echo $titre . " : <br /> \n";

        foreach ($liste_fonction as $ligne) {
            if ($i != 0) {
                if (!isset($ligne['file'])) {
                    $ligne['file'] = 'fichier inconnu';
                }
                if (!isset($ligne['line'])) {
                    $ligne['line'] = 'inconnue';
                }

                $result = '<strong>' . $ligne['function'];
                $result .= '</strong> dans <em>' . $ligne['file'];
                $result .= '</em> (ligne ' . $ligne['line'] . ") <br />\n";
                echo $result;

            } else {
                $i++;
            }
        }
    }
}

