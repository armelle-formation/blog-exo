-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  sam. 16 nov. 2019 à 00:50
-- Version du serveur :  10.2.27-MariaDB
-- Version de PHP :  7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ocarugfu_blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_private` tinyint(1) NOT NULL DEFAULT 0,
  `category_id_parent` int(11) DEFAULT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `is_active` int(1) NOT NULL DEFAULT 0,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `is_private`, `category_id_parent`, `date_create`, `is_active`, `user_id`) VALUES
(1, 'Blog', 1, NULL, '2019-08-18 15:06:37', 1, 1),
(3, 'Développement', 0, 1, '2019-08-11 17:15:02', 1, 1),
(4, 'Recrutement', 0, 1, '2019-08-06 17:15:02', 1, 1),
(5, 'Gestion de projet', 0, 1, '2019-08-11 17:15:02', 1, 1),
(6, 'Vie professionnelle', 0, 1, '2019-08-11 17:15:02', 1, 1),
(8, 'Formation', 0, 1, '2019-08-11 17:15:02', 1, 1),
(31, 'Home', 1, NULL, '2019-08-28 20:23:01', 0, 1),
(32, 'Actualités', 0, 1, '2019-09-14 22:55:25', 0, 1),
(42, 'A propos de moi', 1, 31, '2019-09-18 19:03:12', 1, 1),
(43, 'Skills', 1, 31, '2019-09-18 19:15:57', 1, 1),
(44, 'Légal', 1, NULL, '2019-09-29 11:50:50', 1, 1),
(45, 'Données', 1, 44, '2019-09-29 11:51:33', 1, 1),
(46, 'Copyright', 1, 44, '2019-10-15 19:45:42', 1, 1),
(47, 'Bonnes pratiques', 0, 1, '2019-11-11 23:14:10', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `post_id` int(10) NOT NULL,
  `status_id` int(5) NOT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `comment_title` varchar(255) NOT NULL,
  `comment_text` varchar(500) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`comment_id`, `user_id`, `post_id`, `status_id`, `pseudo`, `comment_title`, `comment_text`, `date_create`, `date_update`) VALUES
(13, 2, 114, 2, 'Lara', 'Formez-vous', 'Encore faudrait-il déjà appliquer la méthode de base correctement ! Les gens manquent cruellement de formation et souvent cette méthode ne sert qu\'à ajouter de la pression supplémentaires aux développeurs. Bien dommage', '2019-11-13 21:31:23', '2019-11-13 21:33:17'),
(14, 41, 114, 3, 'Elie', 'Abusé', 'Gernre... Va te former toi-même Unknown !', '2019-11-13 21:32:16', '2019-11-13 21:33:29'),
(15, 9, 114, 2, 'Elie', 'Mal pratiqué', 'Je suis d\'accord avec Lara, la méconnaissance dessert complètement cette méthode', '2019-11-13 21:35:37', '2019-11-13 21:36:00'),
(16, 2, 111, 3, 'Lara', 'Ouahou', 'Drolement loin du compte !', '2019-11-14 21:06:06', '2019-11-14 21:32:39');

-- --------------------------------------------------------

--
-- Structure de la table `comment_status`
--

CREATE TABLE `comment_status` (
  `status_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `comment_status`
--

INSERT INTO `comment_status` (`status_id`, `name`) VALUES
(1, 'A modérer'),
(2, 'Validé'),
(3, 'Masqué');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `message_id` int(10) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `date_send` datetime NOT NULL DEFAULT current_timestamp(),
  `sender` varchar(255) NOT NULL,
  `email_sender` varchar(255) NOT NULL,
  `recipient` varchar(255) DEFAULT NULL,
  `email_recipient` varchar(255) NOT NULL,
  `content` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`message_id`, `subject`, `date_send`, `sender`, `email_sender`, `recipient`, `email_recipient`, `content`) VALUES
(1, 'Prise de contact', '2019-11-15 22:51:37', 'Annie', 'braud.a@free.fr', 'Contact aBBlog', 'contact@blog.oc-armellebraud.fr', 'Bonjour, si vous cherchez de rédacteurs pour votre site web je peux vous proposer quelques textes. N\'hésitez pas à me contacter. ');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(10) NOT NULL,
  `title` varchar(155) NOT NULL,
  `chapo` varchar(500) DEFAULT NULL,
  `content` longtext NOT NULL,
  `active` tinyint(1) NOT NULL,
  `date_publish` datetime NOT NULL DEFAULT current_timestamp(),
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime NOT NULL DEFAULT current_timestamp(),
  `date_last_access` datetime DEFAULT NULL,
  `image_file` varchar(255) DEFAULT NULL,
  `is_highlighted` tinyint(1) NOT NULL DEFAULT 0,
  `count` int(10) DEFAULT 0,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`post_id`, `title`, `chapo`, `content`, `active`, `date_publish`, `date_create`, `date_update`, `date_last_access`, `image_file`, `is_highlighted`, `count`, `user_id`) VALUES
(1, 'Utilisation des données personnelles', '', '<h2>Définitions</h2>\n\n<p><strong>Client :</strong> tout professionnel ou personne physique capable au sens des articles 1123 et suivants du Code civil, ou personne morale, qui visite le Site objet des présentes conditions générales.<br /><strong>Prestations et Services :</strong> <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> met à disposition des Clients :</p>\n\n<p><strong>Contenu :</strong> Ensemble des éléments constituants l’information présente sur le Site, notamment textes – images – vidéos.</p>\n\n<p><strong>Informations clients :</strong> Ci après dénommé « Information (s) » qui correspondent à l’ensemble des données personnelles susceptibles d’être détenues par <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> pour la gestion de votre compte, de la gestion de la relation client et à des fins d’analyses et de statistiques.</p>\n\n<p><strong>Utilisateur :</strong> Internaute se connectant, utilisant le site susnommé.</p>\n\n<p><strong>Informations personnelles :</strong> « Les informations qui permettent, sous quelque forme que ce soit, directement ou non, l\'identification des personnes physiques auxquelles elles s\'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).</p>\n\n<p>Les termes « données à caractère personnel », « personne concernée », « sous traitant » et « données sensibles » ont le sens défini par le Règlement Général sur la Protection des Données (RGPD : n° 2016-679)</p>\n\n<h2>1. Présentation du site internet.</h2>\n\n<p>En vertu de l\'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l\'économie numérique, il est précisé aux utilisateurs du site internet <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> l\'identité des différents intervenants dans le cadre de sa réalisation et de son suivi:</p>\n\n<p><strong>Propriétaire</strong> : aBBlog – 6 rue de Brest 75020 Paris<br /><strong>Responsable publication</strong> : aBBlog – contact@blog.oc-armellebraud.fr<br />\nLe responsable publication est une personne physique ou une personne morale.<br /><strong>Webmaster</strong> : Armelle Braud – contact@blog.oc-armellebraud.fr<br /><strong>Hébergeur</strong> : Planet Hoster – 4416 Louis-B.-Mayer Laval H7P 0G1 Québec Canada 0101010101<br /><strong>Délégué à la protection des données</strong> : Armelle Braud – contact@blog.oc-armellebraud.frrrrrrrrrrrrrrrrrr</p>\n\n<p>Les mentions légales sont issues du modèle proposé par le <a href=\"https://fr.orson.io/1371/generateur-mentions-legales\">générateur de mentions légales RGPD d\'Orson.io</a></p>\n\n<h2>2. Conditions générales d’utilisation du site et des services proposés.</h2>\n\n<p>Le Site constitue une œuvre de l’esprit protégée par les dispositions du Code de la Propriété Intellectuelle et des Réglementations Internationales applicables. Le Client ne peut en aucune manière réutiliser, céder ou exploiter pour son propre compte tout ou partie des éléments ou travaux du Site.</p>\n\n<p>L’utilisation du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment, les utilisateurs du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> sont donc invités à les consulter de manière régulière.</p>\n\n<p>Ce site internet est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention. Le site web <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est mis à jour régulièrement par <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> responsable. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.</p>\n\n<h2>3. Description des services fournis.</h2>\n\n<p>Le site internet <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> a pour objet de fournir une information concernant l’ensemble des activités de la société. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> s’efforce de fournir sur le site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> des informations aussi précises que possible. Toutefois, il ne pourra être tenu responsable des oublis, des inexactitudes et des carences dans la mise à jour, qu’elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.</p>\n\n<p>Toutes les informations indiquées sur le site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> sont données à titre indicatif, et sont susceptibles d’évoluer. Par ailleurs, les renseignements figurant sur le site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.</p>\n\n<h2>4. Limitations contractuelles sur les données techniques.</h2>\n\n<p>Le site utilise la technologie JavaScript. Le site Internet ne pourra être tenu responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour Le site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est hébergé chez un prestataire sur le territoire de l’Union Européenne conformément aux dispositions du Règlement Général sur la Protection des Données (RGPD : n° 2016-679)</p>\n\n<p>L’objectif est d’apporter une prestation qui assure le meilleur taux d’accessibilité. L’hébergeur assure la continuité de son service 24 Heures sur 24, tous les jours de l’année. Il se réserve néanmoins la possibilité d’interrompre le service d’hébergement pour les durées les plus courtes possibles notamment à des fins de maintenance, d’amélioration de ses infrastructures, de défaillance de ses infrastructures ou si les Prestations et Services génèrent un trafic réputé anormal.</p>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> et l’hébergeur ne pourront être tenus responsables en cas de dysfonctionnement du réseau Internet, des lignes téléphoniques ou du matériel informatique et de téléphonie lié notamment à l’encombrement du réseau empêchant l’accès au serveur.</p>\n\n<h2>5. Propriété intellectuelle et contrefaçons.</h2>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est propriétaire des droits de propriété intellectuelle et détient les droits d’usage sur tous les éléments accessibles sur le site internet, notamment les textes, images, graphismes, logos, vidéos, icônes et sons. Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>.</p>\n\n<p>Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.</p>\n\n<h2>6. Limitations de responsabilité.</h2>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> agit en tant qu’éditeur du site. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>  est responsable de la qualité et de la véracité du Contenu qu’il publie.</p>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ne pourra être tenu responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site internet <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l’apparition d’un bug ou d’une incompatibilité.</p>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ne pourra également être tenu responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>. Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des utilisateurs. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie …).</p>\n\n<h2>7. Gestion des données personnelles.</h2>\n\n<p>Le Client est informé des réglementations concernant la communication marketing, la loi du 21 Juin 2014 pour la confiance dans l’Economie Numérique, la Loi Informatique et Liberté du 06 Août 2004 ainsi que du Règlement Général sur la Protection des Données (RGPD : n° 2016-679).</p>\n\n<h5>7.1 Responsables de la collecte des données personnelles</h5>\n\n<p>Pour les Données Personnelles collectées dans le cadre de la création du compte personnel de l’Utilisateur et de sa navigation sur le Site, le responsable du traitement des Données Personnelles est : aBBlog. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>est représenté par Armelle Braud, son représentant légal</p>\n\n<p>En tant que responsable du traitement des données qu’il collecte, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> s’engage à respecter le cadre des dispositions légales en vigueur. Il lui appartient notamment au Client d’établir les finalités de ses traitements de données, de fournir à ses prospects et clients, à partir de la collecte de leurs consentements, une information complète sur le traitement de leurs données personnelles et de maintenir un registre des traitements conforme à la réalité. Chaque fois que <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> traite des Données Personnelles, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> prend toutes les mesures raisonnables pour s’assurer de l’exactitude et de la pertinence des Données Personnelles au regard des finalités pour lesquelles <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> les traite.</p>\n\n<h5> 7.2 Finalité des données collectées</h5>\n\n<p> <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est susceptible de traiter tout ou partie des données :</p>\n\n<ul><li>pour permettre la navigation sur le Site et la gestion et la traçabilité des prestations et services commandés par l’utilisateur : données de connexion et d’utilisation du Site, facturation, historique des commandes, etc.</li>\n	<li>pour prévenir et lutter contre la fraude informatique (spamming, hacking…) : matériel informatique utilisé pour la navigation, l’adresse IP, le mot de passe (hashé)</li>\n	<li>pour améliorer la navigation sur le Site : données de connexion et d’utilisation</li>\n	<li>pour mener des enquêtes de satisfaction facultatives sur <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> : adresse email</li>\n	<li>pour mener des campagnes de communication (sms, mail) : numéro de téléphone, adresse email</li>\n</ul><p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ne commercialise pas vos données personnelles qui sont donc uniquement utilisées par nécessité ou à des fins statistiques et d’analyses.</p>\n\n<h5>7.3 Droit d’accès, de rectification et d’opposition</h5>\n\n<p> Conformément à la réglementation européenne en vigueur, les Utilisateurs de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> disposent des droits suivants : </p>\n\n<p>droit d\'accès (article 15 RGPD) et de rectification (article 16 RGPD), de mise à jour, de complétude des données des Utilisateurs droit de verrouillage ou d’effacement des données des Utilisateurs à caractère personnel (article 17 du RGPD), lorsqu’elles sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l\'utilisation, la communication ou la conservation est interdite</p>\n\n<ul><li>droit de retirer à tout moment un consentement (article 13-2c RGPD)</li>\n	<li>droit à la limitation du traitement des données des Utilisateurs (article 18 RGPD)</li>\n	<li>droit d’opposition au traitement des données des Utilisateurs (article 21 RGPD)</li>\n	<li>droit à la portabilité des données que les Utilisateurs auront fournies, lorsque ces données font l’objet de traitements automatisés fondés sur leur consentement ou sur un contrat (article 20 RGPD)</li>\n	<li>droit de définir le sort des données des Utilisateurs après leur mort et de choisir à qui <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> devra communiquer (ou non) ses données à un tiers qu’ils aura préalablement désigné</li>\n</ul><p>Dès que <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> a connaissance du décès d’un Utilisateur et à défaut d’instructions de sa part, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> s’engage à détruire ses données, sauf si leur conservation s’avère nécessaire à des fins probatoires ou pour répondre à une obligation légale.</p>\n\n<p> Si l’Utilisateur souhaite savoir comment <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> utilise ses Données Personnelles, demander à les rectifier ou s’oppose à leur traitement, l’Utilisateur peut contacter <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> par écrit à l’adresse suivante :</p>\n\n<p>aBBlog – DPO, Armelle Braud<br />\n6 rue de Brest 75020 Paris.  </p>\n\n<p>Dans ce cas, l’Utilisateur doit indiquer les Données Personnelles qu’il souhaiterait que <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> corrige, mette à jour ou supprime, en s’identifiant précisément avec une copie d’une pièce d’identité (carte d’identité ou passeport).</p>\n\n<p>Les demandes de suppression de Données Personnelles seront soumises aux obligations qui sont imposées à <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> par la loi, notamment en matière de conservation ou d’archivage des documents. Enfin, les Utilisateurs de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> peuvent déposer une réclamation auprès des autorités de contrôle, et notamment de la CNIL (https://www.cnil.fr/fr/plaintes).</p>\n\n<h5> 7.4 Non-communication des données personnelles</h5>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> s’interdit de traiter, héberger ou transférer les Informations collectées sur ses Clients vers un pays situé en dehors de l’Union européenne ou reconnu comme « non adéquat » par la Commission européenne sans en informer préalablement le client. Pour autant, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> reste libre du choix de ses sous-traitants techniques et commerciaux à la condition qu’il présentent les garanties suffisantes au regard des exigences du Règlement Général sur la Protection des Données (RGPD : n° 2016-679).</p>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> s’engage à prendre toutes les précautions nécessaires afin de préserver la sécurité des Informations et notamment qu’elles ne soient pas communiquées à des personnes non autorisées. Cependant, si un incident impactant l’intégrité ou la confidentialité des Informations du Client est portée à la connaissance de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, celle-ci devra dans les meilleurs délais informer le Client et lui communiquer les mesures de corrections prises. Par ailleurs <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ne collecte aucune « données sensibles ».</p>\n\n<p>Les Données Personnelles de l’Utilisateur peuvent être traitées par des filiales de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> et des sous-traitants (prestataires de services), exclusivement afin de réaliser les finalités de la présente politique.</p>\n\n<p>Dans la limite de leurs attributions respectives et pour les finalités rappelées ci-dessus, les principales personnes susceptibles d’avoir accès aux données des Utilisateurs de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> sont principalement les agents de notre service client.</p>\n\n<h5>7.5 Types de données collectées</h5>\n\n<p>Concernant les utilisateurs d’un Site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, nous collectons les données suivantes qui sont indispensables au fonctionnement du service , et qui seront conservées pendant une période maximale de 1 an mois après la fin de la relation contractuelle:<br />\nnom, prenom, email, position</p>\n\n<p><a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> collecte en outre des informations qui permettent d’améliorer l’expérience utilisateur et de proposer des conseils contextualisés :<br />\nLes données de géolocalisation ne seront en AUCUN CAS utilisée à des fins publicitaires ou commerciales.</p>\n\n<p>Ces  données sont conservées pour une période maximale de 9 mois après la fin de la relation contractuelle</p>\n\n<h2>8. Notification d’incident</h2>\n\n<p>Quels que soient les efforts fournis, aucune méthode de transmission sur Internet et aucune méthode de stockage électronique n\'est complètement sûre. Nous ne pouvons en conséquence pas garantir une sécurité absolue. Si nous prenions connaissance d\'une brèche de la sécurité, nous avertirions les utilisateurs concernés afin qu\'ils puissent prendre les mesures appropriées. Nos procédures de notification d’incident tiennent compte de nos obligations légales, qu\'elles se situent au niveau national ou européen. Nous nous engageons à informer pleinement nos clients de toutes les questions relevant de la sécurité de leur compte et à leur fournir toutes les informations nécessaires pour les aider à respecter leurs propres obligations réglementaires en matière de reporting.</p>\n\n<p>Aucune information personnelle de l\'utilisateur du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> n\'est publiée à l\'insu de l\'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l\'hypothèse du rachat de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> et de ses droits permettrait la transmission des dites informations à l\'éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l\'utilisateur du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>.</p>\n\n<h3>9. Sécurité</h3>\n\n<p>Pour assurer la sécurité et la confidentialité des Données Personnelles et des Données Personnelles de Santé, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> utilise des réseaux protégés par des dispositifs standards tels que par pare-feu, la pseudonymisation, l’encryption et mot de passe.</p>\n\n<p> Lors du traitement des Données Personnelles, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>prend toutes les mesures raisonnables visant à les protéger contre toute perte, utilisation détournée, accès non autorisé, divulgation, altération ou destruction.</p>\n\n<p> </p>\n\n<h2>10. Liens hypertextes « cookies » et balises (“tags”) internet</h2>\n\n<p>Le site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>. Cependant, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.</p>\n\n<p>Sauf si vous décidez de désactiver les cookies, vous acceptez que le site puisse les utiliser. Vous pouvez à tout moment désactiver ces cookies et ce gratuitement à partir des possibilités de désactivation qui vous sont offertes et rappelées ci-après, sachant que cela peut réduire ou empêcher l’accessibilité à tout ou partie des Services proposés par le site.</p>\n\n<h5>10.1. « COOKIES »</h5>\n\n<p> Un « cookie » est un petit fichier d’information envoyé sur le navigateur de l’Utilisateur et enregistré au sein du terminal de l’Utilisateur (ex : ordinateur, smartphone), (ci-après « Cookies »). Ce fichier comprend des informations telles que le nom de domaine de l’Utilisateur, le fournisseur d’accès Internet de l’Utilisateur, le système d’exploitation de l’Utilisateur, ainsi que la date et l’heure d’accès. Les Cookies ne risquent en aucun cas d’endommager le terminal de l’Utilisateur.</p>\n\n<p> <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est susceptible de traiter les informations de l’Utilisateur concernant sa visite du Site, telles que les pages consultées, les recherches effectuées. Ces informations permettent à <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> d’améliorer le contenu du Site, de la navigation de l’Utilisateur.</p>\n\n<p> Les Cookies facilitant la navigation et/ou la fourniture des services proposés par le Site, l’Utilisateur peut configurer son navigateur pour qu’il lui permette de décider s’il souhaite ou non les accepter de manière à ce que des Cookies soient enregistrés dans le terminal ou, au contraire, qu’ils soient rejetés, soit systématiquement, soit selon leur émetteur. L’Utilisateur peut également configurer son logiciel de navigation de manière à ce que l’acceptation ou le refus des Cookies lui soient proposés ponctuellement, avant qu’un Cookie soit susceptible d’être enregistré dans son terminal. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> informe l’Utilisateur que, dans ce cas, il se peut que les fonctionnalités de son logiciel de navigation ne soient pas toutes disponibles.</p>\n\n<p> Si l’Utilisateur refuse l’enregistrement de Cookies dans son terminal ou son navigateur, ou si l’Utilisateur supprime ceux qui y sont enregistrés, l’Utilisateur est informé que sa navigation et son expérience sur le Site peuvent être limitées. Cela pourrait également être le cas lorsque <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ou l’un de ses prestataires ne peut pas reconnaître, à des fins de compatibilité technique, le type de navigateur utilisé par le terminal, les paramètres de langue et d’affichage ou le pays depuis lequel le terminal semble connecté à Internet.</p>\n\n<p> Le cas échéant, <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> décline toute responsabilité pour les conséquences liées au fonctionnement dégradé du Site et des services éventuellement proposés par <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, résultant (i) du refus de Cookies par l’Utilisateur (ii) de l’impossibilité pour <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> d’enregistrer ou de consulter les Cookies nécessaires à leur fonctionnement du fait du choix de l’Utilisateur. Pour la gestion des Cookies et des choix de l’Utilisateur, la configuration de chaque navigateur est différente. Elle est décrite dans le menu d’aide du navigateur, qui permettra de savoir de quelle manière l’Utilisateur peut modifier ses souhaits en matière de Cookies.</p>\n\n<p> À tout moment, l’Utilisateur peut faire le choix d’exprimer et de modifier ses souhaits en matière de Cookies. <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> pourra en outre faire appel aux services de prestataires externes pour l’aider à recueillir et traiter les informations décrites dans cette section.</p>\n\n<p> Enfin, en cliquant sur les icônes dédiées aux réseaux sociaux Twitter, Facebook, Linkedin et Google Plus figurant sur le Site de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> ou dans son application mobile et si l’Utilisateur a accepté le dépôt de cookies en poursuivant sa navigation sur le Site Internet ou l’application mobile de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, Twitter, Facebook, Linkedin et Google Plus peuvent également déposer des cookies sur vos terminaux (ordinateur, tablette, téléphone portable).</p>\n\n<p> Ces types de cookies ne sont déposés sur vos terminaux qu’à condition que vous y consentiez, en continuant votre navigation sur le Site Internet ou l’application mobile de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>. À tout moment, l’Utilisateur peut néanmoins revenir sur son consentement à ce que <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> dépose ce type de cookies.</p>\n\n<h5>10.2. BALISES (“TAGS”) INTERNET</h5>\n\n<p> <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> peut employer occasionnellement des balises Internet (également appelées « tags », ou balises d’action, GIF à un pixel, GIF transparents, GIF invisibles et GIF un à un) et les déployer par l’intermédiaire d’un partenaire spécialiste d’analyses Web susceptible de se trouver (et donc de stocker les informations correspondantes, y compris l’adresse IP de l’Utilisateur) dans un pays étranger.</p>\n\n<p> Ces balises sont placées à la fois dans les publicités en ligne permettant aux internautes d’accéder au Site, et sur les différentes pages de celui-ci.  </p>\n\n<p>Cette technologie permet à <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> d’évaluer les réponses des visiteurs face au Site et l’efficacité de ses actions (par exemple, le nombre de fois où une page est ouverte et les informations consultées), ainsi que l’utilisation de ce Site par l’Utilisateur.</p>\n\n<p> Le prestataire externe pourra éventuellement recueillir des informations sur les visiteurs du Site et d’autres sites Internet grâce à ces balises, constituer des rapports sur l’activité du Site à l’attention de <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a>, et fournir d’autres services relatifs à l’utilisation de celui-ci et d’Internet.</p>\n\n<p> </p>\n\n<h2>11. Droit applicable et attribution de juridiction.</h2>\n\n<p>Tout litige en relation avec l’utilisation du site <a href=\"http://blog.oc-armellebraud.fr/\">http://blog.oc-armellebraud.fr</a> est soumis au droit français. En dehors des cas où la loi ne le permet pas, il est fait attribution exclusive de juridiction aux tribunaux compétents de Paris</p>\n', 1, '2019-08-05 10:50:42', '2019-08-05 10:41:37', '2019-11-04 10:50:42', '2019-11-15 22:25:39', 'privacy.jpg', 0, 75, 1),
(2, 'Mentions légales', '', '<p>C\'est là qu\'on mets les infos de copyrights images, thèmes et textes</p>\n', 1, '2019-10-15 19:46:06', '2019-10-15 19:46:07', '2019-10-15 19:46:07', '2019-11-15 22:25:39', 'Copyright_s.jpg', 0, 7, 1),
(3, 'Pour recruter des développeurs, associez vous à la BattleDev !', 'Vous êtes recruteur ? Vous recherchez des développeurs ? Associez votre entreprise à la BattleDev pour rencontrer ces profils très recherchés.', '<h2>5 000 développeurs ont rendez-vous pour la BattleDev</h2>\n\n<p>Le BDM, édité par HelloWork, organise la <a href=\"https://battledev.blogdumoderateur.com/?utm_source=blogdumoderateur&amp;utm_medium=siteweb&amp;utm_campaign=recruter-developpeurs-associez-battledev-novembre-2019&amp;utm_term=14e%20%C3%A9dition%20de%20la%20BattleDev&amp;utm_content=lien\" target=\"_blank\" rel=\"noreferrer noopener\">14e édition de la BattleDev</a> le 26 novembre 2019. C’est un événement durant lequel les meilleurs développeurs s’affrontent : ils ont deux heures pour résoudre 6 exercices de code, en utilisant leur langage de prédilection. Plus de 5 000 développeurs sont attendus, principalement des spécialistes des langages PHP, Java, Python 3, C#, C, Node.js, C++, Ruby et Scala.</p>\n\n<p>Certains participent pour le challenge ou pour gagner des cadeaux, d’autres souhaitent être repérés par les entreprises qui recrutent en prouvant leurs compétences. Si vous recherchez des développeurs, vous gagnerez du temps et maximiserez vos chances de réussir vos recrutements en associant votre entreprise à l’événement.</p>\n\n<h2>Devenez partenaire pour recruter les meilleurs développeurs</h2>\n\n<p>Lors de leur inscription, les développeurs indiquent s’ils souhaitent être contactés par des recruteurs. Ce filtre permet aux entreprises de gagner du temps, puisqu’elles sont assurées de contacter des développeurs ouverts aux opportunités. Le vivier de candidats intéressés est important, ce qui permet aux entreprises d’accélérer leurs recrutements.</p>\n\n<p>L’autre avantage de la BattleDev est que vous pourrez échanger avec les meilleurs développeurs, ceux qui auront le mieux réussi les exercices proposés. Vous aurez déjà pré-validé un certain nombre de compétences techniques avant d’échanger plus précisément avec eux : ce second filtre permet d’accéder aux meilleurs développeurs.</p>\n\n<p>L’événement vous intéresse ? Remplissez ce formulaire, nous vous recontacterons très rapidement pour vous permettre d’associer votre entreprise à la BattleDev.</p>\n\n<p class=\"blockquote-footer\" style=\"text-align:right;\"><a href=\"https://www.blogdumoderateur.com/recruter-developpeurs-associez-battledev-novembre-2019/#\">Article de Thomas Coëffé</a> pour le site le <a href=\"https://www.blogdumoderateur.com\">Blog Du Modérateur</a> paru le 18 octobre 2019</p>\n', 1, '2019-08-03 20:10:25', '2019-08-03 18:45:00', '2019-11-13 20:10:25', '2019-11-15 22:25:40', 'u8pyroc5r725nn3mveu4.jpg', 0, 48, 10),
(4, 'Une bombe à retardement dans des feuilles de calcul', 'Un ancien sous-traitant de Siemens a plaidé coupable la semaine dernière pour avoir mis en place des bombes logiques dans des feuilles de calcul qu\'il a créées pour la société.\nFace à ces accusations, David Tinley, 62 ans, originaire d\'une ville proche de Pittsburgh, encourt jusqu\'à 10 ans de prison, une amende de 250 000 dollars, ou les deux.', '<h2>Les bombes logiques n\'ont pas été détectées pendant deux ans</h2>\n\n<p><a href=\"https://fr.scribd.com/document/419389583/5948342-0-19339\" target=\"_blank\" title=\"\" rel=\"noreferrer noopener\">Selon des documents judiciaires</a>, Tinley a fourni des services logiciels pour les bureaux de Siemens à Monroeville, en Pennsylvanie, pendant près de dix ans. Parmi les tâches qui lui ont été confiées figurait la création de feuilles de calcul que la société utilisait pour gérer les commandes de matériel.</p>\n\n<p>Les feuilles de calcul incluaient des scripts personnalisés qui mettaient à jour le contenu du fichier en fonction des commandes en cours stockées dans d’autres documents distants, permettant à la société d’automatiser la gestion des stocks et des commandes.</p>\n\n<p>Cependant, alors que les dossiers de Tinley ont fonctionné sans problème pendant des années, ils ont commencé à ne plus fonctionner correctement vers 2014. Selon des documents judiciaires, Tinley avait installé des \"bombes logiques\" qui se déclenchaient après une certaine date et faisaient crasher les fichiers.</p>\n\n<p>Chaque fois que les scripts se bloquaient, Siemens appelait Tinley, qui réparait les fichiers moyennant des frais.</p>\n\n<p>La combine a duré deux ans, jusqu\'en mai 2016. À cette période, la tromperie de Tinley a été dévoilée par les employés de Siemens.<a href=\"https://www.law360.com/commercialcontracts/articles/1180318/after-delay-pa-programmer-pleads-to-siemens-sabotage\" target=\"_blank\" title=\"\" rel=\"noreferrer noopener\"> Selon un rapport de Law360</a>, le pot aux roses a été découvert lorsque Tinley était en dehors de la ville et qu’il a dû confier un mot de passe administratif pour les feuilles de calcul au personnel informatique de Siemens afin qu\'ils puissent corriger eux-mêmes les scripts de buggy et exécuter une commande urgente.</p>\n\n<p>Les informaticiens de Siemens ont découvert la bombe logique, et tout a commencé à partir de là. Tinley a été accusé en mai et a <a href=\"https://www.justice.gov/usao-wdpa/pr/siemens-contract-employee-intentionally-damaged-computers-planting-logic-bombs-programs\" target=\"_blank\" title=\"\" rel=\"noreferrer noopener\">plaidé coupable la semaine dernière</a>, le 19 juillet. L\'audience de détermination de la peine de l\'entrepreneur est prévue pour le 8 novembre.</p>\n\n<h2>Une peine de prison attendue</h2>\n\n<p>Pour Tinley, les chances d’échapper à la prison ferme sont minces. En 2006, un ancien employé d\'UBS PaineWebber <a href=\"https://www.zdnet.com/article/logic-bomb-backfires-on-insider-hacker/\" target=\"_blank\" title=\"\" rel=\"noreferrer noopener\">a été condamné à huit ans de prison </a>pour avoir mis en place une bombe logique sur le réseau de l\'entreprise avant de jouer sur la chute du cours des actions.</p>\n\n<p>En septembre 2018, un homme d\'Atlanta a été condamné à deux ans de prison pour avoir <a href=\"https://www.zdnet.com/article/man-gets-two-years-in-prison-for-sabotaging-us-army-servers-with-logic-bomb/\" target=\"_blank\" title=\"\" rel=\"noreferrer noopener\">mis en place une bombe logique sur l\'une des bases de données de paie de l\'armée américaine,</a> ce qui a entraîné un retard de 17 jours dans le paiement de la solde de l\'armée de réserve américaine.</p>\n\n<p> </p>\n\n<p class=\"blockquote-footer\" style=\"text-align:right;\"><a href=\"https://www.zdnet.fr/actualites/un-sous-traitant-de-siemens-met-en-place-une-bombe-a-retardement-dans-des-feuilles-de-calcul-d-entreprise-39888161.htm\">Article original de Catalin Cimpanu</a> paru sur <a href=\"https://www.zdnet.fr/\">ZDNet</a> le 23 juillet 2019</p>\n', 1, '2019-09-17 20:10:44', '2019-08-03 18:45:00', '2019-11-13 20:10:44', '2019-11-15 22:25:40', 'Trojean_min.jpg', 1, 594, 4),
(19, 'CanIEmail : vérifiez les compatibilités de vos emails', 'Avec CanIEmail, les intégrateurs d\'emails peuvent enfin vérifier la compatibilité des balises HTML et des fonctionnalités CSS avec la plupart des messageries.', '<h2>CanIEmail, l’équivalent de CanIUse pour l’intégration des emails</h2>\n\n<p><a href=\"https://caniuse.com/\" target=\"_blank\" rel=\"noreferrer noopener\">CanIUse</a> est un outil utilisé très fréquemment par les intégrateurs web. Il permet de connaître la compatibilité des dernières technologies du web avec les navigateurs. Vous pouvez ainsi savoir si Chrome 76 supporte les images au format WebP (oui) ou si Internet Explorer interprète correctement la déclaration CSS position:sticky (non). Le service va jusqu’à segmenter les compatibilités en fonction des versions des navigateurs et donne des indications en cas de support partiel.</p>\n\n<p><a href=\"https://www.caniemail.com/\" target=\"_blank\" rel=\"noreferrer noopener\">CanIEmail</a> arrive enfin, pour le plus grand bonheur des intégrateurs d’emails. Le principe est similaire à CanIUse : il suffit de saisir une balise HTML ou une propriété CSS pour connaître sa compatibilité avec les principales boîtes de messagerie du marché : Apple Mail sur macOS et iOS, Gmail sur le web, iOS et Android, Outlook sur Windows (2003-2019), macOS, Outlook.com, iOS et Android, Yahoo! Mail, Mozilla Thunderbird, Orange, Samsung, SFR… Comme sur CanIUse, les versions des messageries sont précisées et des notes sont ajoutées en cas de support partiel des fonctionnalités HTML/CSS.</p>\n\n<h2>Vérifiez la compatibilité des balises HTML/CSS avec Gmail, Outlook…</h2>\n\n<p>Vous pouvez utiliser CanIEmail de deux façons : en cherchant directement la balise HTML ou la fonction CSS pour laquelle vous avez un doute ; vous pouvez aussi cliquer sur Features, en haut à droite du site, pour parcourir les déclarations CSS et balises HTML référencées sur CanIEmail. CanIEmail est un projet collaboratif, les développeurs front peuvent y contribuer <a href=\"https://github.com/hteumeuleu/caniemail/tree/master/_features\" target=\"_blank\" rel=\"noreferrer noopener\">sur GitHub</a> (pour signaler une erreur ou enrichir le service). Au lancement de CanIEmail, 50 fonctionnalités HTML et CSS peuvent être testées, sur plus de 25 clients.</p>\n\n<p>À noter que les développeurs de CanIEmail ne sont pas les mêmes que ceux de CanIUse, (mais la ressemblance entre les deux services est totalement assumée) : pour CanIEmail, tout le mérite revient à Rémi Parmentier, plus connu sous le pseudo <a href=\"https://twitter.com/HTeuMeuLeu\">HTeuMeuLeu</a>.</p>\n', 1, '2019-09-19 23:18:45', '2019-09-18 12:30:20', '2019-11-11 23:18:45', '2019-11-15 22:25:40', 'caniemail.png', 0, 12, 5),
(24, 'Présentation', 'Ce que j\'aime dans mon métier, c\'est la faculté de se fixer des problèmes à résoudre. ', '<p>Ancienne documentaliste d\'entreprise, je me suis reconvertie dans le développement informatique il y a maintenant 10 ans. D\'un profil atypique, j\'en ai fait une force : adaptabilité, autonomie, sociabilité... Aujourd\'hui j\'asseois mes compétences en suivant une formation de développeur PHP/Symfony en plus de mon travail de développeur Coldfusion au sein du Groupe Argus. Cela me rappelle à quel point apprendre est grisant !</p>\n', 1, '2019-09-18 15:52:24', '2019-09-18 17:41:39', '2019-11-15 15:52:24', '2019-11-15 22:25:44', 'profil_big.jpg', 0, 4, 1),
(100, 'Recherche', 'fab fa-searchengin', '<p>Explorer possibilités</p>\n', 1, '2019-09-18 21:47:43', '2019-09-18 19:16:50', '2019-11-11 21:47:43', NULL, NULL, 0, NULL, 1),
(101, 'Back-end', 'fas fa-laptop-code', '<p>Plusieurs langages</p>\n', 1, '2019-09-18 21:48:14', '2019-09-18 19:17:17', '2019-11-11 21:48:14', NULL, NULL, 0, NULL, 1),
(102, 'Design', 'fas fa-magic', '<p>Utilisateur final</p>\n', 1, '2019-09-18 21:37:11', '2019-09-18 19:18:00', '2019-11-11 21:37:11', NULL, NULL, 0, NULL, 1),
(103, 'Organisation', 'fas fa-tools', '<p>mettre en place et tester des outils de travail collaboratif</p>\n', 1, '2019-09-18 21:45:19', '2019-09-18 19:18:19', '2019-11-11 21:45:19', NULL, NULL, 0, NULL, 1),
(104, 'Analytics', 'fas fa-chart-line', '<p>To an English person, it will seem like simplified English,told me what</p>\r\n', 1, '2019-09-18 19:18:19', '2019-09-18 20:20:52', '2019-09-18 20:20:52', NULL, NULL, 0, NULL, 1),
(105, 'Apprendre', 'fas fa-chalkboard-teacher', '<p>En formation après reconversion</p>\n', 1, '2019-09-18 21:48:42', '2019-09-18 20:21:39', '2019-11-11 21:48:42', NULL, NULL, 0, NULL, 1),
(106, 'Encadrement', 'fas fa-hands-helping', '<p>Accompagnement, transmission</p>\n', 1, '2019-09-18 21:41:52', '2019-09-18 20:21:59', '2019-11-11 21:41:52', NULL, NULL, 0, NULL, 1),
(109, 'Hauteur et position des images - Masonery &amp; Isotope', 'Avec l\'arrivée du responsive design, les gens ont commencé à redimensionner les fenêtres jute pour voir si les sites Web s\'adaptaient bien. Est ce qu\'ils venaient juste pour ça ? Bien sûr, les visiteurs normaux qui viennent lire ou voir quelque chose ne font pas ce genre de choses !', '<p>Pour être sûrs que leur site passerait bien le test du redimensionnement intempestif, beaucoup de développeurs ont alors doté leur site Web des librairies <a href=\"http://masonry.desandro.com/\" target=\"_blank\" rel=\"noreferrer noopener\">Masonery</a> ou <a href=\"http://isotope.metafizzy.co/\" target=\"_blank\" rel=\"noreferrer noopener\">Isotope</a>. Probablement aussi parce qu\'ils ont cet effet magique <em>shuffle </em>lors du redimensionnement (et du tri). Cette fonctionnalité s\'appuie sur les positions absolues et les dimensions des éléments. Le problème est que, dans le responsive design (ou fluid Design), la dimension de l’image n\'est pas spécifiée pour qu\'elle puisse être redimensionnée à la demande....</p>\n\n<p> Je vais illustrer le problème en évoquant la librairie Masonery car elle est totalement gratuite, mais la solution s’applique également avec Isotope.</p>\n\n<h2>Problème</h2>\n\n<p>Avec jQuery, les initialisations sont généralement faites dans <em>$(document).ready()</em>. </p>\n\n<pre>\n<code class=\"language-javascript\">$(function(){\n	var $container = $(\'#container\');\n	// initialize\n	$container.masonry({\n		itemSelector: \'.item\'\n	});\n});</code></pre>\n\n<p>Et sans spécifier de dimensions, les images à l\'intérieur pourront apparaître comme ceci :</p>\n\n<p><img alt=\"\" src=\"http://localhost/blog-exo/uploads/images/blog/corps-articles/l_image-issue-1.jpg\" style=\"height:469px;width:800px;\" /></p>\n\n<p>Ce n\'est pas bon. Les images se chevauchent. Mes chats ne sont pas contents.</p>\n\n<h2>Pourquoi les images se chevauchent ?</h2>\n\n<p>Comme je l\'ai écrit au début, les librairies requièrent des positions et des dimensions absolues. De toute évidence, vous ne pouvez pas obtenir les valeurs appropriées de quelque chose qui n\'existe pas encore. En plaçant l\'initialisation dans <em>$(document).ready()</em>, vous devrez certainement faire face à ce problème lors du chargement de la première page. Pourquoi ? Parce qu’il est déclenché lorsque le document est prêt et qu’il est généralement bien avant que les images. Il se peut qu\'au prochain chargement, tout se passe bien si le navigateur utilise des images en cache.</p>\n\n<h2>Solution numéro 1</h2>\n\n<p>Il suffit de placer l’initialisation de la librairie dans <em>$(window).load()</em> au lieu de <em>$(document) .ready()</em></p>\n\n<pre>\n<code class=\"language-javascript\">$(window).load(function(){\n	var $container = $(\'#container\');\n	// initialize\n	$container.masonry({\n		itemSelector: \'.item\'\n	});\n});</code></pre>\n\n<p>De cette façon, elle s’initialisera APRÈS que toutes les ressources soient chargées. C\'est beaucoup mieux n\'est ce pas ?</p>\n\n<p><img alt=\"\" src=\"http://localhost/blog-exo/uploads/images/blog/corps-articles/l_image-issue-2.jpg\" style=\"height:634px;width:800px;\" /></p>\n\n<p>Mes chats sont contents !</p>\n\n<h2>Solution numéro 2</h2>\n\n<p>Attendre que toutes les ressources soient chargées est une bonne solution si vous n’en n\'avez pas trop. Dans le cas contraire, regardez plutot du côté de <a href=\"https://github.com/desandro/imagesloaded\" target=\"_blank\" rel=\"noreferrer noopener\">imagesLoaded</a>. La solution avec imagesLoaded est décrite sur la page des <a href=\"http://masonry.desandro.com.s3-website-us-east-1.amazonaws.com/extras.html#imagesloaded\" target=\"_blank\" rel=\"noreferrer noopener\">Extras de Masonery</a>, je ne vais donc pas la répéter ici :o)</p>\n\n<p style=\"text-align:right;\"><em><span class=\"text-small\">Traduction de l\'article de <a href=\"https://aiocollective.com/blog/author/kokers/\">Eliza Witkowska</a> paru sur le site <a href=\"https://aiocollective.com/\">https://aiocollective.com</a>. </span></em></p>\n', 1, '2019-11-01 20:09:36', '2019-11-01 19:00:09', '2019-11-13 20:09:36', '2019-11-15 22:25:39', 'masonery.png', 0, 50, 1),
(110, 'Des outils de gestion de projet pour développeurs ', 'Trouver l’application idéale pour faciliter sa vie de développeur n’est pas chose facile. Tout le monde a des besoins et des manières de travailler différentes. Sans oublier que pour chercher et tester différents outils, il faut du temps ! Parce que je connais bien cette problématique, j\'ai décidé de partager quelques outils qui m\'aident lorsque je travaille seule ou équipe d\'une dizaine de personnes. ', '<p>J\'ai testé de nombreux outils de gestion de projet et particulièrement la fonctionnalité <em>Planning de Gantt</em>. Les gratuits et les payants (en période d\'essai). Ce sont de bons assistants : ils vous aident à vous organiser et ainsi à libérer un peu votre cerveau. Personnellement, j\'ai souvent besoin de planifier et de me  projeter dans l\'avenir c\'est pourquoi je n\'utilise pas toutes les fonctionnalités de ce genre d\'outil mais je pense que c\'est le plus important.</p>\n\n<p>Je vous encourage donc à en tester au moins quelques-uns avant de vous engager. Une fois que vous en avez apprivoisé un, il n’est pas si facile d\'en changer car se réhabituer à un outil comme celui là demande parfois un certain temps d\'adaptation tant ils peuvent s\'avèrer complexes.</p>\n\n<h2>TeamGantt</h2>\n\n<p>J\'utilise actuellement la version gratuite de <a href=\"https://www.teamgantt.com\" target=\"_blank\" rel=\"noreferrer noopener\">Team Gantt</a>. Cette version est limitée à un projet ouvert en court, à 3 utilisateurs et à 5 équipiers. Dans cette version, l\'outil n\'est donc pas pas adapté à une activité multiprojets ou à un travail en équipe. A moins de vouloir jongler sans arrêt en ouvrant et fermant des projets. Cela dit, pour moi qui traite un projet à la fois, elle me suffit. Ce que j\'aime particulièrement, c\'est son style et l\'intuitivité de l\'interface. J\'aime les choses agréables à utiliser et je trouve le planning de Gantt vraiment beau. Il est également trés facile de modifier, déplacer, attribuer les tâches par simple glisser déposer ! Il permet aussi d\'effectuer le suivi de son équipe : tâches, temps passé, charge de travail.</p>\n\n<p><img alt=\"Un exemple de TeamGantt\" src=\"http://localhost/blog-exo/uploads/images/blog/corps-articles/l_teamGantt.png\" style=\"height:450px;width:800px;\" /></p>\n\n<p>Voulant utiliser les fonctionnalités premium (export PDF, , j’ai consulté les tarifs. Il s’avère qu\'il est très cher pour un freelance. J’ai donc commencé à chercher des alternatives à <a href=\"https://www.teamgantt.com\" target=\"_blank\" rel=\"noreferrer noopener\">Team Gantt</a> et trouvé <a href=\"https://clickup.com/\" target=\"_blank\" rel=\"noreferrer noopener\">ClickUp</a>.</p>\n\n<h2>Clickup</h2>\n\n<p>Clickup est tout nouveau, il lui manque donc certaines fonctionnalités dont vous pourriez avoir besoin (comptes d\'invité, gestion d\'autorisation). Il est constemment en amélioration et l\'équipe de développement introduit activement des corrections et nouvelles fonctionnalités. Le support est super réactif et génial, les commentaires des utilisateurs sont pris en compte (vraiment) et l\'application est très intuitive. </p>\n\n<p>Le planning de Gantt est en direct  et la feuille de route et les demandes de fonctionnalités sont publiques. Dans ClickUp, vous avez un plan de stockage gratuit (100 Mo de stockage) et illimité. Le forfait payant vous permet simplement de stocker autant de fichiers que vous le souhaitez. De plus il est est abordable et par utilisateur.</p>\n\n<p><img alt=\"\" src=\"http://localhost/blog-exo/uploads/images/blog/corps-articles/l_clickup-1024x595.jpg\" style=\"height:464px;width:800px;\" /></p>\n\n<p>Ces deux outils sont bien évidemment en anglais ;)</p>\n', 1, '2019-11-03 20:09:11', '2019-11-03 17:21:43', '2019-11-13 20:09:11', '2019-11-15 22:25:39', 'v093vyl5ucn4ob4ubro4.jpg', 1, 31, 1);
INSERT INTO `posts` (`post_id`, `title`, `chapo`, `content`, `active`, `date_publish`, `date_create`, `date_update`, `date_last_access`, `image_file`, `is_highlighted`, `count`, `user_id`) VALUES
(111, 'Le salaire des profils tech en France en 2019', 'On lit tout et son contraire sur les salaires des professionnels du numérique. C’est peut-être dû aux réalités très différentes cachées derrière un même intitulé. Si nous parlons aujourd’hui de l’étude menée par Silkhom, c’est parce que pour une fois, les fourchettes proposées semblent cohérentes avec la réalité du marché.', '<h2>Une étude crédible sur les salaires de la tech</h2>\n\n<p>On lit tout et son contraire sur les salaires des professionnels du numérique. C’est peut-être dû aux réalités très différentes cachées derrière un même intitulé. Le quotidien d’un chef de projet digital dans une TPE en région n’est pas le même que celui d’un chef de projet digital dans un grand groupe international à Paris. Leurs salaires non plus. Dans le digital, plus que dans d’autres secteurs, on peut exercer le même métier dans des contextes totalement différents. Si on ajoute à cela l’évolution rapide des métiers du web et leur difficile définition, on arrive très souvent à des études de rémunération bancales, diffusant parfois l’idée que les salaires des professionnels du web sont mirobolants – alors que la situation de chacun est souvent bien plus complexe. Si nous parlons aujourd’hui de <a href=\"https://www.silkhom.com/barometre-des-salaires-2019-combien-gagnent-les-profils-tech-les-plus-recherches/\" target=\"_blank\" rel=\"noreferrer noopener\">l’étude menée par Silkhom</a>, c’est parce que pour une fois, les fourchettes proposées semblent cohérentes avec la réalité du marché.</p>\n\n<h2>Le salaire des développeurs en 2019</h2>\n\n<p>Parmi les métiers techniques étudiés, on retrouve bien évidemment les développeurs informatiques. Leur salaire dépend de leurs spécialités. Ainsi, les développeurs front-end (intégrateurs) sont relativement peu rémunérés par rapport aux autres développeurs : de 28 à 33 000 euros par an en région, contre 34 à 43 000 euros à Paris pour les juniors. Lorsqu’ils deviennent lead dev front-end, ils peuvent toucher jusqu’à 70 000 euros annuels en région parisienne. Le salaire obtenu dépendra notamment des frameworks maîtrisés et des connaissances web transverses (sensibilité SEO, bonne gestion des interfaces en responsive etc.).</p>\n\n<p style=\"text-align:center;\"><img alt=\"\" src=\"http://blog.oc-armellebraud.fr/uploads/images/blog/corps-articles/l_salaire-dev-front-664x445.png\" style=\"height:335px;width:500px;\" /></p>\n\n<p>Certaines spécialités se tiennent dans un mouchoir de poche côté salaire : on pense ici aux développeurs Java JEE, .NET, Python, WinDev, Cobol, PHP, Back-end ou Fullstack JavaScript et aux développeurs mobiles. À quelques différences près, ils démarrent leur carrière aux alentours de 32 à 38 000 euros en région, plutôt 35-45K à Paris, et peuvent obtenir des rémunérations supérieures à 50 000 euros après 5 ans d’expérience en région parisienne. Le point commun des développeurs informatiques : leur salaire augmente assez rapidement au cours de leur carrière. Ci-dessous, le salaire d’un développeur .NET.</p>\n\n<p style=\"text-align:center;\"><img alt=\"\" src=\"http://blog.oc-armellebraud.fr/uploads/images/blog/corps-articles/l_salaire-dev-dot-net-664x365.png\" style=\"height:274px;width:500px;\" /></p>\n\n<p>Le salaire d’un développeur .NET : de 32 000 euros à plus de 55 000 euros. Crédits : Silkhom.</p>\n\n<p>Les ingénieurs (études et développement, big data, qualité, cloud, DevOps, système et réseau, base de données…) démarrent généralement leur carrière à des niveaux proches de leurs collègues développeurs, mais atteignent des rémunérations plus élevées après seulement quelques années d’expérience.</p>\n\n<p style=\"text-align:center;\"><img alt=\"\" src=\"http://blog.oc-armellebraud.fr/uploads/images/blog/corps-articles/l_salaire-ingenieur-big-data-664x365.png\" style=\"height:274px;width:500px;\" /></p>\n\n<h2>Le salaire des autres professionnels techniques en 2019</h2>\n\n<p>L’étude de Silkhom ne se contente pas d’analyser le salaire des développeurs. D’autres spécialités techniques sont passées au crible, voici les résultats obtenus pour les professions les plus emblématiques de la tech :</p>\n\n<ul><li>\n	<p><strong>CTO : de </strong>50-60K (région, confirmé) à plus de 100K (Paris, expert)</p>\n	</li>\n	<li>\n	<p><strong>DSI :</strong> de 60-80K (région, confirmé) à 90-200K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Responsable infrastructure :</strong> de 45-55K (région, confirmé) à 70K+ (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Scrum master :</strong> de 35-38K (régions, confirmé) à 52-65K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Data scientist :</strong> de 45-50K (régions, junior) à 70-100K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Chef de projet :</strong> de 30-38K (régions, junior) à 65-80K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>UX/UI designer :</strong> de 28-34K (régions, junior) à 45-52K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Product owner :</strong> de 30-36K (régions, junior) à 50-60K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>DevOps :</strong> de 30-38K (régions, junior) à plus de 60K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>SRE – Site Reliability Engineer :</strong> de 32-43K (régions, junior) à 80K+ (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Administrateur BDD :</strong> de 30-38K (régions, junior) à 70K+ (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Administrateur système :</strong> de 28-36K (régions, junior) à 46-55K (Paris, senior)</p>\n	</li>\n	<li>\n	<p><strong>Technicien système :</strong> de 23-25K (régions, junior) à 32-36K (Paris, confirmé)</p>\n	</li>\n</ul><p>Pour chaque métier, vous accédez à la fourchette de salaire pour les juniors, confirmés, seniors voire lead dev en régions, dans les grandes villes (une donnée rare) et à Paris. <a href=\"https://www.silkhom.com/barometre-des-salaires-2019-combien-gagnent-les-profils-tech-les-plus-recherches/\" target=\"_blank\" rel=\"noreferrer noopener\">Vous pouvez consulter l’étude de Silkhom en intégralité ici</a>.</p>\n', 1, '2019-11-05 20:08:24', '2019-11-05 11:39:07', '2019-11-13 20:08:24', '2019-11-15 22:25:39', 'salaire-tech-france-2019-1200x653.jpg', 1, 96, 10),
(112, '25 ans de PHP', 'Le PHP Day 2019, qui s’est tenue cette année à Vérone (Italie), s’est achevé par un exposé étonnamment réfléchi du créateur de PHP. Dernier intervenant de la conférence, Rasmus Lerdorf, 50 ans, a passé en revue les 25 dernières années du langage de programmation et présenté quelques leçons tirées de l\'évolution d’un préprocesseur hypertexte en un acteur majeur de l’infrastructure Web.', '<p>Partageant au passage ses anecdotes sur une vie passée au service de la techologie et sur les grands bouleversements qui ont secoués cette dernière, Lerdorf a également présenté son propre point de vue sur ce que tout cela signifiait, et a même donné de bons conseils aux nouvelles générations de jeunes hackers pleins d\'espoir qui pourraient aussi vouloir changer la donne.</p>\n\n<div class=\"youtube-embed-wrapper\" style=\"padding-bottom:56.25%;padding-top:30px;height:0;\"><iframe frameborder=\"0\" height=\"315\" src=\"https://www.youtube-nocookie.com/embed/wCZ5TJCBWMg?rel=0\" style=\"width:100%;height:100%;\" width=\"560\"></iframe></div>\n\n<p class=\"legend\" style=\"text-align:center;\">phpday 2019 | Verona, 10-11 Mai | phpday.it</p>\n', 1, '2019-10-16 20:08:00', '2019-11-06 20:51:52', '2019-11-13 20:08:00', '2019-11-15 22:25:38', 'zwzf56j5sgzrthqwp5vg.jpg', 0, 25, 1),
(114, 'What’s up agile : Modern Agile', '18 ans après, le manifeste agile est devenu une référence dans le monde de l’agilité bien qu’il existe des contestations et des propositions d’évolution comme le Modern Agile.', '<p>Le <strong>Manifeste Agile</strong> ou Agile Manifesto a été écrit en février 2001 par des spécialistes du développement logiciel. Il en est sorti 4 valeurs et 12 principes fondateurs agile.</p>\n\n<blockquote>\n<p>Nous découvrons comment mieux développer des logiciels par la pratique et en aidant les autres à le faire. Ces expériences nous ont amenés à valoriser:</p>\n\n<ul><li>Les individus et leurs interactions PLUS que les processus et les outils</li>\n	<li>Des logiciels opérationnels PLUS qu’une documentation exhaustive</li>\n	<li>La collaboration avec les clients PLUS que la négociation contractuelle</li>\n	<li>L’adaptation au changement PLUS que le suivi d’un plan.</li>\n</ul><p>Autrement dit, même s’il y a de la valeur dans les éléments à droite, nous valorisons davantage les éléments à gauche.” </p>\n\n<p class=\"blockquote-footer\"><a href=\"http://agilemanifesto.org/\" target=\"_blank\" rel=\"noreferrer noopener\">agilemanifesto.org</a></p>\n</blockquote>\n\n<p>Cela était un changement par rapport à la manière dont on développait les logiciels et on créait de la valeur au sein d’un produit.</p>\n\n<h2>Qu’est ce que le Modern Agile ?</h2>\n\n<p>Le Modern Agile se présente comme une version évoluée du Manifeste Agile. Il a été créé par Joshua Kerievsky, CEO d’Industrial Logic, et introduit lors de <a href=\"https://www.agilealliance.org/resources/videos/modern-agile/\" target=\"_blank\" rel=\"noreferrer noopener\">sa conférence à l’Agile 2016</a>.</p>\n\n<blockquote>\n<p>Modern Agile est une communauté de personnes intéressées par le fait de découvrir de meilleures façons d’obtenir des résultats exceptionnels. Il tire parti de la sagesse de nombreuses entreprises, est piloté par des principes et est indépendant de tout framework.</p>\n\n<p class=\"blockquote-footer\">Joshua Kerievsky, PDG, Industrial Logic</p>\n</blockquote>\n\n<p> </p>\n\n<p>Le Modern Agile propose 4 principes :</p>\n\n<ul><li>“Make people awesome”</li>\n	<li>“Make safety a prerequisite”</li>\n	<li>“Experiment and learn rapidly”</li>\n	<li>“Deliver Value Continuously”</li>\n</ul><p>Eric Siber, Agile Java Craftsman et Creative Ecosystem Organizer, a fait l’exercice de questionner la twittosphère afin d’en avoir <a href=\"https://eric.siber.fr/2016/09/08/modern-agile-declinaison-francophone-principes/\" target=\"_blank\" rel=\"noreferrer noopener\">une version française</a> qui se résume par :</p>\n\n<p style=\"text-align:center;\"><a href=\"http://twitter.com/esiber/status/778505249476739072/photo/1?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E778505249476739072&amp;ref_url=https%3A%2F%2Fblog.octo.com%2Fwhats-up-agile-modern-agile%2F\"><img alt=\"\" height=\"674\" src=\"http://blog.oc-armellebraud.fr/uploads/images/blog/corps-articles/l_0f7boilda82a6dqniywp.png\" width=\"450\" /></a></p>\n\n<p>Si le manifeste agile est très axé développement logiciel, le Modern Agile est plus centré sur un état d’esprit. Il est totalement applicable à d’autres corps de métier.</p>\n\n<p>Je vous propose de décortiquer ces 4 principes.</p>\n\n<h2>Premier principe : Faire grandir les gens</h2>\n\n<p>Le principe “Make People Awesome” s’adresse aux personnes qui vont créer des choses afin d’aider d’autres personnes à grandir, et donc vont tendre à être fantastiques.</p>\n\n<p>On parle ici d’écosystème favorisant le faire grandir les personnes qui achètent, font, utilisent, vendent ou financent un produit contribuant à rendre l’autre “awesome”.</p>\n\n<p>Pour ce faire, il est important d’apprendre à connaître sur l’autre :</p>\n\n<ul><li>son contexte,</li>\n	<li>ses problématiques,</li>\n	<li>ce qui le/la freine,</li>\n	<li>quelles sont ses ambitions.</li>\n</ul><h2>Deuxième principe : Installer confiance et bienveillance</h2>\n\n<p>“Make safety a prerequisite” consiste à mettre les personnes en sécurité pour les aider à grandir. Ici, on parle particulièrement de sécurité humaine qui inclut :</p>\n\n<ul><li>la sécurité physique</li>\n	<li>la sécurité psychologique</li>\n</ul><blockquote>\n<p>Si vous avez une culture de la peur, aucune de vos pratiques ou processus fantaisistes ne vous aideront.</p>\n\n<p class=\"blockquote-footer\">Joshua Kerievsky</p>\n</blockquote>\n\n<p>La peur du changement, être vulnérable, prendre des risques, exprimer une opinion ou être blâmé sont des freins à l’amélioration. Le “droit à l’erreur” est une notion forte en agile qu’il faut favoriser.</p>\n\n<p>Pour cela, il est évoqué le fait de :</p>\n\n<ul><li>S’encourager mutuellement</li>\n	<li>S’écouter</li>\n	<li>Reformuler pour mieux se comprendre</li>\n	<li>Ne pas se dominer, ni s’interrompre</li>\n	<li>Être attentif et intéressé sans jugement</li>\n</ul><p>Ce principe tire beaucoup d’autres notions : la collaboration, la solidarité, l’écoute, la confiance, le droit à l’erreur, l’autonomie, la responsabilisation, l’auto-organisation, etc.</p>\n\n<h2>Troisième principe : Expérimenter et apprendre rapidement</h2>\n\n<p>Le meilleur moyen d’apprendre est d’expérimenter, le plus rapidement possible et le plus souvent possible, afin d’en tirer vite des leçons et ainsi de suite. C’est le processus d’amélioration continue.</p>\n\n<p>La “Feedback loop” , un des composants de base de la méthodologie Lean Startup, est un bon résumé pour “Experiment and learn rapidly” :</p>\n\n<p>Build ⇨ Measure ⇨Learn</p>\n\n<h2>Quatrième Principe : Apporter de la valeur en continu</h2>\n\n<p>La quatrième principe “Deliver Value Continuously” est très similaire au premier principe du Manifeste Agile :</p>\n\n<blockquote>\n<p>Notre plus haute priorité est de satisfaire le client en livrant rapidement et régulièrement des fonctionnalités à grande valeur ajoutée ».</p>\n</blockquote>\n\n<p> </p>\n\n<p>Il a pour objectif de délivrer de la valeur :</p>\n\n<ul><li>au plus tôt</li>\n	<li>en priorisant les éléments dont les clients ont besoin et à fort impact</li>\n	<li>en ayant une boucle de feedback rapide et de manière constante</li>\n</ul><h2>En résumé</h2>\n\n<p>Ces 4 principes ont pour effet de renforcer l’idée que l’agilité est bien <strong>un savoir-être</strong> plutôt qu’un savoir-faire.</p>\n\n<p>Le Modern Agile synthétise des valeurs de bon sens (collaboration, solidarité, bienveillance, confiance, amélioration continue, expérimentation, etc.) issues du Manifeste agile avec un fort impact sur l’écosystème : bien-être, efficacité, forte valeur ajoutée, fidélisation.</p>\n\n<p class=\"blockquote-footer\">Article de <a href=\"https://blog.octo.com/author/reine-ramade-rera/\">Reine Ramade</a> pour le <a href=\"https://blog.octo.com/\">Octo Talks !</a> publié le 25/10/2019 </p>\n', 1, '2019-11-11 20:07:20', '2019-11-11 23:13:03', '2019-11-13 20:07:20', '2019-11-15 22:25:38', 'ndiixplcinw9jfajf8mh.jpg', 0, 66, 1);

-- --------------------------------------------------------

--
-- Structure de la table `post_category`
--

CREATE TABLE `post_category` (
  `post_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `post_category`
--

INSERT INTO `post_category` (`post_id`, `category_id`) VALUES
(1, 45),
(2, 46),
(3, 4),
(4, 3),
(4, 6),
(19, 3),
(24, 42),
(100, 43),
(101, 43),
(102, 43),
(103, 43),
(105, 43),
(106, 43),
(109, 3),
(110, 3),
(110, 5),
(111, 3),
(111, 6),
(112, 3),
(114, 47);

-- --------------------------------------------------------

--
-- Structure de la table `post_tag`
--

CREATE TABLE `post_tag` (
  `post_id` int(10) NOT NULL,
  `tag_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `post_tag`
--

INSERT INTO `post_tag` (`post_id`, `tag_id`) VALUES
(3, 23),
(3, 24),
(3, 31),
(4, 20),
(4, 25),
(4, 26),
(4, 27),
(4, 28),
(19, 1),
(19, 7),
(19, 18),
(19, 19),
(19, 20),
(109, 2),
(109, 7),
(109, 10),
(109, 14),
(109, 20),
(110, 13),
(110, 15),
(110, 16),
(110, 31),
(111, 17),
(111, 21),
(112, 20),
(112, 32),
(114, 16),
(114, 29),
(114, 30),
(114, 31);

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`role_id`, `role_name`) VALUES
(1, 'Administrateur'),
(2, 'Rédacteur'),
(3, 'Commentateur');

-- --------------------------------------------------------

--
-- Structure de la table `tags`
--

CREATE TABLE `tags` (
  `tag_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL,
  `date_create` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tags`
--

INSERT INTO `tags` (`tag_id`, `name`, `user_id`, `date_create`) VALUES
(1, 'HTML', 1, '2019-08-12 15:23:03'),
(2, 'Responsive', 1, '2019-08-12 15:23:03'),
(7, 'CSS', 1, '2019-08-18 16:20:43'),
(9, 'Jquery', 1, '2019-09-14 22:33:06'),
(10, 'Bug', 1, '2019-09-14 22:34:57'),
(11, 'Code Review', 1, '2019-09-14 22:35:05'),
(13, 'Outils', 1, '2019-10-06 14:44:14'),
(14, 'Image', 1, '2019-11-01 15:56:58'),
(15, 'Planning de Gantt', 1, '2019-11-03 19:41:17'),
(16, 'Productivité', 1, '2019-11-03 19:41:38'),
(17, 'Salaire', 1, '2019-11-06 09:15:13'),
(18, 'email', 1, '2019-11-06 09:17:02'),
(19, 'Compatibilité', 1, '2019-11-06 09:17:18'),
(20, 'Code', 1, '2019-11-06 09:17:24'),
(21, 'embauche', 1, '2019-11-06 09:19:41'),
(23, 'Battle Dev', 1, '2019-11-10 22:41:45'),
(24, 'Marque Entreprise', 1, '2019-11-10 22:41:54'),
(25, 'Fraude', 1, '2019-11-10 23:05:48'),
(26, 'Prison', 1, '2019-11-10 23:06:01'),
(27, 'Bombe logique', 1, '2019-11-10 23:09:32'),
(28, 'Hacker', 1, '2019-11-10 23:09:41'),
(29, 'Agile', 1, '2019-11-11 23:13:17'),
(30, 'Scrum', 1, '2019-11-11 23:13:21'),
(31, 'Equipe', 1, '2019-11-11 23:13:30'),
(32, 'PHP', 1, '2019-11-13 20:07:37');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `date_create` datetime NOT NULL DEFAULT current_timestamp(),
  `date_update` datetime DEFAULT current_timestamp(),
  `image` varchar(255) DEFAULT NULL,
  `geo_consent` tinyint(1) NOT NULL DEFAULT 0,
  `cookie` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role_id`, `active`, `date_create`, `date_update`, `image`, `geo_consent`, `cookie`) VALUES
(1, 'Armelle', 'Braud', 'armelle.braud@free.fr', '$2y$10$zwK9YWQiPD0ILMt/2dPnB.5hP9rw.Rdt6VWUckJG9F4v1K0boVjfi', 1, 1, '2019-08-07 13:26:06', '2019-09-09 11:47:44', 'ui-ab.jpg', 1, 'YnZ3bytkNUw5enFxNU1DWklvQ2h0bmxJRHQ0R2lDdWxkUkZPZmdCRTMyK3BRZ0EzUUpuUVNNdjc1SUVWU3ErRg=='),
(2, 'Lara', 'Leuse', 'lraleuse@free.fr', '$2y$10$oDVChCFyW2/.Dpo4Oh.FROi3ylqfJthcE5A6hBHuX47ALzHVUtpLu', 3, 1, '2019-08-18 13:26:06', '2019-09-08 16:52:34', 'ui-divya.jpg', 0, '0'),
(4, 'Oussama', 'Lairbon', 'olairbon@free.fr', '$2y$10$auaeG4KmV3wnEDjqYDe3LuCRJJHR5r1t2OYoNp6yHLMUKY9znp20G', 1, 1, '2019-08-28 18:18:33', '2019-09-08 16:52:34', 'avatar-1.jpg', 1, 'aUVYekRZaWpRNlhIUHVMeFJzZCtKNk5FejYwNXphN2lpd2diblhWY2xvQ0cxZC9OZzI0cGtFaHBmUjgwQ0tUV1J6ZE1QdFk4WkJqUlR5QldIa3Fvc3c9PQ=='),
(5, 'Annie', 'Versaire', 'aversaire@test.fr', '$2y$10$W4KYS.nQ2Iz71H6hnBUd0OYyeqcWEbz6oUcaLKeK4eJeXuDg4tCRO', 2, 1, '2019-09-04 19:26:48', '2019-09-08 16:52:34', 'Femme.png', 0, ''),
(9, 'Alain', 'Dissoir', 'adissoir@test.fr', '$2y$10$6M1sbJmztnWyM/qMbG2tdOnNOz/V6I7BDSFrtb6yRSGM6tJwm9eK.', 3, 1, '2019-07-16 16:36:17', '2019-09-08 16:52:34', 'xagfcbyj5irlejp207b6.jpg', 0, NULL),
(10, 'Harry', 'Covert', 'contact@blog.oc-armellebraud.fr', '$2y$10$4tQo.g8pjEMtIPNTAfic/uQ2EtXkAEyGiEIpV4vdEPlo.wlTdReV.', 2, 1, '2019-09-09 14:51:45', '2019-09-09 15:20:19', 'ui-sherman.jpg', 0, NULL),
(41, 'Elie', 'Coptère', 'ecoptere@test.fr', '$2y$10$rM3YcXWaKx5Z7TkGObRl5.rvU8CztJbD8FKaL0SSZcuchKB5zUz9C', 3, 1, '2019-11-13 20:59:14', '2019-11-13 20:59:14', 'uszv9eiptmhkp4ajl3vi.jpg', 0, NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `categorie_parent` (`category_id_parent`),
  ADD KEY `category_user` (`user_id`);

--
-- Index pour la table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `comments_posts` (`post_id`),
  ADD KEY `comments_users` (`user_id`),
  ADD KEY `status_comments` (`status_id`);

--
-- Index pour la table `comment_status`
--
ALTER TABLE `comment_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`message_id`);

--
-- Index pour la table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`post_id`,`category_id`),
  ADD KEY `category_constraint` (`category_id`);

--
-- Index pour la table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`post_id`,`tag_id`),
  ADD KEY `categorie_tag` (`tag_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Index pour la table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`tag_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email_user` (`email`),
  ADD KEY `role_user` (`role_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT pour la table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `comment_status`
--
ALTER TABLE `comment_status`
  MODIFY `status_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `message_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=791;

--
-- AUTO_INCREMENT pour la table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `tags`
--
ALTER TABLE `tags`
  MODIFY `tag_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `category_parent` FOREIGN KEY (`category_id_parent`) REFERENCES `categories` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_posts` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `status_comments` FOREIGN KEY (`status_id`) REFERENCES `comment_status` (`status_id`);

--
-- Contraintes pour la table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `post_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `post_category`
--
ALTER TABLE `post_category`
  ADD CONSTRAINT `categories_post_category` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  ADD CONSTRAINT `post_post_category` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_post_tag` FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_post_tag` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `role_user` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
