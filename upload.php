<?php
/**
 * This file allow to upload image from ckEditor (admin post edit page).
 * Must be placed in root directory
 */

include_once ('core/config/config.php');

// CKEditorParameters
$type = $_GET['type'];
$CKEditor = $_GET['CKEditor'];
$funcNum = $_GET['CKEditorFuncNum'];

// Image upload
if ($type == 'image') {

    $return = File::CkUploadImage($_FILES, BLOG_IMG_PATH . "corps-articles/", "upload");

    if (empty($return['success']) || !$return['success']) {
        echo $return["message"];
        exit();
    }

    if (isset($_SERVER['HTTPS'])) {
        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
    } else {
        $protocol = 'http';
    }

    $url = $protocol."://".$_SERVER['SERVER_NAME'] . SERVER_BASE . BLOG_IMG_PATH ."corps-articles/" . $return['filename'];

    echo '<script>window.parent.CKEDITOR.tools.callFunction(' . $funcNum . ', "' . $url . '", "' . $return['message'] . '")</script>';
    exit;

} else {
    echo "Le fichier n'est pas image !";
}
