
/*
* =======================================================
*  DATATABLES
* =======================================================
*/

function dataTablesInit(idTable) {
    let aCols = [0,7];
    switch(idTable){
        case 'table-info-posts':
            aCols = [0,7];
            break;
        case 'table-info-cats':
            aCols = [0,1,7];
            break;
        case 'table-info-tags':
            aCols = [0,1,5];
            break;
        case 'table-info-comments':
            aCols = [0,1,7];
            break;
        case 'table-info-users':
            aCols = [0,1,7];
            break;
        default:
            aCols = [0,7];
            break;
    }
    $.extend(true, $.fn.dataTable.defaults, {
        responsive: {
            details: {
                renderer: function ( api, rowIdx, columns ) {
                    const data = $.map( columns, function ( col, i ) {
                        if($.inArray(i,aCols) < 0) {
                            return col.hidden ?
                                '<tr class="responsive-infos" data-dt-row="' + col.rowIndex + '" data-dt-column="' + col.columnIndex + '">' +
                                '<td class="responsive-infos"><b>' +
                                $.trim(stripHtml(col.title,'')) + '</b> : ' +
                                $.trim(stripHtml(col.data,',')) +
                                '</td>' +
                                '</tr>'
                                :
                                '';
                        }
                    } ).join('');

                    return data ?
                        $('<table/>').append( data ) :
                        false;
                }
            }
        },
        oLanguage: {
            sUrl: assetPath + "lib/datatables/lang/fr_FR.json"
        },
        iDisplayLength: 10,
        aLengthMenu: [5, 10, 25, 50, 100],
        orderCellsTop: true,
        fixedHeader: true,
        filtering:false,
        dom: "<'row'<'col-12'i>>t<'row'<'col-md-6 col-sm-12 text-xs-center'l><'col-md-6 col-sm-12'p>>"
    });
}

//CATEGORIES
function initCatsTable(){

    $('#table-info-cats').DataTable({
        retrieve: true,
        responsive:true,
        "columns": [
            { "width": "5%" },
            { "width": "5%" },
            { "width": "40%" },
            null,
            null,
            null,
            null,
            null,
            { "width": "10%" },
        ],
        columnDefs: [
            {
                "targets": [3],
                "visible": true,
                "searchable": false
            },
            {
                "targets": [6],
                "width": "15%",
                "type": "date-euro"
            }
        ],
        aaSorting: [
            [3, 'asc']
        ],
        rowGroup: {
            startRender: function (rows, group) {
                const catName = group.length ? "Catégorie parente : " + group : 'Catégories principales';
                return catName + ' (' + rows.count() + ' sous-rubriques)';
            },
            dataSrc: 3
        }
    });
}

//POSTS
function initPostsTable(){

    //Search input
    addInputToTable('#table-info-posts',[3,6],[1,2]);

    //Table
    const oTablePosts = $('#table-info-posts').DataTable({

        "columns": [
            null,
            { "width": "6%" },
            { "width": "6%" },
            null,
            null,
            null,
            null,
            null,
            { "width": "10%" },
            { "width": "5%" }
        ],
        columnDefs: [
            {
                "className": 'control',
                "orderable": false,
                "targets": [ 0 ]
            },
            {
                "targets": [ 1 ],
                "type": "num"
            },
            {
                "targets": [ 2 ],
                "type": "num"
            },
            {
                "targets": [ 6 ],
                "type": "date-euro"
            },
            {
                "targets": [ 9 ],
                "visible": false
            }
        ],
        aaSorting: [
            [6, 'desc']
        ],
        initComplete: function () {
            this.api().columns([4,5,7]).every(function () {
                const column = this;
                const select = $('<br /><select class="form-control"><option value="">Tous</option></select>')
                    .appendTo($(column.header()))
                    .on('change', function () {
                        const val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val)
                            .draw();
                    });
                $(select).click(function (e) {
                    e.stopPropagation();
                });

                const aData = column.data();
                const aWords = [];
                for (let i = 0; i < aData.length ;i++){
                    const aCleanedWords = aData[i].split('<br>');
                    for (let h = 0; h < aCleanedWords.length ;h++){
                        const word = $.trim(aCleanedWords[h]);
                        if(word.length > 0){
                            aWords.push(word);
                        }
                    }
                }
                $.each(sort_unique(aWords), function(d, value){
                    const val = $('<div/>').html(value).text();
                    select.append('<option value="' + val + '">' + val + '</option>');
                });
            });

        }
    });

    //Filter
    addFilterToTable('#table-info-posts',oTablePosts,[3,6]);

    return oTablePosts;
}

//TAGS
function initTagsTable(){

    //Search input
    addInputToTable('#table-info-tags',[2,4],[]);

    //Table
    const oTableTags = $('#table-info-tags').DataTable({
        aaSorting: [
            [2, 'asc']
        ],
        "columns": [
            { "width": "5%" },
            { "width": "5%" },
            null,
            { "width": "20%" },
            { "width": "20%" },
            { "width": "15%" }

        ],
        columnDefs: [
            {
                "targets": [1],
                "type": "num"
            },
            {
                "targets": [4],
                "type": "date-euro"
            }
        ],
        initComplete: function () {
            this.api().columns([3]).every(function () {
                const column = this;
                const select = $('<br /><select class="form-control"><option value="">Tous</option></select>')
                    .appendTo($(column.header()))
                    .on('change', function () {
                        const val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                $(select).click(function (e) {
                    e.stopPropagation();
                });
                column.data().unique().sort().each(function (d) {
                    const val = $('<div/>').html(d).text();
                    select.append('<option value="' + val + '">' + val + '</option>');
                });
            });

        }
    });

    //Filter
    addFilterToTable('#table-info-tags',oTableTags,[2,4]);
}

//USERS
function initUsersTable() {

    //Search input
    addInputToTable('#table-info-users',[2,4,5],[]);

    //Table
    const oTableUsers = $('#table-info-users').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "5%" },
            { "width": "25%" },
            { "width": "20%" },
            { "width": "15%" },
            null,
            { "width": "10%" },
            { "width": "10%" },
        ],
        "columnDefs": [
            { "targets": [ 0 ], "type": "num" },
            { "type": "date-euro", targets: 5 }
        ],
        "order": [[ 5, "desc" ]],
        initComplete: function () {
            this.api().columns([3,6]).every(function () {
                const column = this;
                const select = $('<br /><select class="form-control"><option value="">Tous</option></select>')
                    .appendTo($(column.header()))
                    .on('change', function () {
                        const val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                $(select).click(function (e) {
                    e.stopPropagation();
                });
                column.data().unique().sort().each(function (d) {
                    const val = $('<div/>').html(d).text();
                    select.append('<option value="' + val + '">' + val + '</option>');
                });
            });
        }
    });

    //Filter
    addFilterToTable('#table-info-users',oTableUsers,[2,4,5]);

}

//COMMENTS
function initCommentsTable() {

    //Search input
    addInputToTable('#table-info-comments',[3,4,5],[]);

    //Table
    const oTableComments = $('#table-info-comments').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "5%" },
            { "width": "10%" },
            { "width": "10%" },
            null,
            { "width": "10%" },
            { "width": "10%" },
            { "width": "10%" },
        ],
        "columnDefs": [
            { "targets": [ 0 ], "type": "num" },
            { "type": "date-euro", targets: 5 }
        ],
        "order": [[ 5, "desc" ]],
        initComplete: function () {
            this.api().columns([2, 6]).every(function () {
                const column = this;
                const select = $('<br /><select class="form-control"><option value="">Tous</option></select>')
                    .appendTo($(column.header()))
                    .on('change', function () {
                        const val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                $(select).click(function (e) {
                    e.stopPropagation();
                });
                column.data().unique().sort().each(function (d) {
                    const val = $('<div/>').html(d).text();
                    select.append('<option value="' + val + '">' + val + '</option>');
                });
            });

        }
    });

    //Filter
    addFilterToTable('#table-info-comments',oTableComments,[3,4,5]);

}


/*
* =======================================================
*  ACTIONS TABLE
* =======================================================
*/
//Search input
function addInputToTable(idTable,aColsActives,aColsInactives){
    $(idTable + ' thead th').each(function(index) {
        if($.inArray(index,aColsActives) >= 0){
            //var title =  $(idTable + ' thead th').eq($(this).index()).text();
            $(this).append('<br /><input class="form-control" type="text" placeholder="Rechercher" />' );
        }

        if(aColsInactives.length){
            if($.inArray(index,aColsInactives) >= 0){
                $(this).append('<br /><input class="form-control" type="text" disabled="disabled" readonly="readonly" />' );
            }
        }
    });
}

//Filter
function addFilterToTable(idTable, oTable, aCols){

    $(idTable + ' thead tr th').each( function (i) {
        if($.inArray(i,aCols) >= 0) {
            $('input', this).on('keyup change', function () {
                if (oTable.column(i).search() !== this.value) {
                    oTable
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });

            $('input', oTable.column(i).header()).on('click', function(e) {
                e.stopPropagation();
            });
        }
    } );
}
//Prepare confirmation popin for change user status
function hydratePopinStatusUser(el)
{

    const id = $(el).data('id');
    const status = $(el).data('status');
    const libStatus = status ? "Désactivation" : "Activation";
    const txtStatus = status ? "désactiver" : "activer";

    $("#statusUserModal .modal-title").text(libStatus + " d'un utilisateur");
    $("#statusUserModal form").attr("action",serverPath + "admin/config/users/status");
    $("#statusUserModal").find("#idUser").val(id);
    $("#statusUserModal").find("#txtStatus").text(txtStatus);
}

//Send if to change user status
function changeStatusUser(form) {
    var data = form.serialize();

    $.ajax({
        url: $(form).attr("action"),
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function (response) {
            if (!response.success) {
                $("#statusUserModal .modal-content .modal-footer .confirm").hide();
                $("#statusUserModal .modal-content .modal-body .info").hide();
                $("#statusUserModal .modal-content .modal-body .result-message")
                    .empty()
                    .text(response.message)
                    .show();
            } else {
                $(location).attr('href', serverPath + 'admin/config/users')
            }
        },
        error: function () {
            $("#statusUserModal .modal-content .modal-footer .confirm").hide();
            $("#statusUserModal .modal-content .modal-body .info").hide();
            $("#statusUserModal .modal-content .modal-body .result-message")
                .empty()
                .text(genericalErrorMessage)
                .show();
        }
    });
}

//Prepare confirmation popin for change comment status
function hydratePopinComment(commentId)
{
    $.ajax({
        type : 'post',
        url : serverPath + 'admin/comments/' + commentId + '/edit',
        data :  'id='+ commentId,
        dataType : 'json',
        processData: false,
        contentType: false,
        success : function(data){
            if(!data.success) {
                $('.modal-body').html(genericalErrorMessage);
            } else {

                let oDate = DateTimeObject(data.comment._date_create);
                const optionsDate = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
                let linkUser =  $('.modal-body').find('#link-user-comment').attr('href') + data.comment._user._user_id + '/edit';

                $('.modal-title').text(data.title);
                $('.modal-body').find('#link-user-comment').attr('href', linkUser);
                $('.modal-body').find('#commentId').val(data.comment._comment_id);
                $('.modal-body').find('#postId').val(data.comment._post._post_id);
                $('.modal-body').find('#userFullname').text(data.comment._user._firstname+ " " + data.comment._user._lastname);
                $('.modal-body').find('#commentDate').text(oDate.toLocaleDateString('fr-FR', optionsDate) + " à " + oDate.toLocaleTimeString('fr-FR'));
                $('.modal-body').find('#pseudo').val(data.comment._pseudo);
                $('.modal-body').find('#title').val(data.comment._comment_title);
                $('.modal-body').find('#usercomment').val(data.comment._comment_text);

                let options = "";
                $.each(data.aStatus, function(index, item) {
                    let selected = "";
                    if(item._status_id === data.comment._status._status_id){
                        selected = ' selected="selected"';
                    }
                    options += '<option value="' + item._status_id + '"' + selected + '>' + item._name + '</option>';
                });
                $("#statusComment").html(options);
            }
        }
    });
}