
$(document).ready(function( $ ) {
    // Go to top
    $('.go-top').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('html, body').animate({scrollTop : 0},500);
    });

    // Tooltips
    $('.tooltips').tooltip();

    // Menu
    responsiveView();
    $(window).on('load', function(){
        responsiveView();
    });
    $(window).on('resize', function(){
        responsiveView();
    });

    msgSuccess();
});

/*
* =======================================================
*  LEFTBAR ACCORDEON
* =======================================================
*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'slow',
        showCount: false,
        autoExpand: true,
        classExpand: 'dcjq-current-parent'
    });
});

// sidebar toggle
function responsiveView() {
    const wSize = $(window).width();
    if (wSize <= 1024) {
        $('#sidebar').removeClass('show');
    }

    if (wSize > 1024) {
        $('#sidebar').addClass('show');
    }
}


/*
* =======================================================
*  POST
* =======================================================
*/

function initFormPost(){

    //Datepicker
    $('#datepubli').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        daysOfWeekHighlighted: "6,0",
        autoclose: true,
        todayHighlight: true,
        language: "fr"
    });
    $("#datepubli").datepicker("setDate", $("#datepubli").data('date'));
    $('.open-datetimepicker').click(function(event){
        event.preventDefault();
        e.stopPropagation();
        $('#datepubli').focus();
    });


    //CkEditor
    CKEDITOR.replace('content',{
        filebrowserImageUploadUrl: serverPath + "upload.php?type=image"
    });

    //Tags
    $('#tags').selectivity({
        multiple: true,
        placeholder: 'Cliquez pour sélectionner des tags',
        items: data,
        tokenSeparators: [' '],
        allowClear: true,
        value:aValues
    }).change(function(e){
        $("#tags-ids").val(e.value);
    });

    //FileInput
    initFileUpload($('#image'));
}

function initFileUpload(input){

    input.fileinput({
        browseClass: "btn btn-theme btn-block",
        browseLabel: "Parcourir",
        browseIcon: "<i class=\"far fa-image\"></i>",
        maxFileCount: 1,
        allowedFileTypes: ['image'],
        uploadUrl: uploadPath,
        initialPreview: [ imagePost['url'] ],
        initialPreviewAsData: true,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewShowDelete: true,
        initialPreviewShowUpload: false,
        initialPreviewConfig: [
            {caption : imagePost['namefile'], size : imagePost['size'], downloadUrl: false, showRemove: true}
        ],
        language: 'fr',
        theme: 'fas',
        showCaption: true,
        showPreview: true,
        showRemove: false,
        showUpload: false,
        showCancel: false,
        showClose: false,
        maxImageWidth: sizesImg['maxWidth'],
        maxImageHeight: sizesImg['maxHeight'],
        minImageWidth: sizesImg['minWidthPost'],
        minImageHeight: sizesImg['minHeightPost'],
        showUploadedThumbs: true,
        enableResumableUpload: true,
        autoReplace: true,
        maxFileSize: 4 * 1024,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            },
            showUpload: false,
            showDrag: false,
            showRemove: true
        }
    });


    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $("#img-changed").val(1);
        input.fileinput('clear');
    });

}

/*
* =======================================================
*  USER
* =======================================================
*/

function initFormUser(){

    //Password
    $("#dspChangePass").click(function(event){
        event.preventDefault();
        event.stopPropagation();

        $(".containPass").toggleClass("hidden");
    });

    // Create a new password
    $(".getNewPass").click(function(){
        const field = $(".containPass").closest('div').find('input[rel="gp"]');
        const pass = randString(field);
        $("#password-confirm, #userpassword").val(pass);
    });

    // Auto Select Pass On Focus
    $('input[rel="gp"]').on("click", function () {
        $(this).select();
    });

    //FileInput
    initFileUploadUser($('#image'));

}

function initFileUploadUser(input){

    input.fileinput({
        browseClass: "btn btn-theme btn-block",
        browseLabel: "Parcourir",
        browseIcon: "<i class=\"far fa-image\"></i>",
        maxFileCount: 1,
        allowedFileTypes: ['image'],
        uploadUrl: uploadPath + "images/",
        initialPreview: [ imageProfile['url'] ],
        initialPreviewAsData: true,
        initialPreviewFileType: 'image', // image is the default and can be overridden in config below
        initialPreviewShowDelete: true,
        initialPreviewShowUpload: false,
        initialPreviewConfig: [
            {caption : imageProfile['namefile'], size : imageProfile['size'], downloadUrl: false, showRemove: true}
        ],
        language: 'fr',
        theme: 'fas',
        showCaption: true,
        showPreview: true,
        showRemove: false,
        showUpload: false,
        showCancel: false,
        showClose: false,
        showUploadedThumbs: true,
        maxImageWidth: sizesImg['maxWidth'],
        maxImageHeight: sizesImg['maxHeight'],
        minImageWidth: sizesImg['minWidthUser'],
        minImageHeight: sizesImg['minHeightUser'],
        enableResumableUpload: true,
        autoReplace: true,
        maxFileSize: 4 * 1024,
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            },
            showUpload: false,
            showDrag: false,
            showRemove: true
        }
    });

    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(event){
        event.preventDefault();
        event.stopPropagation();
        $("#img-changed").val(1);
        input.fileinput('clear');
    });
}

/*
* =======================================================
*  HOME MADE CHECKBOX STYLE
* =======================================================
*/
function stylizeCheckbox(){

    $('.button-checkbox').each(function () {

        // Settings
        let $widget = $(this);
        let $button = $widget.find('button');
        let $checkbox = $widget.find('input:checkbox');
        let text = $button.find('.btn-label').text();
        let textOn = (typeof $button.data('text-on') === 'undefined') ? text : $button.data('text-on');
        let textOff = (typeof $button.data('text-off') === 'undefined') ? text : $button.data('text-off');
        let color = $button.data('color');
        let colorOn = (typeof $button.data('color-on') === 'undefined') ? color : $button.data('color-on');
        let colorOff = (typeof $button.data('color-off') === 'undefined') ? color : $button.data('color-off');
        let settings = {
            on: {
                icon: 'far fa-check-square'
            },
            off: {
                icon: 'far fa-square'
            }
        };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default btn-' + colorOff)
                    .addClass('btn-' + colorOn)
                    .find(".btn-label").empty().text(textOn);
            }
            else {
                $button
                    .removeClass('btn-default btn-' + colorOn)
                    .addClass('btn-' + colorOff)
                    .find(".btn-label").empty().text(textOff);
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length === 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
}

/*
* =======================================================
*  MISCELLANEOUS
* =======================================================
*/

//Generic call Ajax for action
function updateElement(form){
    $.ajax({
        type : 'POST',
        url : $(form).attr("action"),
        data :  $(form).serialize(),
        dataType : 'json',
        success : function(data){
            if(!data.success) {
                var message = data.message.length ? data.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                $(location).attr('href', data.redirect + "?s=1");
            }
        },
        error : function(){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

//Generic call Ajax for action with file
function saveWithFile(form, oFormData){

    const oContMsg =  $(".result-message");

    $.ajax({
        url : $(form).attr('action'),
        type : 'POST',
        dataType : 'json',
        processData: false,
        contentType: false,
        data: oFormData,
        success : function(response){
            if(!response.success){
                let message = response.message.length ? response.message : genericalErrorMessage;
                oContMsg.addClass("alert-danger").empty().text(message).show();
            } else {
                $(location).attr('href', response.redirect + "?s=1");
            }
        },
        error : function(){
            oContMsg.addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}


// Generate a password string
function randString(id){
    const dataSet = $(id).attr('data-character-set').split(',');
    let possible = '';
    if($.inArray('a-z', dataSet) >= 0){
        possible += 'abcdefghijklmnopqrstuvwxyz';
    }
    if($.inArray('A-Z', dataSet) >= 0){
        possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if($.inArray('0-9', dataSet) >= 0){
        possible += '0123456789';
    }
    if($.inArray('#', dataSet) >= 0){
        possible += '![]{}()%&*$#^<>~@|';
    }
    let text = '';
    for(var i=0; i < $(id).attr('data-size'); i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function DateTimeObject(dateString){
    return new Date(Date.parse(dateString.replace('-','/','g')));
}

function isJson(item) {
    item = typeof item !== "string"
        ? JSON.stringify(item)
        : item;

    try {
        item = JSON.parse(item);
    } catch (e) {
        return "JSON invalide : " + e ;
    }

    if (typeof item === "object" && item !== null) {
        return true;
    }

    return "JSON invalide";
}

/*
* =======================================================
*  SNIPPETS
* =======================================================
*/
jQuery.fn.clickToggle = function(a, b) {
    return this.on("click", function(ev) { [b, a][this.$_io ^= 1].call(this, ev) })
};
