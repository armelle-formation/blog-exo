$(document).ready(function( $ ) {

    initCalendar();
    initGraph();




    //Bar chart
    if ($(".custom-bar-chart")) {
        $(".bar").each(function() {
            let i = $(this).find(".value").html();
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: i
            }, 500)
        })
    }

});

function initGraph(){
    const lineOptions = {
        elements:{
            line: {
                fill:false
            }
        },
        layout: {
            padding: {
                bottom: 10,
                left: 10,
                right: 10,
                top: 10
            }
        },
        responsive: true,
        tooltips:{
            enabled:true
        },
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                display:false

            }],
            xAxes: [{
                display:false
            }],

        }
    };

    //Polar : posts by category
    const jsonPosts  = $("#stats-posts").data('data');

    const labels = jsonPosts.map(function(e) {
        return e.category;
    });
    const dataPosts = jsonPosts.map(function(e) {
        return parseInt(e.value);
    });
    new Chart(document.getElementById("stats-posts"), {
        type: 'polarArea',
        data: {
            datasets: [{
                data: dataPosts,
                backgroundColor: [
                    "rgba(210, 190, 236,0.75)",
                    "rgba(233, 190, 236,0.75)",
                    "rgba(236, 190, 216,0.75)",
                    "rgba(236, 190, 193,0.75)",
                    "rgba(236, 210, 190,0.75)",
                    "rgba(236, 233, 190,0.75)",
                    "rgba(158, 143, 177,0.75)",
                    "rgba(79, 71, 88,0.75)",
                    "rgba(53, 48, 59,0.75)"]
            }],
            labels:labels
        },
        options: {
            scaleStartValue: 0,
            legend: {
                display: false
            },
            scale: {
                ticks: {
                    showLabelBackdrop: true
                }
            },
            tooltips: {
                enabled: true
            }
        }
    });

    //Line Active commentators by months
    const jsonUsers  = $("#active-comments").data('data');
    const labelsUsers = jsonUsers.map(function(e) {
        return e.date;
    });
    const data = jsonUsers.map(function(e) {
        return e.value;
    });
    new Chart(document.getElementById("active-comments"), {
        type: 'line',
        data: {
            datasets: [{
                data: data,
                borderWidth:2,
                borderColor: "#d2beec",
                pointBackgroundColor: "#d2beec",
                pointBorderColor: "#d2beec",
                pointRadius: 5,
                pointHoverRadius:3
            }],
            labels:labelsUsers
        },
        options:lineOptions
    });

    //Line comments by months
    const jsonComments  = $("#comments-by-months").data('data');
    const labelsComments = jsonComments.map(function(e) {
        return e.date;
    });
    const dataComments = jsonComments.map(function(e) {
        return e.value;
    });
    new Chart(document.getElementById("comments-by-months"), {
        type: 'line',
        data: {
            datasets: [{
                data: dataComments,
                borderWidth:2,
                borderColor: "#fff",
                pointBackgroundColor: "#fff",
                pointBorderColor: "#fff",
                pointRadius: 5,
                pointHoverRadius:3
            }],
            labels:labelsComments
        },
        options:lineOptions
    });
}

function initCalendar(){

    $("#my-calendar").zabuto_calendar({
        language: "fr",
        legend: [],
        today: true
    });

    $('.panel .tools .fa-chevron-down').click(function() {
        let el = $(this).parents(".panel").children(".panel-body");
        if ($(this).hasClass("fa-chevron-down")) {
            $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up");
            el.slideUp(200);
        } else {
            $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down");
            el.slideDown(200);
        }
    });

    $('.panel .tools .fa-times').click(function() {
        $(this).parents(".panel").parent().remove();
    });

}