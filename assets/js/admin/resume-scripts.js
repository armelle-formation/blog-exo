function saveResume(form){

    // JSON
    const oContMsg =  $('.result-message');
    const dataResume = $(form).find('#json-input').val();

    if(dataResume.length > 30){

        //Valid JSON ?
        const invalidJson = isJson(dataResume);
        if(invalidJson.length){
            oContMsg.addClass("alert-danger").empty().text(invalidJson).show();
            return false;
        } else {
            const oFormData = new FormData($(form)[0]);
            oFormData.append('resume',  dataResume);
            saveWithFile(form,oFormData);

        }
    }else {
        let message = 'Le json ne peut être vide';
        oContMsg.addClass("alert-danger").empty().text(message).show();
        return false;
    }
}

function visualizeJson(){
    const dataResume = $("#json-input").val();
    if(dataResume.length > 30){
        //CheckJSON
        const invalidJson = isJson(dataResume);
        if(invalidJson.length){
            $("#error-json").text(invalidJson);
        } else {
            $(".error").empty();
            const options = {
                collapsed: $('#checkbox-collapsed').is(':checked'),
                withQuotes: true

            };
            $('#json-renderer').show();
            $('#json-renderer').jsonBrowse(eval('(' + dataResume + ')'), options);
        }
    }else {
        $("#error-json").text("Le JSON ne peut être vide");
    }
}
function initFileZone() {
    $("#file").fileinput({
        browseClass: "btn btn-theme btn-block",
        browseLabel: "Parcourir",
        browseIcon: "<i class=\"far fa-image\"></i>",
        maxFileCount: 1,
        uploadUrl: cvPath,
        initialPreview: [cvFile['url']],
        initialPreviewAsData: true,
        initialPreviewShowDelete: true,
        initialPreviewShowUpload: false,
        initialPreviewConfig: [
            {
                type: "pdf",
                size: cvFile['filesize'],
                filename: cvNameFile,
                caption: "CV Armelle Braud",
                url: cvFile['url'],
                downloadUrl: false,
                showRemove: true,
                key: 14
            },
        ],
        preferIconicPreview: true,
        previewFileIconSettings: {
            'pdf': '<i class="fas fa-file-pdf text-danger"></i>'
        },
        language: 'fr',
        theme: 'fas',
        showCaption: true,
        showPreview: true,
        showRemove: false,
        showUpload: false,
        showCancel: false,
        showClose: false,
        showUploadedThumbs: true,
        enableResumableUpload: true,
        autoReplace: true,
        maxFileSize: 4 * 1024,
        fileActionSettings: {
            showZoom: function (config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            },
            showUpload: false,
            showDrag: false,
            showRemove: true
        }
    }).on('fileloaded', function (event, file) {
        $("#file-name").val(file.name);
    });

    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(event){
        event.preventDefault();
        $("#file-changed").val(1);
        $('#file').fileinput('clear');
        $('#file-name').val("");
    });

}