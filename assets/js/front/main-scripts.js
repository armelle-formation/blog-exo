$(document).ready(function() {

    //Display a modal popin to encourage user to update browser
    checkBrowser();

});

// ========================================================================= //
//  SMOOTH
// ========================================================================= //
function scrollTo(selector) {
    let offset = $(selector).offset();
    let main = $('#page');

    $("html, body").animate({
        scrollTop: offset.top - (main.offset().top - main.scrollTop()) - 100
    }, "slow");
    return false;
}

// ========================================================================= //
// PITCH
// ========================================================================= //
function initTypeJS(){

    var typed = $(".typed");
    $(function() {
        typed.typed({
            strings: ["Armelle Braud", "Web Developpeuse", "celle qu'il vous faut !"],
            typeSpeed: 75,
            loop: true,
        });
    });
}

// ========================================================================= //
//  Resume navigation
// ========================================================================= //
function navResume(){

    $('[data-spy="scroll"]').on('activate.bs.scrollspy', function(event,el) {
        if (el.relatedTarget === '#remove') {
            $(".resume-navbar").hide();
            $("#main-nav").slideUp(700);
        } else {
            $("#main-nav").slideDown(700);
            $(".resume-navbar").show();
        }
    })

    $('#navbarSupportedContent .link-scroll').click(function(event) {
        event.preventDefault();
        event.stopPropagation();
        scrollTo($(this));
        window.location.hash = $(this).attr('href')
    });
}

// ========================================================================= //
//  Owl Carousel Services
// ========================================================================= //
function initCarousel(){
$('.services-carousel').owlCarousel({
    autoplay: true,
    loop: true,
    margin: 20,
    dots: true,
    nav: false,
    responsiveClass: true,
    responsive: { 0: { items: 1 }, 360: { items: 2 }, 768: { items: 2 }, 900: { items: 4 } }
});
}


// ---------------------------------------------- //
// Navbar
// ---------------------------------------------- //
function initNavbar(){
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 64 ) {
            $("#main-nav").slideDown(700);
        } else {
            $("#main-nav").slideUp(700);
        }

        if (scroll > 857 ) {
            $(".resume-navbar").show();
        } else {
            $(".resume-navbar").hide();
        }
    });

    //Smooth scroll
    $('.link-scroll, .continue').on('click', function (e) {
        const anchor = $(this).attr("href");
        if ( anchor.length > 0) {
            scrollTo("#" + anchor.split("#")[1])
        }
        e.preventDefault();
        e.stopPropagation();
    });

    activeLinksNav();
}

//Apply class active on navbars
function activeLinksNav(){
    $('.nav-link').on('click', function (e) {
        $('.nav-link').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        $('.navbar-collapse').collapse('hide');
    });
}
// ---------------------------------------------- //
// Contact
// ---------------------------------------------- //
function sendMessageContact(form){

    $.ajax({
        type : 'POST',
        url : $(form).attr('action'),
        data :  $(form).serialize(),
        dataType : 'json',
        success : function(data){
            if(!data.success) {
                let message = data.message.length ? data.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                $(location).attr('href', data.redirect + "?s=1#contact");
            }
        },
        error : function(){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}