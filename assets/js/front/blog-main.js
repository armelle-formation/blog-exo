$(document).ready(function () {

    //Display a modal popin to encourage user to update browser
    checkBrowser();

    //Submit form comment
    $("#login-submit").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        commentLogin($("#comment-login"));
        return false;
    });

    $('#newAccountModal').on('show.bs.modal', function (e) {
        initFileUpload($('#image'));
    });

    //Submit form new account
    $('#new-account-submit').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        saveUser($('#new-account-form'),'fr');
        return false;
    });

    //Init error message modal
    $('.modal').on('show.bs.modal', function (e) {
        $('.result-message').hide();
    });

    //Code Highlight
    hljs.initHighlightingOnLoad();
});

function commentLogin(form){
    var data = $(form).serialize();

    $.ajax({
        url : $(form).attr('action'),
        type : 'POST',
        dataType : 'json',
        data: data,
        success : function(response){
            console.log(response);
            if(!response.success){
                var message = response.message.length ? response.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                location.reload()
            }
        },
        error : function(e, status,response){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

//Delete comment
function changeStatusComment(form){

    $.ajax({
        type : 'POST',
        url : $(form).attr('action'),
        data :  $(form).serialize(),
        dataType : 'json',
        success : function(data){
            if(!data.success) {
                var message = data.message.length ? data.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                $('#hideComModal').modal('toggle');
                location.reload();
            }
        },
        error : function(){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

function initFileUpload(input){

    //File Upload
    input.fileinput({
        uploadUrl: uploadPath + "images/",
        maxFileCount: 1,
        language: 'fr',
        theme: 'fas',
        browseClass: "btn btn-theme btn-block",
        browseLabel: "Parcourir",
        initialCaption: "Sélectionnez l'image",
        browseIcon: "<i class=\"far fa-image\"></i>",
        allowedFileTypes: ['image'],
        showRemove: false,
        showUpload: false,
        autoReplace: true,
        maxFileSize: 4 * 1024,
        maxImageWidth: sizesImg['maxWidth'],
        maxImageHeight: sizesImg['maxHeight'],
        minImageWidth: sizesImg['minWidthUser'],
        minImageHeight: sizesImg['minHeightUser'],
        enableResumableUpload: true,
        fileActionSettings: {
            showUpload: false,
            showZoom: false
        }

    });

    //Clear fileinput after a remove
    $(".kv-file-remove").click(function(event){
        event.preventDefault();
        e.stopPropagation();
        input.fileinput('clear');
    });
}

