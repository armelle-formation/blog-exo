$(document).ready(function(){

    $('.modal').on('show.bs.modal', function(){
        if (!$(this).data('template')) {
            $(this).data('template', $(this).html())
        } else {
            $(this).html($(this).data('template'))
        }
        $('.result-modal-message').removeClass('alert-success alert-warning alert-danger').empty().hide();
    }) ;

    $("#forgotPassBtn").click(function () {
        hydratePopin($(this),'form-password','org','org');
    });

    $("#request-reinit-pass-btn").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        reinitPassword($("#form-password"));
        return false;

    })

    $("#login-form-btn").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        doLogin($("#form-login"));
        return false;
    });

    $("#form-reinit-pass-btn").click(function(e){
        e.preventDefault();
        e.stopPropagation();
        reinitPassword($("#form-reset-pass"));
        return false;
    });

    $(".link-scroll").click(function(e){
        $(".modal").modal('hide');
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip({
            html: true,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
        })
    })
})

//Save changes on comments (admin)
function updateComment(form, org = 'fr'){

    $.ajax({
        type : 'POST',
        url : serverPath + 'admin/comments/save',
        data :  $(form).serialize(),
        dataType : 'json',
        success : function(data){
            if(!data.success) {
                let message = data.message.length ? data.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                if(org == 'fr'){
                    $(".result-message").removeClass('alert-success alert-warning alert-danger').addClass("alert-success").empty().text(data.message).show();
                    $(form).trigger("reset");
                } else {
                    $(location).attr('href', data.redirect);
                }
            }
        },
        error : function(){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

//Delete comment
function deleteComment(form){

    $.ajax({
        type : 'POST',
        url : $(form).attr('action'),
        data :  $(form).serialize(),
        dataType : 'json',
        success : function(data){
            if(!data.success) {
                let message = data.message.length ? data.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                $(location).attr('href', data.redirect);
            }
        },
        error : function(){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

//Save User
function saveUser(form, org = 'fr'){

    const oFormData = new FormData($(form)[0]);
    const oContMsg = $(".result-message");

    $.ajax({
        url : $(form).attr('action'),
        type : 'POST',
        dataType : 'json',
        processData: false,
        contentType: false,
        data: oFormData,
        beforeSend: function() {
            if(org === 'fr'){
                $("div.spanner").addClass("show");
                $("div.overlay").addClass("show");
            }
        },
        success : function(response,){
            if(!response.success){
                $("div.spanner").removeClass("show");
                $("div.overlay").removeClass("show");
                let message = response.message.length ? response.message : genericalErrorMessage;
                oContMsg.addClass("alert-danger").empty().text(message).show();
            } else {
                if(org === 'fr'){
                    let message = "Compte crée avec succés";

                    //Affichage du résultat
                    $("div.spanner").removeClass("show");
                    $("div.overlay").removeClass("show");
                    oContMsg.removeClass('alert-success alert-warning alert-danger').addClass("alert-success").empty().text(message).show();

                    setTimeout(function() {
                        $('#newAccountModal').modal('toggle');
                    }, 2000);
                } else {
                    $(location).attr('href', response.redirect + "?s=1");
                }
            }

        },
        error : function(){
            if(org === 'fr'){
                $("div.spanner").removeClass("show");
                $("div.overlay").removeClass("show");
            }
            oContMsg.addClass('alert-success alert-warning alert-danger').text(genericalErrorMessage).show();
        }
    });
}

function reinitPassword(form){
    var org = $(form).find('#org').val();
    var classResult = org == 'modal' ? '.result-modal-message' : '.result-message';

    $.ajax({
        url : $(form).attr('action'),
        type : 'POST',
        data : $(form).serialize(),
        dataType : 'json',
        success : function(response){
            if(response.success){
                $(classResult).removeClass('alert-success alert-warning alert-danger').addClass("alert-success").text(response.message).show();

                if(org === 'modal') {
                    $(form).find("#request-reinit-pass-btn, .main-content").hide();
                    $(form).find(":button").text('Fermer');
                }

            } else {
                $(classResult).addClass('alert-danger').text(response.message).show();
            }
        },
        error : function(r){
            $(".result-message").text(genericalErrorMessage);
        }
    });
}

/*Login*/
function doLogin(form){
    let data = $(form).serialize();

    $.ajax({
        url : $(form).attr('action'),
        type : 'POST',
        dataType : 'json',
        data: data,
        success : function(response){
            if(!response.success){
                let message = response.message.length ? response.message : genericalErrorMessage;
                $(".result-message").addClass("alert-danger").empty().text(message).show();
            } else {
                $(location).attr('href', response.redirect);
            }
        },
        error : function(e, status,response){
            $(".result-message").addClass("alert-warning").text(genericalErrorMessage).show();
        }
    });
}

/*
* =======================================================
*  MISCELLANEOUS
* =======================================================
*/

//Prepare confirmation popin for changes by Ajax call
function hydratePopin(el,idForm,idField, nameData = 'id')
{
    var value = $(el).data(nameData);
    $("#" + idForm).find("#" + idField).val(value);

}

function msgSuccess(message = genericalSuccessMessage){
    const success = new URL(window.location).searchParams.get('s');
    if(parseInt(success) === 1){
        $(".result-message").addClass("alert-success").empty().text(message).show();
    } else {
        $(".result-message").removeClass('alert-success alert-warning alert-danger').empty().hide();
    }
}

function sort_unique(arr) {
    return arr.sort(function(a,b){
        return (a > b) ? 1 : -1;
    }).filter(function(el,i,a) {
        return (i==a.indexOf(el));
    });
}


function stripHtml(html,separator){

    var temporalDivElement = document.createElement("div");
    html = html.replace(/<br\s*\/?>/mg,separator);
    temporalDivElement.innerHTML = html;
    return temporalDivElement.textContent || temporalDivElement.innerText || "";
}