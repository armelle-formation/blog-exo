/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example
    config.toolbarCanCollapse = true;
    config.removePlugins = "";
    config.extraPlugins = 'autogrow,youtube,codesnippet';
    config.youtube_width = 560;
    config.youtube_height  = 315;
    config.youtube_related = false;
    config.youtube_responsive = true;
    config.youtube_privacy = true;
    config.filebrowserUploadMethod = 'form'; // Added for file browser
    config.height = 650;
    config.autoGrow_minHeight = 400;
    config.autoGrow_maxHeight = 650;
    config.autoGrow_bottomSpace = 50;
    config.bodyClass = 'post-body',
    config.toolbarGroups = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'undo', groups: [ 'undo' ] },
        { name: 'clipboard', groups: [ 'clipboard' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'insert', groups: [ 'insert' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'colors', groups: [ 'colors' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'bidi', 'paragraph', 'align' ] },
        { name: 'paragraph', groups: [ 'blocks' ] },
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] }
    ];
    config.removeButtons = 'Save,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,BGColor,BidiLtr,BidiRtl,Language,Flash,Iframe,Font,FontSize,About,Print,Templates';
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6';
    config.removePlugins = 'resize';
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.contentsCss = [ assetPath + 'lib/ckeditor-4.13.0/contents.css', assetPath + 'lib/ckeditor-4.13.0/ck-admin-styles.css' ],
    config.stylesSet = 'default';
    config.allowedContent = true;
    config.extraAllowedContent = 'div(*);iframe"*"(*)[*]{*};';
    config.removePlugins = 'image';
};

CKEDITOR.stylesSet.add( 'default', [
    // Block Styles
    { name: 'Chapo', element: 'p', attributes: { class:"lead" } },
    { name: 'Auteur', element: 'p', attributes: { class:"blockquote-footer" } },

    // Blockquote
    { name: 'Citation', element: 'blockquote', attributes: { 'class': 'blockquote-text'} },

    // Inline Styles
    { name: 'Gras',  element: 'span', attributes: { class:"text-bold" } },
    { name: 'Petit',  element: 'span', attributes: { class:"text-small" } },
    { name: 'Legende',  element: 'p', attributes: { class:"legend" } },

    // Object Styles
    {
        name: 'Image on Left',
        element: 'img',
        attributes: {
            style: 'padding: 5px; margin-right: 5px',
            border: '2',
            align: 'left'
        }
    }
]);
