<?php

class BlogController extends AbstractController
{
    /**
     * Display the home page of Blog
     * @param array $get
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Twig\Error\SyntaxError
     * @throws Exception
     */
    public function displayMainBlog(array $get)
    {
        $aParams = $this->getInitFrontRender();
        $aPostsDate = [];
        $postManager = new PostManager($this->_db);
        $date = date_create('tomorrow midnight');

        //Pagination
        $page = !empty($get['page']) ? $get['page'] : 1;
        $debut = ($page - 1) * NB_POST_PER_PAGE;

        $aPosts = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'c.category_id_parent' => 1,
                'c.is_active' => 1,
                'c.is_private' => 0,
                'p.date_publish' => '<= "' . $date->format('Y-m-d H:i:s') . '"'
            ],
            'p.date_publish',
            'DESC',
            true,
            true,
            false,
            true,
            NB_POST_PER_PAGE,
            $debut,
            COMMENT_ACTIVE
        );

        foreach ($aPosts['aPosts'] as $oPost) {
            $postImage = $oPost->getImageFile();
            if ($postImage == null || !file_exists(ROOT . BLOG_IMG_PATH . "m_" . $postImage)) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }

            $userImage = $oPost->getUser()->getImage();
            if ($userImage == null || !file_exists(ROOT . PROFILE_IMG_PATH . "v_" . $userImage)) {
                $oPost->getUser()->setImage(GENERIC_USER_IMAGE);
            }
        }

        //Dates format
        if (count($aPosts['aPosts'])) {
            for ($i = 0; $i < count($aPosts['aPosts']); $i++) {
                $post = $aPosts['aPosts'][$i];
                $dateFormat = Helper::timeElapsedString($post->getDateUpdate());
                $aPostsDate[$i] = $dateFormat;
            }
            $aParams['dates'] = $aPostsDate;
        }

        //Params
        $aParams['posts'] = $aPosts['aPosts'];
        $aParams['nbTotalPosts'] = $aPosts['nbTotal'];
        $aParams['nbPages'] = ceil($aPosts['nbTotal'] / NB_POST_PER_PAGE);
        $aParams['currentPage'] = $page;

        echo $this->_twig->render('front/pages/blog/displayMainBlog.html.twig', $aParams);
    }
}
