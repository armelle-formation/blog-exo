<?php

class CategoryController extends AbstractController
{

    /**
     * Display all categories with associated infos for BackOffice listing page
     * @throws Exception
     */
    public function displayListCategories()
    {
        $aParams = $this->getInitAdminRender();

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $aParams['title'] = 'Editorial';
            $aParams['subTitle'] = 'Liste des catégories';

            // Categories List
            $catManager = new CategoryManager($this->_db);
            $aArgs = [];
            if (!$this->_userService->isAdminSession()) {
                $aArgs = [
                    "is_private" => 0,
                    "category_id_parent" => 1
                ];
            }

            $aCats = $catManager->getCategoriesByProperties($aArgs, 'c.name', 'ASC', true);

            //Categories with posts count
            $postsCounter = [];
            foreach ($aCats as $cat) {
                $postsCounter[$cat->getCategoryId()]['count'] = $catManager->countPostsByCat($cat->getCategoryId());
            }

            //Authors
            $userManager = new UserManager($this->_db);
            $aAuthors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

            $aParams['categories'] = $aCats;
            $aParams['authors'] = $aAuthors;
            $aParams['postsCounter'] = $postsCounter;

            echo $this->_twig->render('admin/pages/listCategories.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Edit a category. BackOffice page
     * @param null $id
     * @throws Exception
     */
    public function editCategory($id = null)
    {
        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {
            $aParams = $this->getInitAdminRender();

            // List of all blog categories for parents list
            $catManager = new CategoryManager($this->_db);
            $aCats = $catManager->getCategoriesByProperties();

            // Author's list
            $userManager = new UserManager($this->_db);
            $aEditors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

            if (!is_null($id)) {
                $oCat = $catManager->getCatById($id, true);
                $aParams['subTitle'] = "Modification de la catégorie #" . $oCat->getCategoryId();

            } else {
                $aParams['subTitle'] = "Ajout d'une catégorie";
                $oCat = new CategoryEntity();
                $oCat->setUser($userManager->getUserById(intval(Session::getInstance()->getValue('user_id'))));
            }

            $aParams['title'] = 'Editorial';
            $aParams['category'] = $oCat;
            $aParams['categories'] = $aCats;
            $aParams['editors'] = $aEditors;

            echo $this->_twig->render('admin/pages/editCategory.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Save a category after editing. BackOffice page
     * @param array $data
     * @throws BlogException
     * @throws Exception
     */
    public function saveCategory(array $data): void
    {
        $result = [
            "success" => false,
            "message" => "",
            "redirect" => SERVER_BASE . "admin/blog/cat"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {
                    $categoryManager = new CategoryManager($this->_db);

                    // Check if the required fields have been posted
                    if (empty($data['name'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Tous les champs sont obligatoires",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Edit or new
                    if (!empty($data['id'])) {
                        $oCategory = $categoryManager->getCatById($data['id']);
                    } else {
                        $oCategory = new CategoryEntity();
                    }

                    //CheckParent category if send
                    if ($this->_userService->isAdminSession()) {
                        if (!empty($data['parent-category'])) {
                            $parentCat = $categoryManager->getCatById(intval($data['parent-category']));
                            if (is_null($parentCat->getCategoryId()) || !$parentCat->getCategoryId()) {
                                throw new BlogException(
                                    "Not Found",
                                    404,
                                    "La catégorie parente est inconnue",
                                    'DataException', __FUNCTION__
                                );
                            }
                            $oCategory->setCategoryIdParent($data['parent-category']);
                        } else {
                            $oCategory->setCategoryIdParent(null);
                        }

                    }

                    $isPrivate = (!empty($data['isPrivate']) && $data['isPrivate'] == 'on') ? 1 : 0;
                    $isActive = (!empty($data['isActive']) && $data['isActive'] == 'on') ? 1 : 0;
                    $idAuthor = $this->_userService->isAdminSession() ? $data['editor-list'] : $data['idUser'];
                    $userManager = new UserManager($this->_db);
                    $oAuthor = $userManager->getUserById($idAuthor);

                    $oCategory->setName(Helper::cleanPostString($data['name']));
                    $oCategory->setIsPrivate($isPrivate);
                    $oCategory->setUser($oAuthor);

                    //Desactivate cat => Check if this cat has posts
                    if ($oCategory->getIsActive() && !$isActive) {
                        $nbPosts = $categoryManager->countPostsByCat($oCategory->getCategoryId(), true);
                        if (!is_int($nbPosts) || $nbPosts > 0) {
                            throw new BlogException(
                                "Forbidden",
                                403,
                                "Désactivation impossible : contient des articles actifs",
                                'ApplicationException',
                                __FUNCTION__
                            );
                        }
                    }
                    $oCategory->setIsActive($isActive);

                    $idCategory = $categoryManager->saveCategory($oCategory);
                    $result["success"] = true;
                    $result["redirect"] = SERVER_BASE . "admin/blog/cat/" . $idCategory . "/edit";

                } catch (BlogException $e) {
                    $idCategory = null;
                    $result["message"] = $e->getCustomText();
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }

    /**
     * Delete a category. BackOffice page
     * @param $data
     * @throws BlogException
     */
    public function deleteCategory($data)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "admin/blog/cat"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    if (empty($data['idCat'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Paramètres incorrects",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    $categoryManager = new CategoryManager($this->_db);
                    $oCat = $categoryManager->getCatById($data['idCat']);

                    // Category Exists ?
                    if (is_null($oCat->getCategoryId()) || !$oCat->getCategoryId()) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Catégorie indisponible pour suppression",
                            'DataException',
                            __FUNCTION__
                        );
                    }

                    //Check if cat has subCat
                    $aSubCat = $categoryManager->getCategoriesByProperties(['category_id_parent' => $oCat->getCategoryId()]);
                    if (count($aSubCat) > 0) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Suppression impossible : contient des sous catégories",
                            'ApplicationException',
                            __FUNCTION__
                        );
                    }

                    //Check if this cat has posts
                    $nbPosts = $categoryManager->countPostsByCat($oCat->getCategoryId());
                    if (!is_int($nbPosts) || $nbPosts > 0) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Suppression impossible : contient des articles",
                            'ApplicationException',
                            __FUNCTION__
                        );
                    }

                    $categoryManager->deleteCategory($data['idCat']);
                    $result["success"] = true;


                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }
        echo json_encode($result);
    }
}
