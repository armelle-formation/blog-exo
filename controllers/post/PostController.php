<?php

use Twig\Error\SyntaxError;

class PostController extends AbstractController
{

    /**
     * All Posts for the listing page of BackOffice
     * @throws Exception
     */
    public function displayPosts()
    {
        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $aParams = $this->getInitAdminRender();

            $postManager = new PostManager($this->_db);
            $aPosts = $postManager->getAllPostBlog($this->_userService->isAdminSession());

            $aParams['title'] = 'Editorial';
            $aParams['subTitle'] = 'Liste des articles';
            $aParams['posts'] = $aPosts;

            echo $this->_twig->render('admin/pages/listPosts.html.twig', $aParams);

        }else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Display a single post. FrontOffice page
     * @param $id
     * @throws BlogException
     * @throws Exception
     */
    public function displayFrontSinglePost($id)
    {
        if (!empty($id)) {
            $aParams = $this->getInitFrontRender();
            $postManager = new PostManager($this->_db);
            $oPost = $postManager->getPostById($id, true, true, true, true, COMMENT_ACTIVE);

            // Post Exists ?
            if (is_null($oPost->getPostId()) || !$oPost->getPostId()) {
                throw new BlogException(
                    "Not Found",
                    404,
                    "L'article n'est plus disponible",
                    'DataException',
                    __FUNCTION__
                );
            }

            // Active Post ?
            if (!$oPost->getActive()) {
                throw new Exception(
                    "Not Found",
                    404
                );
            }

            //Images
            $postImage = $oPost->getImageFile();
            if ($postImage == null || !file_exists(ROOT . BLOG_IMG_PATH . "l_" . $postImage)) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }

            $userImage = $oPost->getUser()->getImage();
            if ($userImage == null || !file_exists(ROOT . PROFILE_IMG_PATH ."v_" . $userImage)) {
                $oPost->getUser()->setImage(GENERIC_USER_IMAGE);
            }

            //Save number of views
            $date = date_create('now');
            $oPost->setCount($oPost->getCount() + 1);
            $oPost->setDateLastAccess($date->format('Y-m-d H:i:s'));
            $postManager->savePost($oPost);

            //Params
            $aParams['post'] = $oPost;
            $aParams['date_ago'] = Helper::timeElapsedString($oPost->getDateUpdate());
            $aParams['previousPost'] = $postManager->getPreviousPost($oPost->getPostId());
            $aParams['nextPost'] = $postManager->getNextPost($oPost->getPostId());

            echo $this->_twig->render('/front/pages/blog/displayPost.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Display a Posts list from a category. FrontOffice page
     * @param int $idCat
     * @param array $get
     * @throws BlogException
     * @throws SyntaxError
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws Exception
     */
    public function displayPostByCategory(int $idCat, array $get)
    {
        $aParams = $this->getInitFrontRender();
        $postManager = new PostManager($this->_db);
        $date = date_create('tomorrow midnight');

        //Category
        $categoryManager = new CategoryManager($this->_db);
        $currentCat = $categoryManager->getCatById($idCat);

        if (is_null($currentCat->getCategoryId())) {
            throw new BlogException("Not Found", 404, ERROR_MESSAGE, 'DataException');
        }

        //Pagination
        $page = !empty($get['page']) ? $get['page'] : 1;
        $debut = ($page - 1) * NB_POST_PER_PAGE;

        $aPosts = $postManager->getPostsByProperties(
            [
            'p.active' => 1,
            'c.category_id_parent' => 1,
            'c.is_active' => 1,
            'c.is_private' => 0,
            'c.category_id' => $idCat,
            'p.date_publish' => '<= "' . $date->format('Y-m-d H:i:s') . '"'
            ],
            'p.date_publish',
            'DESC',
            true,
            true,
            false,
            true,
            NB_POST_PER_PAGE,
            $debut,
            COMMENT_ACTIVE
        );

        //Properties format
        $formattedPosts = $this->formatPosts($aPosts);
        $aPosts['aPosts'] = $formattedPosts['posts'];
        $aParams['dates'] = $formattedPosts['postDate'];


        //Params
        $aParams['posts'] = $aPosts['aPosts'];
        $aParams['nbTotalPosts'] = $aPosts['nbTotal'];
        $aParams['nbPages'] = ceil($aPosts['nbTotal'] / NB_POST_PER_PAGE);
        $aParams['currentPage'] = $page;
        $aParams['currentCat'] = $currentCat;

        echo $this->_twig->render('/front/pages/blog/displayPostsByCat.html.twig', $aParams);
    }

    /**
     * Display a list of post for a Tag. FrontOffice page
     * @param int $idTag
     * @param array $get
     * @throws BlogException
     * @throws SyntaxError
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Exception
     */
    public function displayPostByTag(int $idTag, array $get)
    {
        $aParams = $this->getInitFrontRender();
        $postManager = new PostManager($this->_db);
        $date = date_create('tomorrow midnight');

        //Tag
        $tagManager = new TagManager($this->_db);
        $currentTag = $tagManager->getTagById($idTag);

        if (is_null($currentTag->getTagID())) {
            throw new BlogException("Not Found", 404, ERROR_MESSAGE, 'DataException');
        }

        //Pagination
        $page = !empty($get['page']) ? $get['page'] : 1;
        $debut = ($page - 1) * NB_POST_PER_PAGE;

        $aPosts = $postManager->getPostsByProperties(
            [
            'p.active' => 1,
            'c.category_id_parent' => 1,
            'c.is_active' => 1,
            'c.is_private' => 0,
            't.tag_id' => $idTag,
            'p.date_publish' => '<= "' . $date->format('Y-m-d H:i:s') . '"'
            ],
            'p.date_publish',
            'DESC',
            true,
            true,
            true,
            false,
            NB_POST_PER_PAGE,
            $debut,
            COMMENT_ACTIVE
        );

        //Properties format
        $formattedPosts = $this->formatPosts($aPosts);
        $aPosts['aPosts'] = $formattedPosts['posts'];
        $aParams['dates'] = $formattedPosts['postDate'];


        //Authors
        $userManager = new UserManager($this->_db);
        $aAuthors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

        //Params
        $aParams['posts'] = $aPosts['aPosts'];
        $aParams['nbTotalPosts'] = $aPosts['nbTotal'];
        $aParams['nbPages'] = ceil($aPosts['nbTotal'] / NB_POST_PER_PAGE);
        $aParams['currentPage'] = $page;
        $aParams['authors'] = $aAuthors;
        $aParams['currentTag'] = $currentTag;

        echo $this->_twig->render('/front/pages/blog/displayPostsByTag.html.twig', $aParams);
    }

    /**
     * Edit post. BackOffice page
     * @param null $id
     * @throws BlogException
     * @throws SyntaxError
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Exception
     */
    public function editPost($id = null)
    {

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $aParams = $this->getInitAdminRender();

            if (!empty($id) && strlen($id)) {
                $postManager = new PostManager($this->_db);
                $oPost = $postManager->getPostById($id, true, true, true);

                // Post Exists ?
                if (is_null($oPost->getPostId()) || !$oPost->getPostId()) {
                    throw new BlogException(
                        "Not Found",
                        404,
                        "L'article n'est pas disponible",
                        'DataException',
                        __FUNCTION__
                    );
                }

                $aParams['subTitle'] = "Modification de l'article #" . $oPost->getPostId();

            } else {
                $oPost = new PostEntity();
                $aParams['subTitle'] = "Ajout d'un article";
            }

            //Get image size (if image)
            $postImage = $oPost->getImageFile();
            if ($postImage != null && file_exists(ROOT . BLOG_IMG_PATH . $postImage)) {
                $aParams['filesize'] = filesize(ROOT . BLOG_IMG_PATH . $postImage);
            }

            //List of Editors
            $userManager = new UserManager($this->_db);
            $aEditors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

            //List of Cat
            $stArgs = [];
            if (!$this->_userService->isAdminSession()) {
                $stArgs = [
                    "is_private" => 0,
                    "category_id_parent" => 1
                ];
            }
            $catManager = new CategoryManager($this->_db);
            $aCats = $catManager->getCategoriesByProperties($stArgs, 'c.category_id_parent, c.name');

            //Array of all tags
            $tagManager = new TagManager($this->_db);
            $aTags = $tagManager->getAllTags();
            $aAllTags = [];

            foreach ($aTags as $tag) {
                $aTag = [];
                $aTag['id'] = $tag->getTagId();
                $aTag['text'] = strtolower($tag->getName());
                array_push($aAllTags, $aTag);
            }

            //Array of tags selected
            $aIdsTagsSelected = array_column($oPost->getTags(), '_tag_id');

            $aParams['title'] = "Editorial";
            $aParams['post'] = $oPost;
            $aParams['editors'] = $aEditors;
            $aParams['allTags'] = $aAllTags;
            $aParams['tagsSelected'] = $aIdsTagsSelected;
            $aParams['categories'] = $aCats;

            echo $this->_twig->render('admin/pages/editPost.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }

    }

    /**
     * Save post after editing. BackOffice page
     * @param $data
     * @param $file
     * @throws BlogException
     * @throws Exception
     */
    public function savePost($data, $file)
    {
        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $result = [
                "success" => false,
                "message" => ERROR_MESSAGE,
                "redirect" => SERVER_BASE . "admin/blog/posts"
            ];

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    $postManager = new PostManager($this->_db);

                    // Check if the required fields have been posted
                    if (empty($data['title']) || empty($data['content']) || empty($data['categories']) || empty($data['datepubli'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Un titre, un texte, une date de publication et une catégorie doivent être renseignés",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Edit or new
                    if (!empty($data['id'])) {
                        $oPost = $postManager->getPostById($data['id'], true, true, true);
                    } else {
                        $oPost = new PostEntity();
                    }

                    $state = (!empty($data['state']) && $data['state'] == 'on') ? 1 : 0;
                    $userManager = new UserManager($this->_db);
                    $idAuthor = $this->_userService->isAdminSession() ? $data['editor-list'] : $data['idUser'];
                    $oAuthor = $userManager->getUserById($idAuthor);
                    $datePubli = str_replace('/', '-', $data['datepubli']);
                    $formatDatePubli = DateTime::createFromFormat('d-m-Y', $datePubli);
                    $hightlight = !empty($data['highlight']) ? $data['highlight'] : 0;

                    //Clean html
                    $config = HTMLPurifier_Config::createDefault();
                    $config->set('Attr.AllowedFrameTargets', array('_blank'));
                    $config->set('HTML.SafeIframe', true);
                    $config->set(
                        'URI.SafeIframeRegexp',
                        '%^(https?:)?(\/\/www\.youtube(?:-nocookie)?\.com\/embed\/|\/\/player\.vimeo\.com\/)%'
                    ); //allow YouTube and Vimeo
                    $config->set('AutoFormat.RemoveEmpty.Predicate', [
                        'iframe' =>
                            array (
                                0 => 'src',
                            )
                    ]);
                    $purifier = new HTMLPurifier($config);
                    $date = date_create('now');

                    $oPost->setTitle($purifier->purify($data['title']));
                    $oPost->setChapo($purifier->purify($data['chapo']));
                    $oPost->setContent($purifier->purify($data['content']));
                    $oPost->setActive($state);
                    $oPost->setDatePublish($formatDatePubli->format('Y-m-d H:i:s'));
                    $oPost->setDateUpdate($date->format('Y-m-d H:i:s'));
                    $oPost->setIsHighlighted($hightlight);
                    $oPost->setUser($oAuthor);

                    //Add Cats
                    if (count(array_filter($data['categories']))) {
                        $postManager->setCategoriesToPost($oPost, implode(",", $data['categories']));
                    }

                    //Add tags
                    if (strlen($data['tags-ids'])) {
                        $postManager->setTagsToPost($oPost, $data['tags-ids']);
                    }

                    // Image detected
                    if (isset($file['image']) && $file['image']['error'][0] == 0 &&  $file['image']['size'] > 0) {
                        $return = File::uploadImage($file, BLOG_IMG_PATH, true, true);
                        if ($return['success']) {
                            if (!is_null($oPost->getImageFile()) && $oPost->getImageFile() != GENERIC_BLOG_IMAGE) {
                                if (strtolower($oPost->getImageFile()) != $return['filename']) {
                                    File::removeImage($oPost->getImageFile(), BLOG_IMG_PATH);
                                }
                            }
                            $oPost->setImageFile($return['filename']);
                        } else {
                            if (MODE_DEV) {
                                throw new BlogException(
                                    "File Upload Error",
                                    500,
                                    $return['message'],
                                    'FileException',
                                    __FUNCTION__
                                );
                            } else {
                                throw new BlogException(
                                    "File Upload Error",
                                    500,
                                    "Une erreur s'est produite à l'enregistrement de l'image",
                                    'FileException',
                                    __FUNCTION__
                                );
                            }
                        }
                    //No image detected but flag img-changed == true
                    } else {
                        if (isset($data['img-changed']) && $data['img-changed']) {
                            if (!is_null($oPost->getImageFile()) && $oPost->getImageFile() != GENERIC_BLOG_IMAGE) {
                                File::removeImage($oPost->getImageFile(), BLOG_IMG_PATH);
                            }
                            $oPost->setImageFile(null);
                        }
                    }

                    $idPost = $postManager->savePost($oPost);
                    $result["success"] = true;
                    $result["redirect"] = SERVER_BASE . "admin/blog/posts/" . $idPost . "/edit";

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }

        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }

    /**
     * Delete post. BackOffice action
     * @param $data
     * @throws BlogException
     * @throws Exception
     */
    public function deletePost($data)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "admin/blog/posts"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    if (empty($data['idPost'])) {
                        throw new BlogException("Not Found", 404, ERROR_MESSAGE, 'DataException', __FUNCTION__);
                    }

                    $postManager = new PostManager($this->_db);
                    $oPost = $postManager->getPostById($data['idPost']);

                    // Post Exists ?
                    if (is_null($oPost->getPostId()) || !$oPost->getPostId()) {
                        throw new BlogException(
                            "Not Found",
                            404,
                            "L'article n'est plus disponible à la suppression",
                            'DataException',
                            __FUNCTION__
                        );
                    }

                    //Delete image
                    if ($oPost->getImageFile() != GENERIC_BLOG_IMAGE) {
                        File::removeImage($oPost->getImageFile(), BLOG_IMG_PATH);
                    }

                    $postManager->deletePost($data['idPost']);
                    $result["success"] = true;

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }

    /**
     * Function to formatted data of posts (avoid duplicated code)
     * @param $aPosts
     * @return array
     * @throws Exception
     */
    private function formatPosts($aPosts)
    {
        $return = [];
        $aPostsDate = [];
        $nbPosts = count($aPosts['aPosts']);

        for ($i = 0; $i < $nbPosts; $i++) {
            $oPost = $aPosts['aPosts'][$i];
            $dateFormat = Helper::timeElapsedString($oPost->getDateUpdate());
            $aPostsDate[$i] = $dateFormat;

            $userImage = $oPost->getUser()->getImage();
            if ($userImage == null || !file_exists(ROOT . PROFILE_IMG_PATH . "v_" . $userImage)) {
                $oPost->getUser()->setImage(GENERIC_USER_IMAGE);
            }

            $postImage = $oPost->getImageFile();
            if ($postImage == null || !file_exists(ROOT . BLOG_IMG_PATH ."m_" . $postImage)) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }
        }

        $return['posts'] = $aPosts['aPosts'];
        $return['postDate'] = $aPostsDate;

        return $return;
    }
}
