<?php

use Twig\Loader\FilesystemLoader;
use Twig\Environment;

class AbstractController
{
    /**
     * @var DbConnect
     */
    protected $_db;

    /**
     * @var \Twig\Environment
     */
    protected $_twig;

    /**
     * @var UserManager
     */
    protected $_userManager;

    /**
     * @var UserService
     */
    protected $_userService;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->_db =  new DbConnect();

        //Managers
        $this->_userManager = new UserManager($this->_db);

        //Service
        $this->_userService = new UserService();

        //Session
        Session::getInstance();

        // Twig Configuration
        $loader = new Twig\Loader\FilesystemLoader('./views');

        $this->_twig = new Twig\Environment($loader, array(
            'cache' => false,
            'debug' => true,
            'autoescape' => false
        ));
        $this->_twig->getExtension(Twig\Extension\CoreExtension::class)->setTimezone('Europe/Paris');
        $this->_twig->addExtension(new Twig_Extensions_Extension_Intl());
        $this->_twig->addExtension(new Twig_Extensions_Extension_Text());
        $this->_twig->addExtension(new Twig\Extension\DebugExtension());
        $this->_twig->addGlobal("sUrl", $this->getPathUrl(false));

    }

    /**
     * Return the http referer url
     * @param bool $referer
     * @return string
     */
    protected function getUrl(bool $referer = false)
    {
        if ($referer) {
            return $_SERVER['HTTP_REFERER'];
        } else {
            return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }
    }

    /**
     * @param bool $asArray
     * @return array|string
     */
    protected function getPathUrl($asArray = true)
    {
        $parsedUrl = parse_url($this->getUrl());
        $return = isset($parsedUrl['path']) ? $parsedUrl['path'] : '/';
        if ($asArray) {
            $return = array_filter(explode("/", $return));
        }
        return $return;
    }

    /**
     * Init common data for all Admin pages
     * @return array
     * @throws Exception
     */
    protected function getInitAdminRender()
    {
        $path = ROOT . PROFILE_IMG_PATH;
        $userManager = new UserManager($this->_db);
        $oCurrentUser = $userManager->getUserById(intval(Session::getInstance()->getValue('user_id')));

        if ($oCurrentUser->getImage() === null || !file_exists($path . 'v_' . $oCurrentUser->getImage())) {
            $image = GENERIC_USER_IMAGE;
        } else {
            $image = $oCurrentUser->getImage();
        }

        //Notifications

        return [
            'role' => $oCurrentUser->getRoleId(),
            'currentUserId' => $oCurrentUser->getUserId(),
            'image' => $image,
            'fullname' => Session::getInstance()->getValue('fullname'),
            'footer' => true
        ];
    }

    /**
     * Init common data for all Front pages
     * @return array
     * @throws Exception
     */
    protected function getInitFrontRender()
    {
        $tagManager = new TagManager($this->_db);
        $categoryManager = new CategoryManager($this->_db);
        $postManager = new PostManager($this->_db);
        $counter = 0;
        $date = date_create('tomorrow midnight');

        //Cats and post count by cat
        $allCatWithCount = [];
        $allCats = $categoryManager->getCategoriesByProperties([
            "is_private" => 0,
            "category_id_parent" => 1,
            "is_active" => 1
        ]);

        //Categories with posts count
        foreach ($allCats as $cat) {
            $allCatWithCount[$counter]['idCat'] = $cat->getCategoryId();
            $allCatWithCount[$counter]['nameCat'] = $cat->getName();
            $allCatWithCount[$counter]['count'] = $categoryManager->countPostsByCat($cat->getCategoryId(), true);

            $counter++;
        }

        //Session
        $userManager = new UserManager($this->_db);
        $oCurrentUser = $userManager->getUserById(intval(Session::getInstance()->getValue('user_id')));

        //Latests posts
        $aLatestsPosts = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'c.category_id_parent' => 1,
                'c.is_active' => 1,
                'c.is_private' => 0,
                'p.date_publish' => '<= "' . $date->format('Y-m-d H:i:s') . '"'
            ],
            'p.date_publish',
            'DESC',
            false,
            true,
            false,
            false,
            3
        );
        foreach ($aLatestsPosts['aPosts'] as $oPost) {
            if ($oPost->getImageFile() == null || !file_exists(ROOT . BLOG_IMG_PATH . "v_" . $oPost->getImageFile())) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }
        }

        $aHightlightPosts = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'c.category_id_parent' => 1,
                'c.is_active' => 1,
                'c.is_private' => 0,
                'p.date_publish' => '<= "' . $date->format('Y-m-d H:i:s') . '"',
                'p.is_highlighted' => 1
            ],
            'p.date_publish',
            'DESC',
            true,
            true,
            false,
            true,
            3,
            null,
            COMMENT_ACTIVE
        );
        foreach ($aHightlightPosts['aPosts'] as $oPost) {
            if ($oPost->getImageFile() == null || !file_exists(ROOT . BLOG_IMG_PATH . "v_" . $oPost->getImageFile())) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }
        }

        $oPostPrivacy = $postManager->getPostById(POST_PRIVACY);
        $oPostLegals = $postManager->getPostById(POST_LEGALS);

        //Tags : for keep more used order, getPosts must be true !
        $aAllTags = $tagManager->getTagsByProperties([], 'nbPosts', 'DESC', false, true);

        return [
            'allCatsWithCount' => $allCatWithCount,
            'allTags' => $aAllTags,
            'latestPosts' => $aLatestsPosts['aPosts'],
            'HightlightPosts' => $aHightlightPosts['aPosts'],
            'privacyPostActive' => $oPostPrivacy->getActive(),
            'legalPostActive' => $oPostLegals->getActive(),
            'role' => $oCurrentUser->getRoleId(),
            'currentUserId' => $oCurrentUser->getUserId(),
            'footer' => true
        ];
    }
}
