<?php

class CommentController extends AbstractController
{
    /**
     * Display all comments for BackOffice listing page
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function displayAllComments()
    {

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {
            $aParams = $this->getInitAdminRender();

            $commentManager = new CommentManager($this->_db);
            $aComments = $commentManager->getCommentsByProperties(
                [],
                true,
                true,
                true,
                'c.date_create',
                'DESC'
            );

            $statusManager = new statusManager($this->_db);
            $aStatus = $statusManager->getAllStatus();

            $aParams['title'] = 'Commentaires';
            $aParams['subTitle'] = 'Liste des commentaires';
            $aParams['comments'] = $aComments;
            $aParams['aStatus'] = $aStatus;

            echo $this->_twig->render('admin/pages/listComments.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }

    }

    /**
     * Edit comment. BackOffice page
     * @param $id
     * @throws BlogException
     */
    public function editComment($id)
    {

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            if (!isset($id) && strlen($id) === 0) {
                throw new BlogException(
                    "Forbidden",
                    403,
                    "Paramètres incorrects",
                    'ArgumentsException',
                    __FUNCTION__
                );
            }

            try {
                $commentManager = new CommentManager($this->_db);
                $oComment = $commentManager->getCommentById($id);

                // Comment Exists ?
                if (is_null($oComment->getCommentId()) || !$oComment->getCommentId()) {
                    throw new BlogException(
                        "Not Found",
                        404,
                        "Le commentaire n'existe pas",
                        'DataException',
                        __FUNCTION__
                    );
                }

                $aParams['title'] = "Visualisation du commentaire #" . $oComment->getCommentId();

                //List of status
                $statusManager = new StatusManager($this->_db);
                $aAllStatus = $statusManager->getAllStatus();

                $aParams['comment'] = $oComment;
                $aParams['success'] = true;
                $aParams['aStatus'] = $aAllStatus;

            } catch (BlogException $e) {
                $aParams['success'] = false;
            }

            echo json_encode($aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }

    }

    /**
     * Save comment after editing. BackOffice page
     * @param $data
     * @throws BlogException
     */
    public function saveComment($data)
    {

        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE
        ];

        if ($this->_userService->checkSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                $commentManager = new CommentManager($this->_db);
                $result["redirect"] = SERVER_BASE . "admin/comments";

                try {
                    if (empty($data['pseudo']) || empty($data['title']) || empty($data['usercomment'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Tous les champs sont obligatoires",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Edit or new
                    if (isset($data['commentId']) && !empty($data['commentId'])) {
                        $oComment = $commentManager->getCommentById($data['commentId']);
                    } else {
                        $oComment = new CommentEntity();
                        $result["redirect"] = SERVER_BASE . "blog/article-" . $data['postId'] . "#comments";
                    }

                    //Post exists ?
                    $postManager = new PostManager($this->_db);
                    $oPost = $postManager->getPostById($data['postId'], false, false, false);
                    if (is_null($oPost->getPostId())) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            ERROR_MESSAGE,
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //User Exists ?
                    $userManager = new UserManager($this->_db);
                    $oUser = $userManager->getUserById(Session::getInstance()->getValue('user_id'));
                    if (is_null($oUser->getUserId())) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            ERROR_MESSAGE,
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Clean html
                    $config = HTMLPurifier_Config::createDefault();
                    $purifier = new HTMLPurifier($config);

                    //Status (to moderate by default)
                    $statusManager = new StatusManager($this->_db);
                    $idStatus = isset($data['statusComment']) ? intval($data['statusComment']) : COMMENT_DEFAULT_STATUS;
                    $oStatus = $statusManager->getStatusById($idStatus);

                    //Title & Comment length
                    $title = $purifier->purify($data['title']);
                    $text = $purifier->purify($data['usercomment']);
                    $pseudo = $purifier->purify($data['pseudo']);
                    if (strlen($title) > 255) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Titre trop long",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }
                    if (strlen($pseudo) > 255) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Pseudo trop long",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }
                    if (strlen($text) > 450) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Texte trop long",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Hydrate oUser
                    if (!isset($data['org'])) {
                        $oComment->setUser($oUser);
                    }

                    $oComment->setPost($oPost);
                    $oComment->setStatus($oStatus);
                    $oComment->setPseudo($pseudo);
                    $oComment->setCommentTitle($title);
                    $oComment->setCommentText($text);

                    //Save comment
                    $commentManager->saveComment($oComment);
                    $result["success"] = true;
                    $result["message"] = "Merci, votre commentaire apparaîtra dans la liste des commentaires après modération.";

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();

                } catch (Exception $e) {
                    if (MODE_DEV) {
                        Debug::printr($e->getMessage());
                        exit();
                    }
                    $result["message"] = ERROR_MESSAGE;
                }
            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new BlogException(
                "Forbidden",
                403,
                "Vous devez être connecté pour laisser un commentaire",
                'ApplicationException',
                __FUNCTION__
            );
        }

        echo json_encode($result);
    }

    /**
     * Delete a comment. BackOffice action
     * @param $data
     * @throws Exception
     */
    public function deleteComment($data)
    {

        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "admin/comments"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    if (empty($data['idComment'])) {
                        throw new BlogException('Forbidden', ERROR_MESSAGE, '');
                    }

                    $commentManager = new CommentManager($this->_db);
                    $oComment = $commentManager->getCommentById($data['idComment']);

                    // Comment Exists ?
                    if (is_null($oComment->getCommentId()) || $oComment->getCommentId() == 0) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Commentaire indisponible pour suppression",
                            'DataException',
                            __FUNCTION__
                        );
                    }

                    $commentManager->deleteComment($data['idComment']);
                    $result["success"] = true;

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }


            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }

    /**
     * Change status of a comment.
     * @param $data
     * @throws BlogException
     */
    public function changeStatus($data)
    {

        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "blog"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    if (!isset($data['idComment']) || !isset($data['idStatus'])) {
                        throw new BlogException('Forbidden', ERROR_MESSAGE, '');
                    }

                    $commentManager = new CommentManager($this->_db);
                    $oComment = $commentManager->getCommentById($data['idComment']);

                    // Comment Exists ?
                    if (is_null($oComment->getCommentId()) || !$oComment->getCommentId()) {
                        throw new BlogException(
                            'Forbidden',
                            "Commentaire indisponible à la suppression",
                            ''
                        );
                    }

                    if (isset($data['idPost'])) {
                        $result["redirect"] .= "/article-" . $data['idPost'] . "#list-comments";
                    }
                    $commentManager->changeStatusComment($data['idComment'], $data['idStatus']);
                    $result["success"] = true;
                    $result["message"] = "";

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }

                echo json_encode($result);
            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }
    }
}
