<?php

class AdminController extends AbstractController
{

    /**
     * AdminController Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->_userService->checkSession();
    }

    /**
     * Get the index page of admin interface : the dashboard
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Twig\Error\SyntaxError
     * @throws Exception
     */
    public function displayDashboard()
    {
        if ($this->_userService->isBackSession()) {

            $aParams = $this->getInitAdminRender();
            $userManager = $userManager = new UserManager($this->_db);
            $postManager = new PostManager($this->_db);
            $commentManager = new CommentManager($this->_db);

            //Bar Chart
            $aParams['aPostsChart'] = $postManager->getStatsPostsPublished();

            //Post by Category
            $aParams['postsByCat'] = json_encode($postManager->getPostsByCategory(), JSON_UNESCAPED_UNICODE);

            //ActiveComments chart
            $aParams['aCommentsChart'] = json_encode($userManager->getStatsActiveCommentators(), JSON_UNESCAPED_UNICODE);

            //Comments by months
            $aParams['aCommentsMonths'] = json_encode($commentManager->getCommentsPublishedByMonths(), JSON_UNESCAPED_UNICODE);

            //Latest Posts
            $aLatestsPosts = $postManager->getPostsByProperties(
                [
                    'p.active' => 1,
                    'c.category_id_parent' => 1,
                    'c.is_active' => 1,
                    'c.is_private' => 0
                ],
                'p.date_publish',
                'DESC',
                true,
                true,
                false,
                false,
                4
            );
            foreach ($aLatestsPosts['aPosts'] as $oPost) {
                $oPost->setDateCreate(Helper::timeElapsedString($oPost->getDateCreate()));
            }
            $aParams['aLatestsPosts'] = $aLatestsPosts;

            //Posts widget
            $nbPosts = count($postManager->getAllPostBlog());
            $nbActivePosts = count($postManager->getAllPostBlog(false, true));
            $aParams['posts']['nb'] = $nbPosts;
            $aParams['activePosts']['nb'] = $nbActivePosts;

            //Top User widget
            $topUser = $userManager->getTopUser();
            $userImage = $topUser['user']->getImage();
            if ($userImage == null || !file_exists(ROOT . PROFILE_IMG_PATH ."v_" . $userImage)) {
                $topUser['user']->setImage(GENERIC_USER_IMAGE);
            }
            $aParams['topuser'] = $topUser;

            //Users widget
            $nbUsers = count($userManager->getAllUsers());
            $nbActiveCommentator = count($userManager->getUsersByProperties([
                "u.active" => 1,
                "u.role_id" => 3
            ]));
            $aParams['users']['nb'] = $nbUsers;
            $aParams['activeCommentators']['nb'] = $nbActiveCommentator;

            //Comment widget
            $commentManager = new CommentManager($this->_db);
            $aParams['commentToMod']['nb'] = count($commentManager->getCommentsByProperties(
                [
                'c.status_id' => COMMENT_TO_MODERATE
                ]
            ));

            //Msg Widget
            $serviceManager = new MessageManager($this->_db);
            $oMessage = $serviceManager->getLastMsg();
            $aParams['lastMsg'] = $oMessage;
            $aParams['lastMsgAgo'] = Helper::timeElapsedString($oMessage->getDateSend());

            //BestPost
            $aParams['topPost'] = $postManager->getTopPost(2);

            //Weather widget
            $aParams['weather']['available'] = false;
            $aParams['nextblock']['width'] = 6;
            if (session::getInstance()->getValue('geo_consent')) {
                //Localization API
                $geoInfos = Api::getLocalization();
                $urlService = "http://openweathermap.org/img/wn/";

                //Get weather infos
                if ($geoInfos !== false) {
                    $weatherInfos = Api::getWeatherInfo('gps');
                    if ($weatherInfos !== false) {
                        if (is_array($weatherInfos) && !array_key_exists('errors', $weatherInfos)) {
                            $aParams['weather']['temperature'] = round($weatherInfos['main']['temp']);
                            $aParams['weather']['description'] = $weatherInfos['weather'][0]['description'];
                            $aParams['weather']['date'] = $weatherInfos['dt'];
                            $aParams['weather']['city'] = $weatherInfos['name'];
                            $aParams['weather']['icon'] = $urlService . $weatherInfos['weather'][0]['icon'] . "@2x.png";
                            $aParams['weather']['available'] = true;
                            $aParams['nextblock']['width'] = 4;
                        }
                    }
                }
            }

            //Params
            echo $this->_twig->render('admin/pages/displayDashboard.html.twig', $aParams);

        }else {
            throw new Exception("Forbidden", 403);
        }
    }
}
