<?php

class TagController extends AbstractController
{


    /**
     * Display all tag for listing page of BackOffice
     */
    public function displayListTags()
    {
        $aParams = $this->getInitAdminRender();

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $tagManager = new TagManager($this->_db);
            $aTags = $tagManager->getAllTags();

            //Authors
            $userManager = new UserManager($this->_db);
            $aAuthors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

            $aParams['title'] = 'Editorial';
            $aParams['subTitle'] = 'Liste des tags';
            $aParams['tags'] = $aTags;
            $aParams['authors'] = $aAuthors;

            echo $this->_twig->render('admin/pages/listTags.html.twig', $aParams);

        }else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Display page for edit a tag. BackOffice page
     * @param null $id
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function editTag($id = null)
    {
        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            $aParams = $this->getInitAdminRender();
            $aParams['title'] = 'Tags';

            if (!is_null($id)) {

                $tagManager = new TagManager($this->_db);
                $oTag = $tagManager->getTagById($id);

                //Posts with this tag
                $postManager = new PostManager($this->_db);
                $aPosts = $postManager->getPostsByTagId($oTag->getTagId());

                //Params
                $aParams['subTitle'] = 'Modification du Tag #' . $oTag->getTagId();

            } else {
                $userManager = new UserManager($this->_db);
                $oTag = new TagEntity();
                $oTag->setUser($userManager->getUserById(intval(Session::getInstance()->getValue('user_id'))));

                //Posts with this tag
                $aPosts = [];

                //Params
                $aParams['subTitle'] = 'Ajouter un tag';
            }
            $oTag->setPosts($aPosts);

            // Author's list
            $userManager = new UserManager($this->_db);
            $aEditors = $userManager->getUserByRole([ID_ADMIN, ID_EDITOR]);

            $aParams['tag'] = $oTag;
            $aParams['aPosts'] = $aPosts;
            $aParams['editors'] = $aEditors;

            echo $this->_twig->render('admin/pages/editTag.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }

    }

    /**
     * Save tag after editing. BackOffice page
     * @param $data
     * @throws Exception
     */
    public function saveTag($data)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "admin/blog/tags"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isBackSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                try {
                    $tagManager = new TagManager($this->_db);

                    // Check if the required fields have been posted
                    if (empty($data['libelle'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Tous les champs sont obligatoires",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    //Edit or new
                    if (!empty($data['id'])) {
                        $oTag = $tagManager->getTagById($data['id']);
                    } else {
                        $oTag = new TagEntity();
                    }

                    $idAuthor = $this->_userService->isAdminSession() ? $data['editor-list'] : $data['idUser'];
                    $userManager = new UserManager($this->_db);
                    $oAuthor = $userManager->getUserById($idAuthor);

                    $oTag->setUser($oAuthor);
                    $oTag->setName(Helper::cleanPostString($data['libelle']));

                    $idTag = $tagManager->saveTag($oTag);
                    $result["success"] = true;
                    $result["redirect"] = SERVER_BASE . "admin/blog/tags/" . $idTag . "/edit";

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }
            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }

    /**
     * Delete tag. BackOffice page. Admin only
     * @param $data
     * @throws Exception
     */
    public function deleteTag($data)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE . "admin/blog/tags"
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    if (!isset($data['idTag'])) {
                        throw new BlogException("Not Found", 404, ERROR_MESSAGE, 'DataException', __FUNCTION__);
                    }

                    $tagManager = new TagManager($this->_db);
                    $oTag = $tagManager->getTagById($data['idTag']);

                    // Tag Exists ?
                    if (is_null($oTag->getTagId()) || !$oTag->getTagId()) {
                        throw new BlogException(
                            "Not Found",
                            404,
                            "Le Tag n'est plus disponible à la suppression",
                            'DataException',
                            __FUNCTION__
                        );
                    }

                    $tagManager->deleteTag($data['idTag']);
                    $result["success"] = true;

                }catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                }


            } else {
                throw new Exception("Method Not Allowed", 405);
            }
        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }
}
