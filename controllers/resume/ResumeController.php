<?php


class ResumeController extends AbstractController
{

    /**
     * Display home page of resume section. FrontOffice page
     */
    public function displayResume()
    {
        $oData = Data::getInstance();
        $oData->setData(JSON_DATA_FILE);

        $ioResumeService = new ResumeService();
        $oResume = $ioResumeService->buildResume();

        $aParams = $this->getInitFrontRender();
        $aParams['title'] = 'CV';
        $aParams['resume'] = $oResume;
        $aParams['footer'] = false;

        echo $this->_twig->render('front/pages/resume/displayResume.html.twig', $aParams);
    }


    /**
     * Display page for edit resume. BackOffice page
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Twig\Error\SyntaxError
     */
    public function editResume()
    {

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            $path = 'uploads/documents/cv/';

            $aParams = $this->getInitAdminRender();
            $aParams['title'] = 'Curriculum Vitae';
            $aParams['subTitle'] = 'Edition';
            $aParams['data'] = file_get_contents(JSON_DATA_FILE);

            if (file_exists($path . NAMEFILE_RESUME)) {
                $aParams['name_file'] = NAMEFILE_RESUME;
                $aParams['file'] = CV_PATH . NAMEFILE_RESUME;
                $aParams['filesize'] = filesize($path . NAMEFILE_RESUME);
            }

            echo $this->_twig->render('admin/pages/editResume.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }

    }

    /**
     * Save resume on a json file after editing. BackOffice page
     * @param array $aData
     * @param object $oFile
     * @return string
     * @throws Exception
     */
    public function saveResume($aData, $oFile)
    {
        $result = [
            "success" => false,
            "redirect" => SERVER_BASE . "admin/resume",
            "message" => ERROR_MESSAGE
        ];

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {

                    //JSON FILE
                    $fJson = JSON_DATA_FILE;

                    if (empty($aData['resume'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Le JSON ne peut être vide",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    $sData = Helper::cleanPostString($aData['resume'], true, false);
                    file_put_contents($fJson, $sData);
                    $result["data"] = $sData;

                    // FILE CV
                    if (empty($aData['file-name'])) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Une version PDF du CV est obligatoire",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }

                    // UPLOAD FILE CV
                    if (isset($oFile['file']) && $oFile['file']['error'][0] == 0 && $oFile['file']['size'] > 0) {
                        $return = File::uploadFile($oFile, CV_PATH, NAMEFILE_RESUME, ['pdf'], 'application/pdf');
                        if (!$return['success']) {
                            throw new BlogException(
                                "File Upload Error",
                                500,
                                $return['message'],
                                'FileException',
                                __FUNCTION__
                            );
                        }
                    }

                    $result["success"] = true;

                } catch (BlogException $e) {
                    $result["message"] = $e->getCustomText();
                } catch (Exception $e) {
                    if (MODE_DEV) {
                        $result["message"] = $e->getMessage();
                    } else {
                        $result["message"] = ERROR_MESSAGE;
                    }
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }

        } else {
            throw new Exception("Forbidden", 403);
        }

        echo json_encode($result);
    }
}
