<?php

/**
 * Class ErrorController : this class not extends AbstractController to be abble to catch PDOException
 * (Constructor of AbstractController init Database connection)
 */

class ErrorController
{
    /**
     * @var \Twig\Environment
     */
    protected $_twig;

    /**
     * Constructor
     */
    public function __construct()
    {

        //Session
        Session::getInstance();

        // Twig Configuration
        $loader = new Twig\Loader\FilesystemLoader('./views');

        $this->_twig = new Twig\Environment($loader, array(
            'cache' => false,
            'debug' => true,
            'autoescape' => false
        ));
        $this->_twig->getExtension(Twig\Extension\CoreExtension::class)->setTimezone('Europe/Paris');
        $this->_twig->addExtension(new Twig_Extensions_Extension_Intl());
        $this->_twig->addExtension(new Twig_Extensions_Extension_Text());
        $this->_twig->addExtension(new Twig\Extension\DebugExtension());

    }

    /**
     * Allows to customize rendering of BlogException
     * @param BlogException $e
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function dspBlogError(BlogException $e)
    {

        $errorCode = $e->getCode();

        if ($errorCode === 404) {
            $this->dspError($e);
        }

        $aParams = [
            'code' => $errorCode
        ];

        if (MODE_DEV) {
            $message = $e->getCustomText();
            $aParams['addInfos'] = $e->getAdditionalMessage();
            $aParams['type'] = $e->getType();
            $aParams['trace'] = $e->getTraceAsString();
        } else {
            $message = !empty($e->getCustomText()) ?
                $e->getCustomText() :
                "Une erreur d'exécution s'est produite";
        }

        $aParams['title'] = 'Erreur ' . $errorCode;
        $aParams['code'] = $errorCode;
        $aParams['message'] = $message;
        $aParams['footer'] = false;

        echo $this->_twig->render('error/displayError.html.twig', $aParams);
    }

    /**
     * Allows to customize the treatment or rendering of an specific error (switch with error code)
     * Can be extended with others errorCodes
     * @param Exception $e
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function dspError(Exception  $e)
    {
        $errorCode = $e->getCode();

        // Extended here : errorCode + associated view
        switch ($errorCode) {
            case 404:
                $nameView = 'error/404.html.twig';
                $message = "Page non trouvée";
            break;
            default:
                $nameView = VIEW_ERROR;
                $message = ERROR_MESSAGE;
            break;
        }

        $aParams = [
            'RoleEntity' => intval(Session::getInstance()->getValue('RoleEntity')),
            'title' => 'Erreur',
            'code' => $errorCode,
            'message' => $message,
            'footer' => false
        ];

        if ($errorCode > 0) {
            $aParams['title'] = $aParams['title']. ' ' . $errorCode;
        }
        if (MODE_DEV) {
            $aParams['trace'] = $e->getTraceAsString();
        }

        echo $this->_twig->render($nameView, $aParams);
    }
}
