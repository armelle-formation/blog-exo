<?php

use Firebase\JWT\JWT;

class UserController extends AbstractController
{

    /**
     * Users list from BackOffice page
     * @return string|null
     * @throws
     */
    public function displayUsers()
    {

        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            $aParams = $this->getInitAdminRender();

            $userManager = new UserManager($this->_db);
            $aUsers = $userManager->getAllUsers();

            $aParams['title'] = 'Administration';
            $aParams['subTitle'] = 'Liste des utilisateurs';
            $aParams['users'] = $aUsers;

            echo $this->_twig->render('admin/pages/listUsers.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Edit User. BackOffice page
     * @param null $id
     * @throws BlogException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function editUser($id = null)
    {
        $aParams = $this->getInitAdminRender();
        $path = ROOT . PROFILE_IMG_PATH;

        if (
            ($this->_userService->checkSession() && $this->_userService->isAdminSession()) ||
            ($id !== null && $id == $aParams['currentUserId'])
        ) {
            if (!empty($id) && strlen($id)) {

                $userManager = new userManager($this->_db);
                $oUser = $userManager->getUserById($id);

                $aParams['subTitle'] = 'Editer un utilisateur';

            } else {

                $oUser = new UserEntity();
                $aParams['subTitle'] = 'Ajouter un utilisateur';
            }

            //Roles
            $roleManager = new RoleManager($this->_db);
            $aRoles = $roleManager->getAllRoles();

            //Params
            $aParams['title'] = 'Administration';
            $aParams['user'] = $oUser;
            $aParams['roles'] = $aRoles;

            if ($oUser->getImage() != null && file_exists($path . 'b_' . $oUser->getImage())) {
                $aParams['filesize'] = filesize($path . 'b_' . $oUser->getImage());
                $aParams['image_file'] = 'b_' . $oUser->getImage();
            }

            echo $this->_twig->render('admin/pages/editUser.html.twig', $aParams);

        } else {
            throw new Exception("Forbidden", 403);
        }
    }

    /**
     * Save user after editing. BackOffice action
     * @param $data
     * @param $file
     * @throws Exception
     */
    public function saveUser($data, $file)
    {
        $idUser = null;
        $aEmailParams = [];
        $result = [
            "success" => false,
            "message" => "",
            "redirect" => SERVER_BASE . "admin/config/users",
            "sendemail" => 0
        ];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            try {

                //Remember idUser for redirections in errors cases
                if (!empty($data['id'])) {
                    $idUser = $data['id'];
                }

                // Check if the required fields have been posted
                if (empty($data['firstname']) || empty($data['lastname']) || empty($data['useremail'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Tous les champs sont obligatoires",
                        "ArgumentsException",
                        __FUNCTION__
                    );
                }

                // Email valid
                if (!Helper::isValidEmail($data['useremail'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "L'email est incorrect",
                        "ValidityException",
                        __FUNCTION__
                    );
                }


                //Edit or new
                $userManager = new UserManager($this->_db);
                if (!empty($data['id'])) {
                    $oUser = $userManager->getuserById($data['id']);
                    $isNew = false;
                } else {
                    $oUser = new UserEntity();
                    $isNew = true;
                }

                //Clean variables
                $lastName = Helper::cleanPostString($data['lastname']);
                $firstName = Helper::cleanPostString($data['firstname']);
                $email = Helper::cleanPostString($data['useremail']);

                $state = (!empty($data['state']) && $data['state'] == 'off') ? 0 : 1; // Active by default
                $consent = (!empty($data['geo-consent'])) ? $data['geo-consent'] : 0; // Default : no consent

                //Role
                $session = Session::getInstance();
                //Edit existing user and user = current user (role not changed)
                if (!is_null($oUser->getUserId()) && $oUser->getUserId() == $session->getValue('user_id')) {
                    $roleId = $oUser->getRoleId();

                //Edit user : role = value of select
                } elseif (!empty($data['role-list']) && in_array($data['role-list'], [1,2,3])) {
                    $roleId = $data['role-list'];

                // Other cases (new...) Role = Commentator
                } else {
                    $roleId = ID_READER; //Default Role : Commentator
                }

                //User Exists ?
                $userExists = $userManager->checkUserExists($email, $oUser->getUserId());

                if ($userExists) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Cet email correspond à un utilisateur existant",
                        "DataException",
                        __FUNCTION__
                    );
                }

                //Check valid strings
                if (!Helper::stringLettersOnly($lastName) || !Helper::stringLettersOnly($firstName)) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Les prénoms et noms doivent être des chaînes de caractères valides",
                        "ValidityException",
                        __FUNCTION__
                    );
                }

                //Hydrate oUser
                $oUser->setFirstname($firstName);
                $oUser->setLastname($lastName);
                $oUser->setEmail($email);
                $oUser->setActive($state);
                $oUser->setRoleId($roleId);
                $oUser->setGeoConsent($consent);


                // Image
                if (isset($file['image']) && $file['image']['error'][0] == 0 && $file['image']['size'] > 0) {
                    $return = File::uploadImage($file, PROFILE_IMG_PATH, false, true);
                    if ($return['success']) {
                        if (!is_null($oUser->getImage())) {
                            File::removeImage($oUser->getImage(), PROFILE_IMG_PATH);
                        }
                        $oUser->setImage($return['filename']);
                    } else {
                        throw new BlogException(
                            "File Upload Error",
                            500,
                            $return['success'],
                            'FileException',
                            __FUNCTION__
                        );
                    }

                } else {
                    if (!empty($data['img-changed']) && $data['img-changed']) {
                        if (!is_null($oUser->getImage())) {
                            File::removeImage($oUser->getImage(), PROFILE_IMG_PATH);
                        }
                        $oUser->setImage(null);
                    }
                }

                // Password
                $pass =  !empty($data['userpassword']) ? Helper::cleanPostString($data['userpassword'], false) : "";
                $confPass = !empty($data['password-confirm']) ? Helper::cleanPostString($data['password-confirm'], false) : "";

                // Add User (password required)
                if ($isNew) {
                    if (empty($pass)) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            "Veuillez renseigner un mot de passe",
                            'ArgumentsException',
                            __FUNCTION__
                        );
                    }
                    $passwordError = Helper::isPasswordComplex($pass);
                    if (!$passwordError["success"]) {
                        throw new BlogException(
                            "Forbidden",
                            403,
                            $passwordError['message'],
                            'ValidityException',
                            __FUNCTION__
                        );
                    }

                    $oUser->setPassword(password_hash($pass, PASSWORD_DEFAULT)); //Password hash

                    //Prepare confirm email
                    $aEmailParams["template"] = "createUser";
                    $aEmailParams["subject"] = "Création de votre compte";
                    $aEmailParams["password"] = $pass;

                // Update user (password not required)
                } else {

                    if (!empty($pass)) {
                        $passwordError = Helper::isPasswordComplex($pass);

                        if (!$passwordError["success"]) {
                            throw new BlogException(
                                "Forbidden",
                                403,
                                $passwordError['message'],
                                'ValidityException',
                                __FUNCTION__
                            );
                        }

                        if (empty($confPass)) {
                            throw new BlogException(
                                "Forbidden",
                                403,
                                "Veuillez confirmer votre mot de passe",
                                'ArgumentsException',
                                __FUNCTION__
                            );
                        }

                        if ($pass != $confPass) {
                            throw new BlogException(
                                "Forbidden",
                                403,
                                "Les mots de passe ne correspondent pas",
                                'ArgumentsException',
                                __FUNCTION__
                            );
                        }

                        $oUser->setPassword(password_hash($pass, PASSWORD_DEFAULT)); //Password hash

                        //Email
                        $aEmailParams["template"] = "updateUser";
                        $aEmailParams["subject"] = "Mise à jour de votre compte";
                        $aEmailParams["password"] = $pass;
                    }
                }

                //Save user
                $idUser = $userManager->saveUser($oUser);
                $result["success"] = true;
                $result["redirect"] = SERVER_BASE . "admin/config/users/" . $idUser . "/edit";
                $result["message"] = "L'enregistrement a bien été effectué";

                //Should an email be sent ?
                if (count($aEmailParams)) {
                    $result["sendemail"] = 1;

                    $aEmailParams["sender"] = NAME_CONTACT;
                    $aEmailParams["emailSender"] = EMAIL_CONTACT;

                    $emailContent = $this->_twig->render('mails/' . $aEmailParams['template'] . '.html.twig', [
                        'nom' => $oUser->getFullname(),
                        'login' => $oUser->getEmail(),
                        'password' => $aEmailParams['password'],
                        'role' => $oUser->getRoleId(),
                        'title' => $aEmailParams["subject"]
                    ]);
                    $result["paramsEmail"] = $aEmailParams;
                    $result["emailContent"] = $emailContent;

                    $result["message"] .= $this->sendEmailToUser($oUser->getEmail(), $emailContent, $aEmailParams);
                }

            } catch (BlogException $e) {
                $result["message"] = $e->getCustomText();
            } catch (Exception $e) {
                if (MODE_DEV) {
                    $result["message"] = $e->getMessage();
                } else {
                    $result["message"] = ERROR_MESSAGE;
                }
            }
        } else {
            throw new Exception("Method Not Allowed", 405);
        }

        echo json_encode($result);
    }

    public function sendEmailToUser($dest, $emailContent, $aEmailParams)
    {
        $message = "";

        try {
            $mailGenerator = new Mail($this->_twig);
            $mailGenerator->sendMailTwig($dest, $emailContent, $aEmailParams);

        } catch (Exception $e) {
            $message = " mais une erreur s'est produite lors de l'envoi de l'email de confirmation";
        }

        return $message;
    }

    /**
     * Sending an email to the user who has forgotten his password. BackOffice and FrontOffice page
     * @param $post
     * @throws Exception
     */
    public function forgotPass($post)
    {

        $userManager = new UserManager($this->_db);
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE
        ];

        //Check post email
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            try {

                if (empty($post['email-reset'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Le champ email est obligatoire",
                        'ArgumentsException',
                        __FUNCTION__
                    );
                }

                $email = Helper::cleanPostString($post['email-reset']);
                if (!Helper::isValidEmail($email)) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "L'adresse email est invalide",
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                //User Exists ?
                $oUser = $userManager->getUserByEmail($email);
                if ($oUser->getUserId() === null) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Email inconnu",
                        'DataException',
                        __FUNCTION__
                    );
                }

                if (!$oUser->getActive()) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Utilisateur refusé. Contacter l'administrateur du site",
                        'ApplicationException',
                        __FUNCTION__
                    );
                }

                //Generate token
                $issuedAt = time();
                $expirationTime = $issuedAt + (60 * 10);  // jwt valid for 10 mn from the issued time
                $payload = array(
                    'userid' => $oUser->getUserId(),
                    'role' => $oUser->getRoleId(),
                    'email' => $oUser->getEmail(),
                    'iat' => $issuedAt,
                    'exp' => $expirationTime
                );
                $key = SECRET_KEY;
                $alg = 'HS256';
                $jwt = JWT::encode($payload, $key, $alg);


                //Send email with reinitialise password link
                $aEmailParams["subject"] = "Mot de passe oublié";
                $aEmailParams["sender"] = NAME_CONTACT;
                $aEmailParams["emailSender"] = EMAIL_CONTACT;

                $emailContent = $this->_twig->render('mails/reinitLink.html.twig', [
                    'nom' => $oUser->getFullname(),
                    'role' => $oUser->getRoleId(),
                    'title' => $aEmailParams["subject"],
                    'link' => COMPLETE_PATH . 'resetPassword?token=' . $jwt
                ]);

                $mailGenerator = new Mail($this->_twig);
                $mailGenerator->sendMailTwig($oUser->getEmail(), $emailContent, $aEmailParams);
                $result["success"] = true;
                $result["message"] = "Merci de vérifier vos courriels, un lien de réinitialisation vous a été envoyé";

            } catch (BlogException $e) {
                if (MODE_DEV) {
                    Debug::printr($e->getMessage());
                    exit();
                } else {
                    $result["message"] = $e->getCustomText();
                }
            } catch (Exception $e) {
                if (MODE_DEV) {
                    Debug::printr($e->getMessage());
                    exit();
                }
            }

        } else {
            throw new Exception("Method Not Allowed", 405);
        }

        echo json_encode($result);
    }

    /**
     * Display page with form for change password. With jwt token identification. FrontOffice page
     * @param array $get
     * @throws Twig\Error\LoaderError
     * @throws Twig\Error\RuntimeError
     * @throws Twig\Error\SyntaxError
     */
    public function displayResetPass($get = [])
    {
        $aParams = [];

        if (count($get) && array_key_exists("token", $get) && !empty($get['token'])) {
            try {
                JWT::$leeway = 60;
                $jwt_decode = JWT::decode($get['token'], SECRET_KEY, array('HS256'));
                $aParams["role"] = $jwt_decode->role;

            } catch (Exception $e) {}

            $aParams["token"] = $get['token'];
        }
        echo $this->_twig->render('front/pages/resetPass.html.twig', $aParams);
    }

    /**
     * Save new password after an user has changed it. FrontOffice page
     * @param $post
     * @throws Exception
     */
    public function reinitPass($post)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE
        ];

        //Check post email
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            try {
                if (empty($post['token'])) {
                    throw new BlogException(
                        "Token expired/invalid",
                        498,
                        "Page expirée",
                        'SecurityException',
                        __FUNCTION__
                    );
                }

                // All fields ?
                if (empty($post['email']) || empty($post['password']) || empty($post['password-confirm'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Tous les champs sont obligatoires",
                        'ArgumentsException',
                        __FUNCTION__
                    );
                }

                $email = Helper::cleanPostString($post['email']);
                $pass = Helper::cleanPostString($post['password'], false);
                $confPass = Helper::cleanPostString($post['password-confirm'], false);
                $passwordError = Helper::isPasswordComplex($pass);

                //Token decode
                JWT::$leeway = 60;
                $jwt_decode = JWT::decode($post['token'], SECRET_KEY, array('HS256'));

                if ($email !== $jwt_decode->email) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Merci de renseigner l'email pour laquelle la demande a été faite",
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                if (!Helper::isValidEmail($email)) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "L'email est incorrect",
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                if (!$passwordError["success"]) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        $passwordError['message'],
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                if ($pass != $confPass) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Les mots de passe ne correspondent pas",
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                //Modify User
                $userManager = new UserManager($this->_db);
                $oUser = $userManager->getuserById($jwt_decode->userid);

                if ($oUser->getUserId() !== null) {

                    $oUser->setPassword(password_hash($pass, PASSWORD_DEFAULT)); //Password hash

                    //Save user
                    $userManager->saveUser($oUser);

                    //Destroy session
                    $aGet = [
                        'org' => $post['org'],
                        'redirect' => false
                    ];

                    $this->_userService->logout($aGet);

                    $result["success"] = true;
                    $result["message"] = "La modification a bien été enregistrée. Un email de confirmation vous a été envoyé.";

                    //Send Email
                    $aEmailParams["subject"] = "Mise à jour de votre compte";
                    $aEmailParams["sender"] = NAME_CONTACT;
                    $aEmailParams["emailSender"] = EMAIL_CONTACT;

                    $emailContent = $this->_twig->render('mails/updateUser.html.twig', [
                        'nom' => $oUser->getFullname(),
                        'login' => $oUser->getEmail(),
                        'password' => $pass,
                        'role' => $oUser->getRoleId(),
                        'title' => $aEmailParams["subject"]
                    ]);

                    $mailGenerator = new Mail($this->_twig);
                    $mailGenerator->sendMailTwig($oUser->getEmail(), $emailContent, $aEmailParams);

                } else {
                    throw new BlogException(
                        "Not Found",
                        404,
                        "Une erreur s'est produite, aucune modification n'a été effectuée",
                        'DataException',
                        __FUNCTION__
                    );
                }
            } catch (BlogException $e) {
                $result["message"] = $e->getCustomText();
            } catch (\Exception $e) {
                $result["message"] = "Le token est invalide";
            } catch (Exception $e) {
                if (MODE_DEV) {
                    $result["message"] = $e->getMessage();
                }
            }

        } else {
            throw new Exception("Method Not Allowed", 405);
        }

        echo json_encode($result);
    }

    /**
     * Change user status (no delete action). BackOffice action
     * @param $post
     * @throws Exception
     */
    public function changeStatusUser($post)
    {
        if ($this->_userService->checkSession() && $this->_userService->isAdminSession()) {

            $result = [
                "success" => false,
                "message" => ERROR_MESSAGE
            ];

            if ($_SERVER['REQUEST_METHOD'] == 'POST') {

                try {
                    if (!empty($post['idUser']) && is_numeric($post['idUser'])) {

                        $userManager = new UserManager($this->_db);
                        $oUser = $userManager->getUserById($post['idUser']);

                        //User exists ?
                        if (!is_null($oUser->getUserID())) {

                            if (Session::getInstance()->getValue('user_id') !== intval($post['idUser'])) {
                                $result["success"] = $userManager->changeStatusUser($oUser);
                            } else {
                                $result["message"] = "Vous ne pouvez pas changer le statut de l'utilisateur courant";
                            }
                        }
                    } else {
                        throw new BlogException("Forbidden", 403, "Utilisateur inconnu", 'DataException', __FUNCTION__);
                    }

                } catch (Exception $e) {
                    if (MODE_DEV) {
                        $result["message"] = $e->getMessage();
                    } else {
                        $result["message"] = ERROR_MESSAGE;
                    }
                }

            } else {
                throw new Exception("Method Not Allowed", 405);
            }

            echo json_encode($result);
            exit();
        }
    }
}
