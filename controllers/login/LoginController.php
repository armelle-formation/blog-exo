<?php

class LoginController extends AbstractController
{

    /**
     * Check session and his type and display corresponding page
     */
    public function displayLogin()
    {
        // Session active ?
        if (Session::getInstance()->getValue('initialize')) {
            $this->redirect();

        // Check Cookie log ?
        } elseif (array_key_exists('keep_log', $_COOKIE)) {
            $cookieCrypted = $_COOKIE['keep_log'];
            $this->checkLogCookie($cookieCrypted);

            $this->redirect();

        } else {
            echo $this->_twig->render('admin/pages/displayLogin.html.twig', [
                'footer' => false,
                'login' => true
            ]);
        }
    }

    /**
     * Redirect according to the type of the session
     */
    public function redirect()
    {
        if ($this->_userService->isBackSession()) {
            header('Location: ' . SERVER_BASE . 'admin/dashboard');
        } else {
            header('Location: ' . SERVER_BASE . 'blog');
        }

    }

    /**
     * Check login credentials, redirect or init sessions
     * @param $post
     * @throws Exception
     */
    public function doLogin($post)
    {
        $result = [
            "success" => false,
            "message" => "",
            "redirect" => SERVER_BASE . "admin"
        ];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            try {

                // Form pause to slow down attempts to recover password
                sleep(0.5);

                if (empty($post['email']) || empty($post['password'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Tous les champs sont obligatoires",
                        'ArgumentsException',
                        __FUNCTION__
                    );
                }

                if (empty($post['origin']) || empty($post['url'])) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        ERROR_MESSAGE,
                        'ArgumentsException',
                        __FUNCTION__
                    );
                }

                $email = Helper::cleanPostString($post['email']);
                $password = Helper::cleanPostString($post['password']);
                $remember = isset($post['remember']) && $post['remember'] ? 1 : 0;
                $url = $post['url'];
                $origin = $post['origin'];

                // Email valid
                if (!Helper::isValidEmail($email)) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "L'email est incorrect",
                        'ValidityException',
                        __FUNCTION__
                    );
                }

                $userManager = new UserManager($this->_db);
                $oUser = $userManager->getUserByEmail($email);

                if (is_null($oUser->getUserId())) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Email inconnu",
                        'DataException',
                        __FUNCTION__
                    );
                }

                if (!$oUser->getActive()) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Compte désactivé. Contactez l'administrateur du site",
                        'SecurityException',
                        __FUNCTION__
                    );
                }

                if (!password_verify($password, $oUser->getPassword())) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Identifiants incorrects",
                        'SecurityException',
                        __FUNCTION__
                    );
                }

                $session = $this->initSession($oUser, $remember);
                $result["success"] = true;

                //Redirection
                $result["redirect"] = $url;
                $aRoles = array(ID_ADMIN, ID_EDITOR);
                if ($origin == 'admin' && (in_array($session->getValue('role'), $aRoles))) {
                    $result["redirect"] = SERVER_BASE . 'admin/dashboard';
                }

            } catch (BlogException $e) {
                $result["message"] = $e->getCustomText();
            } catch (Exception $e) {
                if (MODE_DEV) {
                    $result["message"] = $e->getMessage();
                } else {
                    $result["message"] = ERROR_MESSAGE;
                }
            }
        } else {
            throw new Exception("Method Not Allowed", 405);
        }
        echo json_encode($result);
    }

    /**
     * Initialize session and redirect
     * @param $user
     * @param $remember
     * @return Session
     * @throws BlogException
     */
    public function initSession($user, $remember): Session
    {
        $session = Session::getInstance();

        // Ticket
        $ticket = Key::generateTicket();

        $session->setValue('initialize', true);
        $session->setValue('user_id', $user->getUserId());
        $session->setValue('role', $user->getRoleId());
        $session->setValue('fullname', $user->getFullname());
        $session->setValue('geo_consent', $user->getGeoConsent());
        $session->setValue('timeout_idle', time() + MAX_IDLE_TIME);
        $session->setValue('sm_t', $ticket);

        //Initialize cookies
        $this->initCookies($user, $remember, $ticket);

        return $session;
    }

    /**
     * Initialize cookies ticket and keep_log
     * @param $user
     * @param $remember
     * @param $ticket
     * @throws BlogException
     */
    public function initCookies($user, $remember, $ticket)
    {

        //Cookie keep log
        if ($remember) {
            $cookie = array(
                'ip' => $_SERVER['REMOTE_ADDR'],
                'username' => $user->getEmail()
            );

            // Cookie crypt
            $key = Key::createNewRandomKey();
            $cookieCrypted = Key::encryptDecrypt('encrypt', json_encode($cookie), $key);

            if (!array_key_exists('keep_log', $_COOKIE)) {

                setcookie('keep_log', $cookieCrypted, COOKIE_DURATION);

                //Save cookie into Db
                $manager = new UserManager($this->_db);
                $manager->saveLogCookie($user->getUserId(), $cookieCrypted);
            }
        }

        // Cookie ticket (duration 60 min)
        setcookie("sm_t", "", time()-3600);
        setcookie('sm_t', $ticket, time()+ (60*60), '/');
    }


    /**
     * Check cookie keep_log before connection
     * @param string $cookieCrypted
     * @throws
     */
    public function checkLogCookie($cookieCrypted)
    {
        // Search cookie in Db
        $manager = new UserManager($this->_db);
        $datas = $manager->getLogCookie($cookieCrypted);

        //No user found with decode cookie : unset cookie keep_log
        if (count($datas) === 0) {
            setcookie("keep_log", "", time()-3600);
            header('Location: '. SERVER_BASE . 'admin');
            exit();
        }

        // Check if cookie Ip == Current IP
        $key = Key::createNewRandomKey();
        $cookie = Key::encryptDecrypt('decrypt', $cookieCrypted, $key);
        $cookie = json_decode($cookie);

        //Different IP : unset cookie keep_log
        if ($cookie->ip !== $_SERVER['REMOTE_ADDR']) {
            setcookie("keep_log", "", time()-3600);
            header('Location: '. SERVER_BASE . 'admin');
            exit();
        }

        // Get User
        $userManager = new UserManager($this->_db);
        $user = $userManager->getUserById($datas[0]->user_id);

        //No user found with id : unset cookie keep_log
        if ($user->getUserId() === null) {
            setcookie("keep_log", "", time()-3600);
            header('Location: '. SERVER_BASE . 'admin');
            exit();
        } else {
            $this->initSession($user, true);
        }
    }

    /**
     * Logout user on his request
     * @param array $get
     */
    public function doLogout(array $get)
    {
        $this->_userService->logout($get);
    }
}
