<?php

class MainController extends AbstractController
{

    /**
     * Homepage display. FrontOffice action
     */
    public function displayHome()
    {
        $path = ROOT . BLOG_IMG_PATH;
        $postManager = new PostManager($this->_db);
        $aPosts = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'p.image_file' => 'IS NOT NULL',
                'c.category_id_parent' => 1,
                'c.is_active' => 1,
                'c.is_private' => 0
            ],
            'p.date_create',
            'DESC',
            true,
            true,
            false,
            true,
            '3'
        );

        foreach ($aPosts['aPosts'] as $oPost) {
            $postImage = $oPost->getImageFile();
            if ($postImage == null || !file_exists(ROOT . BLOG_IMG_PATH . "l_" . $postImage)) {
                $oPost->setImageFile(GENERIC_BLOG_IMAGE);
            }
        }

        //Post for about me
        $aPresentation = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'c.category_id' => 42,
                'c.is_active' => 1
            ],
            'p.date_create',
            'DESC',
            false,
            true,
            false,
            false,
            1
        );

        //Post for skills
        $aSkills = $postManager->getPostsByProperties(
            [
                'p.active' => 1,
                'c.category_id' => 43,
                'c.is_active' => 1
            ],
            'p.date_create',
            'DESC',
            false,
            true,
            false
        );

        //Params
        $aParams = $this->getInitFrontRender();
        $aParams['posts'] = $aPosts['aPosts'];
        $aParams['presentation'] = $aPresentation['aPosts'];
        $aParams['skills'] = $aSkills['aPosts'];

        echo $this->_twig->render('front/pages/displayHome.html.twig', $aParams);
    }

    /**
     * Send contact message posted from contact form. FrontOffice action
     * @param $data
     * @throws Exception
     */
    public function sendContactMessage($data)
    {
        $result = [
            "success" => false,
            "message" => ERROR_MESSAGE,
            "redirect" => SERVER_BASE
        ];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            try {
                if (empty($data['name']) || empty($data['emailContact']) || empty($data['subject'] || empty($data['message']))) {
                    throw new BlogException(
                        "Forbidden",
                        403,
                        "Tous les champs sont obligatoires",
                        'ArgumentsException',
                        __FUNCTION__
                    );
                }

                $messageManager = new MessageManager($this->_db);

                //Clean html
                $config = HTMLPurifier_Config::createDefault();
                $purifier = new HTMLPurifier($config);

                //Title & Comment length
                $subject = $purifier->purify($data['subject']);
                $msg = $purifier->purify($data['message']);
                $name = $purifier->purify($data['name']);
                $email = $data['emailContact'];

                if (strlen($subject) > 250) {
                    throw new BlogException("Forbidden", 403, "Sujet trop long", 'ArgumentsException', __FUNCTION__);
                }
                if (strlen($msg) > 450) {
                    throw new BlogException("Forbidden", 403, "Message trop long", 'ArgumentsException', __FUNCTION__);
                }
                if (!Helper::isValidEmail($email)) {
                    throw new BlogException("Forbidden", 403, "Email invalide", 'ValidityException', __FUNCTION__);
                }

                //Hydrate oMessage
                $oMessage = new MessageEntity();
                $oMessage->setSubject($subject);
                $oMessage->setDateSend(new DateTime('now'));
                $oMessage->setSender(NAME_CONTACT);
                $oMessage->setEmailSender(EMAIL_CONTACT);
                $oMessage->setRecipient(NAME_CONTACT);
                $oMessage->setEmailRecipient(EMAIL_CONTACT);
                $oMessage->setContent($msg);

                //Save Message
                $messageManager->saveMessage($oMessage);

                //SendMessage
                $aEmailParams["subject"] = "Message depuis le formulaire de contact";
                $aEmailParams["sender"] = $oMessage->getSender();
                $aEmailParams["emailSender"] = $oMessage->getEmailSender();
                $emailContent = $this->_twig->render('mails/mailContactForm.html.twig', [
                    'subject' => $oMessage->getSubject(),
                    'sender' => $name,
                    'emailSender' => $email,
                    'dateSend' => $oMessage->getDateSend(),
                    'message' => $oMessage->getContent()
                ]);

                $mailGenerator = new Mail($this->_twig);
                $mailGenerator->sendMailTwig(EMAIL_CONTACT, $emailContent, $aEmailParams);

                $result["success"] = true;
                $result["message"] = "Merci pour votre message.\n Nous vous répondrons dans les plus brefs délais.";

            } catch (BlogException $e) {
                $result["message"] = $e->getCustomText();

            } catch (Exception $e) {
                if (MODE_DEV) {
                    Debug::printr($e->getMessage());
                    exit();
                }
            }
        } else {
            throw new Exception("Method Not Allowed", 405);
        }

        echo json_encode($result);
    }
}
