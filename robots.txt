# Fichier robots.txt
User-agent : *

Disallow: /assets/
Disallow: /core/
Disallow: /vendor/
Disallow: /diagrammes/
Disallow: /model/
Disallow: /assets/
Disallow: /controllers/
Disallow: /views/admin/
Disallow: /views/error/
Disallow: /views/mails/
Disallow: /composer.json
Disallow: /*.php$

Disallow: /*?*
Disallow: /*?
Disallow: /*&

Allow: /uploads/

# Sitemap
Sitemap: http://blog.oc-armellebraud.fr/sitemap.xml