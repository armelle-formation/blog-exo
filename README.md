# aBBlog

This project is a training project. 
It's the Project n°5 from PHP / Symfony web developer cursus of OpenClassRooms 

The final website contains a personal Blog PHP with a resume section. 
All texts of FrontOffice part are customizables via the BackOffice part.
The final result is visible : [aBBlog Online](http://blog.oc-armellebraud.fr)



## Technical specifications
* MVC & OOP
* Bootstrap theme
* Libraries PHP installed via composer

## Requirements
* PHP ≥ 7.2
* Composer

##### Extensions ##### 
* json
* gd
* imap
* pdo
* ldap
* intl
* mbstring

##### Vendor #####
* twig 2.12.2
* twig/extensions
* font-awesome 5.9.0
* firebase/php-jwt
* htmlpurifier 4.12


## Installation
### Clone
```bash
git clone https://gitlab.com/armelle-formation/blog-exo.git
```
###  Composer
``` bash
composer install
```
``` bash
composer update
```

### Customization
* Copy `/core/_config.php`, Customized it (customizable section : database credentials, contact email....) and rename it `config.php`
* Create a Database
* Import `database.sql` file
* Create an uploads folder and its tree (like you have configured in `config.php`). For your information, original tree is :
``` bash
└── uploads
    ├── documents
    │   ├── cv
    │   ├── formation
    │   └── projects
    └── images
        ├── admin
        │   └── profile
        └── blog
            └── corps-articles

```
* At the root of your blog images directory (defined by constant `BLOG_IMG_PATH`), you must copy the default images located in `/assets/images/default/`
* Create your own resume.json file from `core/json/_resume_example.json` and rename it like you indicated in config file (filenames section)

For create your own admin user you can :
* Create an account form frontOffice (commentator role account by default)
* Change the role of this user in the database

Having fun (at least I hope) !

## Code Quality
Medal A on SonarQube (HTML, CSS ans PHP PSR-2)  
![alt text](assets/img/quality_gate.jpg)

## Project status
Development has slowed down but not stopped. You can contact me if you need to help or more informations

##Copyright and License of the aBBlog
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.