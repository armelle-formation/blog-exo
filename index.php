<?php

require_once 'core/config/config.php';

try {

    /**
     * Router Initialization
     */
    $router = new Router();

    /*****************************
     * FRONT OFFICE ROUTES
     ****************************/

    /**
     * HomePage
     */
    $router->add(SERVER_BASE, function () {
        $mainController = new MainController();
        $mainController->displayHome();
    });

    /**
     * Blog
     */
    $router->add(SERVER_BASE . 'blog/?', function () {
        $blogController = new BlogController();
        $blogController->displayMainBlog($_GET);
    });

    $router->add(SERVER_BASE . 'blog/article-([\w]*)', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $postController = new PostController();
            $postController->displayFrontSinglePost($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'un article",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'blog/category-([\w]*)/?', function ($idCat) {
        if (isset($idCat) && is_numeric($idCat)) {
            $postController = new PostController();
            $postController->displayPostByCategory($idCat, $_GET);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage des articles par catégorie",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'blog/tag-([\w]*)/?', function ($idTag) {
        if (isset($idTag) && is_numeric($idTag)) {
            $postController = new PostController();
            $postController->displayPostByTag($idTag, $_GET);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage des articles par tag",
                'ArgumentsException'
            );
        }
    });

    /**
     * Comments
     */
    $router->add(SERVER_BASE . 'blog/comment/save', function () {
        if (!empty($_POST)) {
            $controllerComment = new CommentController();
            $controllerComment->saveComment($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }

    });

    $router->add(SERVER_BASE . 'blog/comment/status', function () {
        if (!empty($_POST)) {
            $controllerComment = new CommentController();
            $controllerComment->changeStatus($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    $router->add(SERVER_BASE . 'blog/comment/([\w]*)/delete', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $controllerComment = new CommentController();
            $controllerComment->deleteComment($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour la suppression d'un commentaite",
                'ArgumentsException'
            );
        }
    });

    /**
     * Resume
     */
    $router->add(SERVER_BASE . 'resume/?', function () {
        $resumeController = new ResumeController();
        $resumeController->displayResume();
    });

    /**
     * Contact Message
     */
    $router->add(SERVER_BASE . 'contact-message/?', function () {
        if (!empty($_POST)) {
            $mainController = new MainController();
            $mainController->sendContactMessage($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });


    /*****************************
     * BACK OFFICE ROUTES
     ****************************/

    /**
     * Login
     */
    $router->add(SERVER_BASE . 'admin/?', function () {
        $controllerLogin = new LoginController();
        $controllerLogin->displayLogin();
    });

    /**
     * Dashboard
     */
    $router->add(SERVER_BASE . 'admin/dashboard/?', function () {
        $controllerAdmin = new AdminController();
        $controllerAdmin->displayDashboard();
    });

    /**
     * Manage Posts
     */
    $router->add(SERVER_BASE . 'admin/blog/posts/?', function () {
        $controllerPost = new PostController();
        $controllerPost->displayPosts();
    });

    $router->add(SERVER_BASE . 'admin/blog/posts/add', function () {
        $controllerPost = new PostController();
        $controllerPost->editPost();
    });

    $router->add(SERVER_BASE . 'admin/blog/posts/([\w]*)/edit', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $controllerPost = new PostController();
            $controllerPost->editPost($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'un article",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'admin/blog/posts/delete', function () {
        if (!empty($_POST)) {
            $controllerPost = new PostController();
            $controllerPost->deletePost($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    $router->add(SERVER_BASE . 'admin/blog/posts/save', function () {
        if (!empty($_POST)) {
            $controllerPost = new PostController();
            $controllerPost->savePost($_POST, $_FILES);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manage Categories
     */
    $router->add(SERVER_BASE . 'admin/blog/cat/?', function () {
        $controllerPost = new CategoryController();
        $controllerPost->displayListCategories();
    });

    $router->add(SERVER_BASE . 'admin/blog/cat/add', function () {
        $controllerPost = new CategoryController();
        $controllerPost->editCategory();
    });

    $router->add(SERVER_BASE . 'admin/blog/cat/([\w]*)/edit', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $controllerPost = new CategoryController();
            $controllerPost->editCategory($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'une catégorie",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'admin/blog/cat/delete', function () {
        if (!empty($_POST)) {
            $controllerCategory = new CategoryController();
            $controllerCategory->deleteCategory($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }

    });

    $router->add(SERVER_BASE . 'admin/blog/cat/save', function () {
        if (!empty($_POST)) {
            $controllerCat = new CategoryController();
            $controllerCat->saveCategory($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manage Tags
     */
    $router->add(SERVER_BASE . 'admin/blog/tags/?', function () {
        $tagController = new TagController();
        $tagController->displayListTags();
    });

    $router->add(SERVER_BASE . 'admin/blog/tags/add', function () {
        $tagController = new TagController();
        $tagController->editTag();
    });

    $router->add(SERVER_BASE . 'admin/blog/tags/([\w]*)/edit', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $tagController = new TagController();
            $tagController->editTag($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'un tag",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'admin/blog/tags/delete', function () {
        if (!empty($_POST)) {
            $tagController = new TagController();
            $tagController->deleteTag($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    $router->add(SERVER_BASE . 'admin/blog/tags/save', function () {
        if (!empty($_POST)) {
            $tagController = new TagController();
            $tagController->saveTag($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manages Comments
     */
    $router->add(SERVER_BASE . 'admin/comments/?', function () {
        $controllerComment = new CommentController();
        $controllerComment->displayAllComments();
    });

    $router->add(SERVER_BASE . 'admin/comments/([\w]*)/edit', function ($id) {
        if (isset($id) && is_numeric($id)){
            $controllerComment = new CommentController();
            $controllerComment->editComment($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'un commentaire",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'admin/comments/save', function () {
        if (!empty($_POST)) {
            $controllerComment = new CommentController();
            $controllerComment->saveComment($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }

    });

    $router->add(SERVER_BASE . 'admin/comments/delete', function () {
        if (!empty($_POST)) {
            $controllerComment = new CommentController();
            $controllerComment->deleteComment($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manage Users
     */
    $router->add(SERVER_BASE . 'admin/config/users/?', function () {
        $controllerAdmin = new UserController();
        $controllerAdmin->displayUsers();
    });

    $router->add(SERVER_BASE . 'admin/config/users/add', function () {
        $controllerPost = new UserController();
        $controllerPost->editUser();
    });

    $router->add(SERVER_BASE . 'admin/config/users/([\w]*)/edit', function ($id) {
        if (isset($id) && is_numeric($id)) {
            $controllerUser = new UserController();
            $controllerUser->editUser($id);
        } else {
            throw new BlogException(
                "Bad Request",
                400,
                "ID non passé ou au format incorrect pour l'affichage d'un utilisateur",
                'ArgumentsException'
            );
        }
    });

    $router->add(SERVER_BASE . 'admin/config/users/status', function () {
        if (!empty($_POST)) {
            $controllerUser = new UserController();
            $controllerUser->changeStatusUser($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    $router->add(SERVER_BASE . 'admin/config/users/save', function () {
        if (!empty($_POST)) {
            $controllerPost = new UserController();
            $controllerPost->saveUser($_POST, $_FILES);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manage Password
     */
    $router->add(SERVER_BASE . 'forgotpassword', function () {
        if (!empty($_POST)) {
            $controllerPost = new UserController();
            $controllerPost->forgotPass($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    $router->add(SERVER_BASE . 'resetPassword', function () {
        $controllerPost = new UserController();
        $controllerPost->displayResetPass($_GET);
    });

    $router->add(SERVER_BASE . 'savePassword', function () {
        if (!empty($_POST)) {
            $controllerPost = new UserController();
            $controllerPost->reinitPass($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Manage Resume
     */
    $router->add(SERVER_BASE . 'admin/resume/?', function () {
        $resumeController = new ResumeController();
        $resumeController->editResume();
    });

    $router->add(SERVER_BASE . 'admin/resume/save/?', function () {
        if (!empty($_POST)) {
            $resumeController = new ResumeController();
            $resumeController->saveResume($_POST, $_FILES);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Login
     */
    $router->add(SERVER_BASE . 'admin/login', function () {
        if (!empty($_POST)) {
            $controllerLogin = new LoginController();
            $controllerLogin->doLogin($_POST);
        } else {
            throw new BlogException("Bad Request", 400, ERROR_MESSAGE, "ArgumentsException");
        }
    });

    /**
     * Logout
     */
    $router->add(SERVER_BASE . 'admin/logout', function () {
        $controllerLogin = new LoginController();
        $controllerLogin->doLogout($_GET);
    });

    /***************************
     * GENERIC ROUTES
     **************************/

    /**
     * 404 method définition
     */
    $router->pathNotFound(function () {
        throw new BlogException("Not Found", 404, ERROR_MESSAGE, 'DataException');
    });

    /***************************
     * SPECIFIC ROUTES
     **************************/

    /**
     * Route execute
     */
    $router->run($_SERVER['REQUEST_URI'], '/');

} catch (BlogException $e) {
    $controller = new ErrorController();
    $controller->dspBlogError($e);

} catch (Exception $e) {
    $controller = new ErrorController();
    $controller->dspError($e);

}
